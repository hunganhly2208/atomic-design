import React from 'react'
import ReactDOM from 'react-dom'
import { createHashHistory } from 'history'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router/immutable'
import { PersistGate } from 'redux-persist/lib/integration/react'

import createStore from './Stores/CreateStore.js'
import storage from 'redux-persist/lib/storage'
import { unregister } from './registerServiceWorker'
import App from 'Components/App'
import { UserContext } from 'Contexts/UserData'
import { getTokenData, getPermissions } from 'Utils/token'
import MomentUtils from '@date-io/moment'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import './i18n'

const history = createHashHistory()

const auth = getTokenData()
const permissions = getPermissions()

const userData = { auth, permissions }

// Example clear storage in redux-persist

// storage.getItem('HaiYenroot').then(data => {
//   if (data) {
//     let item = JSON.parse(data)
//     let auth = JSON.parse(item.authentication)
//     let newAuth = { ...auth, user: null, permissions: [] }
//     let newItem = { ...item, authentication: JSON.stringify(newAuth) }
//     storage.setItem('HaiYenroot', JSON.stringify(newItem))
//   }
// })

async function init() {
  const store = createStore(history)

  const MOUNT_NODE = document.getElementById('root')

  const render = () =>
    ReactDOM.render(
      <Provider store={store}>
        <PersistGate loading={<div />} persistor={store.persistor}>
          <ConnectedRouter history={history}>
            <UserContext.Provider value={userData}>
              <MuiPickersUtilsProvider utils={MomentUtils}>
                <App />
              </MuiPickersUtilsProvider>
            </UserContext.Provider>
          </ConnectedRouter>
        </PersistGate>
      </Provider>,
      MOUNT_NODE
    )
  if (module.hot) {
    module.hot.accept('Components/App', () => {
      ReactDOM.unmountComponentAtNode(MOUNT_NODE)
      render()
    })
  }

  render()
}

init()

unregister()
