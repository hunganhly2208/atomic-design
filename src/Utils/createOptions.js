export const createDepartmentOption = (
  departmentId,
  isLead = 0,
  isOrigin = 0
) => {
  return departmentId
    ? {
        departmentId,
        isLead,
        isOrigin,
      }
    : ''
}

export const createOfficeOption = (agencyId, isLead = 0, isOrigin = 0) => {
  return agencyId
    ? {
        agencyId,
        isLead,
        isOrigin,
      }
    : ''
}

export const createDepartmentOptionFromFormik = (
  department,
  isLead = 0,
  isOrigin = 0
) => {
  return department
    ? {
        departmentId: department.value,
        isLead,
        isOrigin,
      }
    : ''
}
