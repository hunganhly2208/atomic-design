import { Config } from 'Config'

export const getImageUrl = subPath => {
  return subPath ? Config.baseImageUrl + subPath : false
}
