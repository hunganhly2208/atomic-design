export const getViewMode = size => {
  if (size >= 992) {
    return 1
  } else if (size >= 768) {
    return 2
  } else {
    return 3
  }
}
