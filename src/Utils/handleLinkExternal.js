import { Config } from 'Config'
export const handleLinkExternal = path => () => {
  window.location.href = `${Config.homePage}/#${path}`
}
