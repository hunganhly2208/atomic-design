export const havePermission = (permissions = []) => {
  return permissions ? permissions.length !== 0 : false
}

export const canAccess = (permissions = [], name = '') => {
  return Boolean(permissions.includes(name))
}

export const checkPermissionsByPath = (permissions = [], path = '') => {
  switch (path) {
    case '/user':
      if (!permissions.includes('users.manage')) return false
      break
    case '/office-management':
      if (!permissions.includes('users.manage')) return false
      break
    case '/department-management':
      if (!permissions.includes('users.manage')) return false
      break
    case '/holiday-management':
      if (!permissions.includes('users.manage')) return false
      break
    case '/news-category/assign/:categoryId':
      if (!permissions.includes('news.manage')) return false
      break
    case '/leave-type-management':
      if (!permissions.includes('leave.manage')) return false
      break
    default:
      break
  }
  return true
}
