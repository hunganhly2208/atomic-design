export const showTitle = path => {
  let lookUp = {
    '/product': 'Product',
    '/modalExample': 'Modal Example',
    '/user': 'User Management',
  }
  return lookUp[path] ? lookUp[path] : 'React Template V5'
}
