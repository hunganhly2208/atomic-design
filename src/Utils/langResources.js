export const resources = {
  en: {
    drawer: {
      homepage: 'Homepage',
      myColleagues: 'My Colleagues',
      meetingRoomBooking: 'Meeting Room Booking',
      leaveRequest: 'Leave Request',
      leaveRequestManager: 'Leave Request Manager',
      staffCalendar: 'Staff Calendar',
      stationeryBooking: 'Stationery Booking',
      stationeryRequestItem: 'Stationery Request Item',
      help: 'Help',
      managementTools: 'Management Tools',
      viewProfile: 'View Profile',
      addNewPost: 'Add New Post',
      viewMyPost: 'View My Post',
      changePassword: 'Change Password',
      logOut: 'Log Out',
    },
    article: {
      view: 'View Article',
      post: 'Post Article',
      email: 'Send Email',
      assignUserToCategory: 'Assign User to category',
      chooseUser: 'Choose user',
      addUser: 'Add user',
      removeUserFromCategory: 'Remove User From Category',
      confirmRemove: 'Do you want to remove this user?',
    },
    profile: {
      generalInfo: 'General Info',
      personalInfo: 'Personal Info',
      userProfile: 'User Profile',
      fullName: 'Full Name',
      gender: 'Gender',
      email: 'Email',
      phone: 'Phone',
      jobTitle: 'Job Title',
      birthDay: 'Birth Day',
      identityCard: 'Identity Card',
      issueAt: 'Issue at',
      issueDate: 'Issue Date',
      taxCode: 'Tax Code',
      insuranceNumber: 'Insurance Number',
      residentialAddress: 'Residential Address',
      address: 'Address',
      editProfile: 'Edit Profile',
      passowrd: 'Password',
      placeOfBirth: 'Place Of Birth',
    },
    user: {
      userManagement: 'User Management',
      searchByName: "Search User's Name",
      department: 'Department',
      office: 'Office',
      newUser: 'New User',
      chooseDepartment: 'Choose Department',
      chooseOffice: 'Choose Office',
      setUserRole: "Set User's roles",
      organizeUser: 'Organize user',
      editUser: 'Edit User',
      deleteUser: 'Delete User',
      createUser: 'Create User',
      uploadAvatar: 'Upload Avatar',
      changeAvatar: 'Change Avatar',
      userInformation: "User's information",
      firstName: 'First Name',
      lastName: 'Last Name',
      displayName: 'Display Name',
      joinDate: 'Join Date',
      workInSaturday: 'Work in saturday',
      workInSunday: 'Work in sunday',
      noUser: 'No User',
      selectGender: 'Select gender',
      male: 'Male',
      female: 'Female',
      chooseJoinDate: 'Choose Join Date',
      no: 'No.',
      assignOfficeLead: 'Assign Office Lead',
      chooseOfficeLeader: 'Choose office leader',
      editOffice: 'Edit Office',
      createOffice: 'Create Office',
      assignDepartmentLead: 'Assign Department Lead',
      createDepartment: 'Create Department',
      editDepartment: 'Edit Department',
      chooseDepartmentLeader: 'Choose Department Leader',
      officeManagement: 'Office Management',
      departmentManagement: 'Department Management',
      noDepartment: 'No Department',
      noOffice: 'No Office',
      nextYear: 'Next Year',
      previousYear: 'Previous Year',
      holidayManagement: 'Holiday Management',
      createHoliday: 'Create Holiday',
      editHoliday: 'Edit Holiday',
      deleteHoliday: 'Delete Holiday',
      noHoliday: 'No Holiday',
      organization: 'Organization',
      officeName: "Office's Name",
      mainOffice: 'Main Office',
      subOffice: 'Sub Office',
      mainDepartment: 'Main Department',
      subDepartment: 'Sub Department',
      userPermission: "User's Permission",
      chooseUserPermission: 'Choose User Permission',
      permissionName: "Permission's Name",
      chooseDate: 'Choose Date',
      confirmDeleteHoliday: 'Do you want do delete this holiday?',
      confirmDeleteUser: 'Do you want to delete this user?',
    },
    leaveType: {
      leaveTypeManagement: 'Leave Type Manangement',
      createLeaveType: 'Create Leave Type',
      evidenceRequire: 'Evidence Require',
      leaveDays: 'Leave Days',
      typeName: "Type's Name",
      editLeaveType: 'Edit Leave Type',
      deleteLeaveType: 'Delete Leave Type',
      confirmDeleteLeaveType: 'Do you want do delete this Leave Type ?',
      abbreviation: 'Abbreviation',
      both: 'Both',
      noLeaveType: 'No Leave Type',
    },
    common: {
      search: 'Search',
      searchKeyword: 'Type Keyword here',
      searchByEmail: 'Search by email',
      searchByName: 'Search by Name',
      sortByType: 'Sort by Type',
      image: 'Image',
      actions: 'Actions',
      nameOrEmail: 'Name or Email',
      loadMore: 'Load More',
      delete: 'Delete',
      edit: 'Edit',
      cancel: 'Cancel',
      description: 'Description',
      name: 'Name',
      confirmDelete: 'Are you sure to delete this item ?',
      yes: 'Yes',
      no: 'No',
      status: 'Status',
      save: 'Save',
      create: 'Create',
      submit: 'Submit',
      detail: 'Detail',
      back: 'Back',
      date: 'Date',
      changeLanguage: 'Change Language',
      aProductOf: 'A product of',
    },
    equipmentGroup: {
      active: 'Active',
      inactive: 'Inactive',
      delete: 'Delete equipment group',
      add: 'Add equipment group',
      edit: 'Edit equipment group',
      noData: 'No data',
    },
    notify: {
      success: "Form submitted successfully, thank you.",
      fail: "Error submitting form, please try again.",
    }
  },
  vn: {
    drawer: {
      homepage: 'Trang Chủ',
      myColleagues: 'Đồng Nghiệp Của Tôi',
      meetingRoomBooking: 'Đặt Phòng Họp',
      leaveRequest: 'Yêu Cầu Nghỉ Phép',
      leaveRequestManager: 'Quản Lý Yêu Cầu Nghỉ Phép',
      staffCalendar: 'Lịch Làm Việc Của Nhân Viên',
      stationeryBooking: 'Đặt Văn Phòng Phẩm',
      stationeryRequestItem: 'Yêu Cầu Văn Phòng Phẩm',
      help: 'Giúp Đỡ',
      managementTools: 'Công Cụ Quản Lý',
      viewProfile: 'Xem Hồ Sơ',
      addNewPost: 'Tạo Bài Viết Mới',
      viewMyPost: 'Xem Bài Viết Của Tôi',
      changePassword: 'Đổi Mật Khẩu',
      logOut: 'Đăng Xuất',
    },
    article: {
      view: 'Xem Bài Viết',
      post: 'Tạo Bài Viết',
      email: 'Gửi Email',
      assignUserToCategory: 'Thêm nhân viên vào thư mục',
      chooseUser: 'Chọn nhân viên',
      addUser: 'Thêm Nhân Viên',
      removeUserFromCategory: 'Xóa Nhân Viên Khỏi Thư Mục',
      confirmRemove: 'Bạn có muốn xóa nhân viên này không?',
    },
    profile: {
      generalInfo: 'Thông Tin Chung',
      personalInfo: 'Thông Tin Cá Nhân',
      userProfile: 'Hồ Sơ Nhân Viên',
      fullName: 'Tên đầy đủ',
      gender: 'Giới Tính',
      email: 'Thư điện tử',
      phone: 'Số Điện Thoại',
      jobTitle: 'Chức danh',
      birthDay: 'Ngày sinh',
      identityCard: 'Số CMND',
      issueAt: 'Được cấp tại',
      issueDate: 'Ngày cấp',
      taxCode: 'Mã số thuế',
      insuranceNumber: 'Số sổ BHXH',
      residentialAddress: 'Thường trú(theo CMND)',
      address: 'Địa Chỉ',
      editProfile: 'Chỉnh Sửa Hồ Sơ',
      password: 'Mật Khẩu',
      placeOfBirth: 'Nơi Sinh',
    },
    user: {
      userManagement: 'Quản lý nhân viên',
      searchByName: 'Tìm Theo Tên NV',
      department: 'Bộ Phận',
      office: 'Chi Nhánh',
      newUser: 'Tạo Nhân Viên Mới',
      chooseDepartment: 'Chọn Bộ Phận',
      chooseOffice: 'Chọn Chi Nhánh',
      setUserRole: 'Cấp Quyền cho nhân viên',
      organizeUser: 'Chọn Tổ chức cho nhân viên',
      editUser: 'Chỉnh sửa nhân viên',
      deleteUser: 'Xóa nhân viên',
      createUser: 'Tạo Nhân Viên',
      uploadAvatar: 'Tải hình đại diện',
      changeAvatar: 'Đổi hình đại diện',
      userInformation: 'Thông tin của nhân viên',
      firstName: 'Họ',
      lastName: 'Tên',
      displayName: 'Tên hiển thị',
      joinDate: 'Ngày tham gia',
      workInSaturday: 'Làm vào thứ bảy',
      workInSunday: 'Làm vào chủ nhật',
      noUser: 'Không có nhân viên',
      selectGender: 'Chọn giới tính',
      male: 'Nam',
      female: 'Nữ',
      chooseJoinDate: 'Chọn Ngày Gia Nhập',
      no: 'STT',
      assignOfficeLead: 'Phân công trưởng chi nhánh',
      chooseOfficeLeader: 'Chọn trưởng chi nhánh',
      editOffice: 'Chỉnh sửa chi nhánh',
      createOffice: 'Tạo chi nhánh',
      assignDepartmentLead: 'Phân công trưởng phòng',
      createDepartment: 'Tạo bộ phận',
      editDepartment: 'Chỉnh sửa bộ phận',
      chooseDepartmentLeader: 'Chọn trưởng đạo phòng',
      officeManagement: 'Quản lý chi nhánh',
      departmentManagement: 'Quản lý bộ phận',
      noDepartment: 'Không có bộ phận',
      noOffice: 'Không có chi nhánh',
      nextYear: 'Năm Tới',
      previousYear: 'Năm Trước',
      holidayManagement: 'Quản lý ngày lễ',
      createHoliday: 'Tạo ngày lễ',
      editHoliday: 'Chỉnh sửa ngày lễ',
      deleteHoliday: 'Xóa ngày lễ',
      noHoliday: 'Không có ngày lễ',
      organization: 'Tổ Chức',
      officeName: 'Tên Trung Tâm',
      mainOffice: 'Trung tâm chính',
      subOffice: 'Trung tâm phụ',
      mainDepartment: 'Bộ phận chính',
      subDepartment: 'Bộ phận phụ',
      userPermission: 'Quyền của nhân viên',
      chooseUserPermission: 'Chọn quyền cho nhân viên',
      permissionName: 'Tên quyền',
      chooseDate: 'Chọn Ngày',
      confirmDeleteHoliday: 'Bạn có muốn xóa ngày lễ này không ?',
      confirmDeleteUser: 'Bạn có muốn xóa nhân viên này không ?',
    },
    leaveType: {
      leaveTypeManagement: 'Quản lý loại phép',
      createLeaveType: 'Tạo mới loại phép',
      evidenceRequire: 'Có cần xác thực không',
      leaveDays: 'Số lượng ngày nghỉ',
      typeName: 'Tên loại Phép',
      editLeaveType: 'Chỉnh sửa loại phép',
      deleteLeaveType: 'Xóa loại phép',
      confirmDeleteLeaveType: 'Bạn có muốn xóa loại phép này không ?',
      abbreviation: 'Tên Viết tắt',
      both: 'Cả hai',
      noLeaveType: 'Không có loại phép',
    },
    common: {
      search: 'Tìm Kiếm',
      searchByEmail: 'Tìm Theo Thư Điện Tử',
      searchByName: 'Tìm Theo Tên',
      sortByType: 'Sắp xếp theo loại',
      image: 'Hình Ảnh',
      loadMore: 'Xem Nhiều hơn',
      delete: 'Xóa',
      edit: 'Chỉnh Sửa',
      cancel: 'Hủy Bỏ',
      description: 'Mô Tả',
      name: 'Tên',
      email: 'Thư Điện Tử',
      confirmDelete: 'Bạn Không Thể Đảo Ngược Sau Khi Xác Nhận',
      year: 'Năm',
      yes: 'Có',
      no: 'Không',
      status: 'Trạng Thái',
      noInfomation: 'Không có thông tin',
      confirmation: 'Xác Nhận',
      confirmContent: 'Bạn có chắc chắn muốn xóa mục này không ?',
      save: 'Lưu',
      create: 'Tạo',
      submit: 'Gửi Đi',
      reset: 'Đặt Lại',
      detail: 'Chi Tiết',
      back: 'Trở lại',
      date: 'Ngày',
      changeLanguage: 'Đổi Ngôn ngữ',
      aProductOf: 'Một sản phẩm của',
    },
    equipmentGroup: {
      active: 'Kích hoạt',
      inactive: 'Chưa kích hoạt',
      delete: 'Xóa nhóm thiết bị',
      add: 'Thêm nhóm thiết bị',
      edit: 'Sửa nhóm thiết bị',
      noData: 'Không có nhóm thiết bị nào.',
    },
    notify: {
      success: "Gửi yêu cầu thành công.",
      fail: "Gửi yêu cầu thất bại.",
    }
  },
}
