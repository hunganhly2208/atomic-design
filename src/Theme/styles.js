/*!

=========================================================
 * DSquare
=========================================================

 * Copyright 2018 DSquare

=========================================================

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

// ##############################
// Variables - Styles that are used on more than one component
// #############################

const primaryColor = '#055366'
const secondaryColor = '#f50057'
const warningColor = '#ff9800'
const whiteColor = '#fff'
const dangerColor = '#f44336'
const darkColor = '#000'
/* const warningColor = '#ff9800'
const successColor = '#4caf50'
const infoColor = '#00acc1'
const roseColor = '#e91e63'
const grayColor = '#999999' */

export {
  // variables
  primaryColor,
  secondaryColor,
  warningColor,
  whiteColor,
  dangerColor,
  darkColor,
  /* ,
  successColor,
  infoColor,
  roseColor,
  grayColor, */
}
