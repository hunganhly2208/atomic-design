import { createMuiTheme } from '@material-ui/core/styles'

import * as styleDefault from './styles'

const theme = createMuiTheme({
  typography: {
    // useNextVariants: true,
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  palette: {
    primary: {
      main: styleDefault.primaryColor,
    },
    secondary: {
      main: styleDefault.secondaryColor,
    },
  },
  overrides: {
    MuiOutlinedInput: {
      input: {
        padding: '16px 8px',
      },
    },
    MuiTable: {},
    MuiTableCell: {
      head: {
        background: styleDefault.primaryColor,
        color: styleDefault.whiteColor,
      },
      root: {
        padding: '8px 16px',
      },
    },
  },
})

export default theme
