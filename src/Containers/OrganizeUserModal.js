import React, { useEffect } from 'react'
import { func, number } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { UserActions } from 'Stores/User/Actions'
import { GlobalSelectors } from 'Stores/Global/Selectors'
import { UserSelectors } from 'Stores/User/Selectors'
import { withTranslation } from 'react-i18next'

import OrganizeUserModal from 'Components/organisms/OrganizeUserModal'

const OrganizeUserModalContainer = ({
  getUserOffices,
  clearUserOffices,
  getUserDepartments,
  clearUserDepartments,
  userId,
  organizeUser,
  ...props
}) => {
  // Effect get list user offices
  useEffect(() => {
    getUserOffices(userId)
    return () => clearUserOffices()
  }, [getUserOffices, clearUserOffices, userId])

  // Effect get list user departments
  useEffect(() => {
    getUserDepartments(userId)
    return () => clearUserDepartments()
  }, [userId, getUserDepartments, clearUserDepartments])

  const handleAction = values => {
    organizeUser(userId, values)
  }
  return <OrganizeUserModal handleAction={handleAction} {...props} />
}

const mapStateToProps = state => ({
  formAssignItem: UserSelectors.getFormAssignItem(state),
  listOffices: GlobalSelectors.getLocations(state),
  departmentOptions: GlobalSelectors.getDepartmentOptions(state),
})

const mapDispatchToProps = dispatch => ({
  // Get and clear user offices
  getUserOffices: userId => dispatch(UserActions.getUserOfficesRequest(userId)),
  clearUserOffices: () => dispatch(UserActions.clearUserOffices()),
  // Get and clear user departments
  getUserDepartments: userId =>
    dispatch(UserActions.getUserDepartmentsRequest(userId)),
  clearUserDepartments: () => dispatch(UserActions.clearUserDepartments()),
  // Organize user
  organizeUser: (userId, values) =>
    dispatch(UserActions.organizeUserRequest(userId, values)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

OrganizeUserModalContainer.propTypes = {
  getUserOffices: func.isRequired,
  clearUserOffices: func.isRequired,
  getUserDepartments: func.isRequired,
  clearUserDepartments: func.isRequired,
  userId: number.isRequired,
  organizeUser: func.isRequired,
}

export default compose(
  withConnect,
  withTranslation()
)(OrganizeUserModalContainer)
