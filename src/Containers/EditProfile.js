import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { func, object } from 'prop-types'
import 'Stores/Profile/Reducers'
import 'Stores/Profile/Sagas'
import 'Stores/User/Reducers'
import 'Stores/User/Sagas'
import { compose } from 'redux'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { ProfileActions } from 'Stores/Profile/Actions'
import { UserActions } from 'Stores/User/Actions'
import { useTitle } from 'Hooks/useTitle'
import { UserSelectors } from 'Stores/User/Selectors'
import { ProfileSelectors } from 'Stores/Profile/Selectors'
import { handleLinkExternal } from 'Utils/handleLinkExternal'
import { withTranslation } from 'react-i18next'

import EditProfile from 'Components/pages/EditProfile'

const EditProfileContainer = ({
  getProfileInfo,
  clearProfileInfo,
  getUserInfo,
  clearUserInfo,
  match,
  editProfile,
  ...props
}) => {
  // Set title for page
  useTitle(props.t('profile:editProfile'))
  let userId = match.params.userId
  // Get and clear profile info
  useEffect(() => {
    getProfileInfo(userId)
    return () => clearProfileInfo()
  }, [getProfileInfo, clearProfileInfo, userId])
  // Get and clear user info
  useEffect(() => {
    getUserInfo(userId)
    return () => clearUserInfo()
  }, [getUserInfo, clearUserInfo, userId])
  const handleGoBack = () => {
    handleLinkExternal(`/profile/${userId}`)()
  }
  const handleEditProfile = values => {
    editProfile(userId, values)
  }
  return (
    <EditProfile
      handleGoBack={handleGoBack}
      handleEditProfile={handleEditProfile}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  userInfo: UserSelectors.getUserInfo(state),
  profileInfo: ProfileSelectors.getProfileInfo(state),
  isLoadingItems: LoadingSelectors.getLoadingItems(state),
  formItem: ProfileSelectors.getFormItem(state),
  isLoadingAction: LoadingSelectors.getLoadingAction(state),
})

const mapDispatchToProps = dispatch => ({
  // Get and clear profile info
  getProfileInfo: userId =>
    dispatch(ProfileActions.getProfileInfoRequest(userId)),
  clearProfileInfo: () => dispatch(ProfileActions.clearProfileInfo()),
  // Get and clear user info
  getUserInfo: userId => dispatch(UserActions.getUserInfoRequest(userId)),
  clearUserInfo: () => dispatch(UserActions.clearUserInfo()),
  // Upload avatar
  uploadAvatar: (files, module, callback) =>
    dispatch(UserActions.uploadAvatarRequest(files, module, callback)),
  // edit profile
  editProfile: (userId, values) =>
    dispatch(ProfileActions.editProfileRequest(userId, values)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

EditProfileContainer.propTypes = {
  getProfileInfo: func.isRequired,
  clearProfileInfo: func.isRequired,
  getUserInfo: func.isRequired,
  clearUserInfo: func.isRequired,
  match: object.isRequired,
  editProfile: func.isRequired,
  t: func,
}

export default compose(
  withConnect,
  withTranslation()
)(EditProfileContainer)
