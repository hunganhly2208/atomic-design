import React from 'react'
import { func, string } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
// import { OfficeActions } from 'Stores/OfficeManagement/Actions'
import { EquipmentGroupsActions } from 'Stores/EquipmentGroups/Actions'
import { withTranslation } from 'react-i18next'
import CreateEditEquipmentGroupsModal from 'Components/organisms/CreateEditEquipmentGroupsModal'

const CreateEditEquipmentGroupsModalContainer = ({
  createEquipmentGroups,
  editEquipmentGroups,
  action,
  ...props
}) => {
  // Handle action create or edit
  const handleAction = values => {
    switch (action) {
      case 'create':
        createEquipmentGroups(values)
        break
      case 'edit':
        console.log("editEquipmentGroups===", values)
        editEquipmentGroups(values)
        break
      default:
        break
    }
  }

  return (
    <CreateEditEquipmentGroupsModal
      action={action}
      handleAction={handleAction}
      {...props}
    />
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  createEquipmentGroups: values => dispatch(EquipmentGroupsActions.createItemRequest(values)),
  editEquipmentGroups: values => dispatch(EquipmentGroupsActions.editItemRequest(values)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

CreateEditEquipmentGroupsModalContainer.propTypes = {
  createEquipmentGroups: func.isRequired,
  editEquipmentGroups: func.isRequired,
  action: string.isRequired,
}

export default compose(
  withConnect,
  withTranslation()
)(CreateEditEquipmentGroupsModalContainer)
