import React, { useEffect } from 'react'
import { func } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { OfficeActions } from 'Stores/OfficeManagement/Actions'
import { ModalActions } from 'Stores/Modal/Actions'
import { OfficeSelectors } from 'Stores/OfficeManagement/Selectors'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import OfficeTable from 'Components/organisms/OfficeTable'

const OfficeTableContainer = ({
  getListOffice,
  clearListOffice,
  setModal,
  ...props
}) => {
  // Get and clear list department
  useEffect(() => {
    getListOffice()
    return () => clearListOffice()
  }, [getListOffice, clearListOffice])
  // Handle Edit Department
  const handleEditOffice = item => () => {
    setModal('CreateEditOfficeModal', {
      action: 'edit',
      item: {
        id: item.get('id'),
        name: item.get('name'),
      },
    })
  }
  const handleAssignLeadOffice = officeId => () => {
    setModal('AssignLeadOfficeModal', {
      officeId,
    })
  }

  return (
    <OfficeTable
      handleEditOffice={handleEditOffice}
      handleAssignLeadOffice={handleAssignLeadOffice}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  items: OfficeSelectors.getItems(state),
  isLoadingList: LoadingSelectors.getLoadingList(state),
})

const mapDispatchToProps = dispatch => ({
  getListOffice: () => dispatch(OfficeActions.getItemsRequest()),
  clearListOffice: () => dispatch(OfficeActions.clearItems()),
  // Set modal
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

OfficeTableContainer.propTypes = {
  getListOffice: func.isRequired,
  clearListOffice: func.isRequired,
  setModal: func.isRequired,
}

export default compose(withConnect)(OfficeTableContainer)
