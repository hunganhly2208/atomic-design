import React from 'react'
import { func, string } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { LeaveTypeActions } from 'Stores/LeaveTypeManagement/Actions'
import { withTranslation } from 'react-i18next'
import CreateEditLeaveTypeModal from 'Components/organisms/CreateEditLeaveTypeModal'

const CreateEditLeaveTypeModalContainer = ({
  createLeaveType,
  editLeaveType,
  action,
  ...props
}) => {
  // Effect get user detail and user roles
  const handleAction = (values = {}) => {
    switch (action) {
      case 'create':
        createLeaveType(values)
        break
      case 'edit':
        editLeaveType(values)
        break
      default:
        break
    }
  }

  return (
    <CreateEditLeaveTypeModal
      action={action}
      handleAction={handleAction}
      {...props}
    />
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  createLeaveType: values =>
    dispatch(LeaveTypeActions.createItemRequest(values)),
  editLeaveType: values => dispatch(LeaveTypeActions.editItemRequest(values)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

CreateEditLeaveTypeModalContainer.propTypes = {
  createLeaveType: func.isRequired,
  editLeaveType: func.isRequired,
  action: string.isRequired,
}

export default compose(
  withConnect,
  withTranslation()
)(CreateEditLeaveTypeModalContainer)
