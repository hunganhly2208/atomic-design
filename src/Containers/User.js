import React from 'react'
import { connect } from 'react-redux'
import { func } from 'prop-types'
import 'Stores/User/Reducers'
import 'Stores/User/Sagas'
import { compose } from 'redux'
import { useTitle } from 'Hooks/useTitle'
import { withTranslation } from 'react-i18next'
import User from 'Components/pages/User'

const UserContainer = ({ ...props }) => {
  // Set title for page
  useTitle(props.t('user:userManagement'))
  return <User {...props} />
}
const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

UserContainer.propTypes = {
  t: func,
}

export default compose(
  withConnect,
  withTranslation()
)(UserContainer)
