import React from 'react'
import 'Stores/News/Reducers'
import 'Stores/News/Sagas'
import 'Stores/User/Reducers'
import 'Stores/User/Sagas'
import { func } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { useTitle } from 'Hooks/useTitle'
import { withTranslation } from 'react-i18next'

import AssignUserToCategory from 'Components/pages/AssignUserToCategory'

const AssignUserToCategoryContainer = ({ ...props }) => {
  useTitle(props.t('article:assignUserToCategory'))
  return <AssignUserToCategory {...props} />
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

AssignUserToCategoryContainer.propTypes = {
  t: func,
}

export default compose(
  withConnect,
  withTranslation()
)(AssignUserToCategoryContainer)
