import React, { useEffect } from 'react'
import { fromJS } from 'immutable'
import { connect } from 'react-redux'
import { func, object } from 'prop-types'
import { compose } from 'redux'
import { useTitle } from 'Hooks/useTitle'
import { ModalActions } from 'Stores/Modal/Actions'
import { EquipmentGroupsActions } from 'Stores/EquipmentGroups/Actions'
import { withTranslation } from 'react-i18next'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import 'Stores/EquipmentGroups/Reducers'
import 'Stores/EquipmentGroups/Sagas'
import EquipmentGroups from 'Components/pages/EquipmentGroups'
import { EquipmentGroupsSelectors } from 'Stores/EquipmentGroups/Selectors'

const EquipmentGroupsContainer = ({
  getListEquipmentGroup,
  clearListEquipmentGroup,
  setModal,
  setFilter,
  filter,
  ...props
}) => {
  // Get and clear list Equipment Group
  useEffect(() => {
    getListEquipmentGroup()
    return () => clearListEquipmentGroup()
  }, [getListEquipmentGroup, clearListEquipmentGroup, filter])
  // Handle Edit Equipment Group
  const handleAddEquipmentGroup = () => {
    setModal('CreateEditEquipmentGroupsModal', {
      action: 'create',
      item: { name: '', status: 1 },
    })
  }
  // Handle Edit Equipment Group
  const handleEditEquipmentGroup = item => () => {
    setModal('CreateEditEquipmentGroupsModal', {
      action: 'edit',
      item: {
        id: item.get('id'),
        name: item.get('name'),
        status: item.get('status'),
      },
    })
  }
  // Handle Delete Equipment Group
  const handleDeleteEquipmentGroup = id => () => {
    setModal('ConfirmationDialog', {
      title: props.t('equipmentGroup:delete'),
      content: props.t('common:confirmDelete'),
      type: 'deleteEquipmentGroup',
      id,
    })
  }
  // Hanlde change limit and page
  const handleChangePage = (e, activePage) =>
    setFilter({ page: activePage + 1 })
  const handleChangeRowsPerPage = e => setFilter({ limit: +e.target.value })
  return (
    <EquipmentGroups
      handleAddEquipmentGroup={handleAddEquipmentGroup}
      handleEditEquipmentGroup={handleEditEquipmentGroup}
      handleDeleteEquipmentGroup={handleDeleteEquipmentGroup}
      handleChangePage={handleChangePage}
      handleChangeRowsPerPage={handleChangeRowsPerPage}
      filter={filter}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  filter: EquipmentGroupsSelectors.getFilter(state),
  items: EquipmentGroupsSelectors.getItems(state),
  isLoadingList: LoadingSelectors.getLoadingList(state),
  totalCount: EquipmentGroupsSelectors.getTotalCount(state),
})

const mapDispatchToProps = dispatch => ({
  getListEquipmentGroup: () => dispatch(EquipmentGroupsActions.getItemsRequest()),
  clearListEquipmentGroup: () => dispatch(EquipmentGroupsActions.clearItems()),
  setFilter: filter => dispatch(EquipmentGroupsActions.setFilter(filter)),
  // Set modal
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

EquipmentGroupsContainer.propTypes = {
  getListEquipmentGroup: func.isRequired,
  clearListEquipmentGroup: func.isRequired,
  filter: object.isRequired,
  setModal: func.isRequired,
  setFilter: func.isRequired,
  t: func,
}

export default compose(
  withConnect,
  withTranslation()
)(EquipmentGroupsContainer)
