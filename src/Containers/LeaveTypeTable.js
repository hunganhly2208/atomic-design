import React, { useEffect } from 'react'
import { func } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { LeaveTypeActions } from 'Stores/LeaveTypeManagement/Actions'
import { ModalActions } from 'Stores/Modal/Actions'
import { LeaveTypeSelectors } from 'Stores/LeaveTypeManagement/Selectors'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import LeaveTypeTable from 'Components/organisms/LeaveTypeTable'

const LeaveTypeTableContainer = ({
  getListLeaveType,
  clearListLeaveType,
  setModal,
  ...props
}) => {
  // Get and clear list department
  useEffect(() => {
    getListLeaveType()
    return () => clearListLeaveType()
  }, [getListLeaveType, clearListLeaveType])
  // Handle Delete Holiday
  const handleDeleteLeaveType = id => () => {
    setModal('ConfirmationDialog', {
      title: props.t('leaveType:deleteLeaveType'),
      content: props.t('leaveType:confirmDeleteLeaveType'),
      type: 'deleteLeaveType',
      id,
    })
  }

  const handleEditLeaveType = item => () => {
    const formItem = {
      id: item.get('typeId'),
      typeName: item.get('typeName') || '',
      description: item.get('description') || '',
      gender: item.get('gender') || '',
    }
    setModal('CreateEditLeaveTypeModal', {
      action: 'edit',
      item: formItem,
    })
  }
  return (
    <LeaveTypeTable
      handleEditLeaveType={handleEditLeaveType}
      handleDeleteLeaveType={handleDeleteLeaveType}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  items: LeaveTypeSelectors.getItems(state),
  isLoadingList: LoadingSelectors.getLoadingList(state),
})

const mapDispatchToProps = dispatch => ({
  getListLeaveType: () => dispatch(LeaveTypeActions.getItemsRequest()),
  clearListLeaveType: () => dispatch(LeaveTypeActions.clearItems()),
  // Set modal
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

LeaveTypeTableContainer.propTypes = {
  getListLeaveType: func.isRequired,
  clearListLeaveType: func.isRequired,
  setModal: func.isRequired,
  t: func,
}

export default compose(withConnect)(LeaveTypeTableContainer)
