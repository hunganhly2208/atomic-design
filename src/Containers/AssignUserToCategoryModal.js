import React from 'react'
import { func, string, number } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { NewActions } from 'Stores/News/Actions'
import { GlobalSelectors } from 'Stores/Global/Selectors'
import { GlobalActions } from 'Stores/Global/Actions'
import { withTranslation } from 'react-i18next'

import AssignUserToCategoryModal from 'Components/organisms/AssignUserToCategoryModal'

const AssignUserToCategoryModalContainer = ({
  handleSearchUser,
  assignLeaders,
  assignUser,
  ...props
}) => {
  // Effect get list leader
  const handleSearch = (str, { action }) => {
    if (action === 'input-blur' || action === 'menu-close') {
      return
    }
    handleSearchUser(str)
  }
  const handleAction = values => {
    assignUser(values)
  }
  return (
    <AssignUserToCategoryModal
      handleSearch={handleSearch}
      handleAction={handleAction}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  searchUserOptions: GlobalSelectors.getSearchUserOptions(state),
})

const mapDispatchToProps = dispatch => ({
  handleSearchUser: keyword =>
    dispatch(GlobalActions.searchUsersRequest(keyword)),
  clearSearchUsers: () => dispatch(GlobalActions.clearSearchUsers()),
  // Assign user
  assignUser: values =>
    dispatch(NewActions.assignUserToCategoryRequest(values)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

AssignUserToCategoryModalContainer.propTypes = {
  getLeaders: func.isRequired,
  clearLeaders: func.isRequired,
  officeId: number.isRequired,
  handleSearchUser: func.isRequired,
  assignLeaders: func.isRequired,
  assignUser: func.isRequired,
}

export default compose(
  withConnect,
  withTranslation()
)(AssignUserToCategoryModalContainer)
