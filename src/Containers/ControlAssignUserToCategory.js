import React, { useState } from 'react'
import { func, object } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { NewActions } from 'Stores/News/Actions'
import { ModalActions } from 'Stores/Modal/Actions'
import { NewSelectors } from 'Stores/News/Selectors'
import ControlAssignUserToCategory from 'Components/organisms/ControlAssignUserToCategory'

const ControlAssignUserToCategoryContainer = ({
  setModal,
  setFilter,
  filter,
  categoryId,
  ...props
}) => {
  const [keyword, setKeyword] = useState(filter.get('keyword'))
  const handleSearch = () => {
    const filter = {
      keyword,
      page: 1,
    }
    setFilter(filter)
  }
  // Handle open modal create user
  const handleAssignUser = () => {
    let item = {
      email: true,
      post: true,
      view: true,
      categoryId: categoryId,
      userId: '',
    }
    setModal('AssignUserToCategoryModal', {
      action: 'create',
      item,
    })
  }
  return (
    <ControlAssignUserToCategory
      keyword={keyword}
      setKeyword={setKeyword}
      handleSearch={handleSearch}
      handleAssignUser={handleAssignUser}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  filter: NewSelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(NewActions.setFilter(filter)),
  // Set Modal
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

ControlAssignUserToCategoryContainer.propTypes = {
  setFilter: func.isRequired,
  setModal: func.isRequired,
  filter: object.isRequired,
}

export default compose(withConnect)(ControlAssignUserToCategoryContainer)
