import React, { useEffect } from 'react'
import { func, object } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { HolidayActions } from 'Stores/HolidayManagement/Actions'
import { ModalActions } from 'Stores/Modal/Actions'
import { HolidaySelectors } from 'Stores/HolidayManagement/Selectors'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import HolidayTable from 'Components/organisms/HolidayTable'

const HolidayTableContainer = ({
  getListHoliday,
  clearListHoliday,
  setModal,
  filter,
  ...props
}) => {
  // Get and clear list department
  useEffect(() => {
    getListHoliday()
    return () => clearListHoliday()
  }, [getListHoliday, clearListHoliday, filter.get('year')])

  // Handle Delete Holiday
  const handeDeleteHoliday = id => () => {
    setModal('ConfirmationDialog', {
      title: props.t('user:deleteHoliday'),
      content: props.t('user:confirmDeleteHoliday'),
      type: 'deleteHoliday',
      id,
    })
  }

  return <HolidayTable handeDeleteHoliday={handeDeleteHoliday} {...props} />
}

const mapStateToProps = state => ({
  items: HolidaySelectors.getItems(state),
  isLoadingList: LoadingSelectors.getLoadingList(state),
  filter: HolidaySelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  getListHoliday: () => dispatch(HolidayActions.getItemsRequest()),
  clearListHoliday: () => dispatch(HolidayActions.clearItems()),
  // Set modal
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

HolidayTableContainer.propTypes = {
  getListHoliday: func.isRequired,
  clearListHoliday: func.isRequired,
  setModal: func.isRequired,
  filter: object.isRequired,
  t: func,
}

export default compose(withConnect)(HolidayTableContainer)
