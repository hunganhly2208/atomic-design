import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'
import { compose } from 'redux'
import { AuthActions } from 'Stores/Authentication/Actions'
import { AuthSelectors } from '../Stores/Authentication/Selectors'
import { GlobalActions } from 'Stores/Global/Actions'
import { getViewMode } from 'Utils/getViewMode'
import useWindowSize from 'Hooks/useWindowSize'
import AuthTemplate from 'Components/templates/AuthTemplate'

// View Mode
// 1: desktop
// 2: tablet
// 3: mobile

const AuthTemplateContainer = ({ getGlobalData, getUserRoles, ...props }) => {
  const [isShowDrawer, setShowDrawer] = useState(false)
  const [viewMode, setViewMode] = useState(1)
  const [isShowProfileDrawer, setShowProfileDrawer] = useState(false)
  // Effect check view mode
  let size = useWindowSize()
  useEffect(() => {
    setViewMode(getViewMode(size.width))
  }, [size.width])
  // Effect get user Roles
  useEffect(() => {
    getUserRoles()
    return () => {
      // Clear user data here
    }
  }, [getUserRoles])
  // Effect get Global Data
  useEffect(() => {
    getGlobalData()
  }, [getGlobalData])
  // Menu drawer
  const handleShowDrawer = () => setShowDrawer(true)
  const handleHideDrawer = () => setShowDrawer(false)
  // Profile drawer
  const handleShowProfileDrawer = () => setShowProfileDrawer(true)
  const handleHideProfileDrawer = () => setShowProfileDrawer(false)
  return (
    <AuthTemplate
      isShowDrawer={isShowDrawer}
      handleShowDrawer={handleShowDrawer}
      handleHideDrawer={handleHideDrawer}
      viewMode={viewMode}
      isShowProfileDrawer={isShowProfileDrawer}
      handleShowProfileDrawer={handleShowProfileDrawer}
      handleHideProfileDrawer={handleHideProfileDrawer}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  userData: AuthSelectors.getUserData(state),
})

const mapDispatchToProps = dispatch => ({
  doLogout: () => dispatch(AuthActions.doLogout()),
  getUserRoles: () => dispatch(AuthActions.getRolesRequest()),
  getGlobalData: () => dispatch(GlobalActions.getGlobalDataRequest()),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

AuthTemplateContainer.propTypes = {
  getUserRoles: PropTypes.func,
  getGlobalData: PropTypes.func,
}

export default compose(withConnect)(AuthTemplateContainer)
