import React from 'react'
import { connect } from 'react-redux'
import { func } from 'prop-types'
import 'Stores/OfficeManagement/Reducers'
import 'Stores/OfficeManagement/Sagas'
import { compose } from 'redux'
import { useTitle } from 'Hooks/useTitle'
import { ModalActions } from 'Stores/Modal/Actions'
import { withTranslation } from 'react-i18next'
import OfficeManagement from 'Components/pages/OfficeMangement'

const OfficeManagementContainer = ({ setModal, ...props }) => {
  // Set title for page
  useTitle(props.t('user:officeManagement'))
  const handleOpenModalCreate = () => {
    const item = {
      name: '',
    }
    setModal('CreateEditOfficeModal', {
      action: 'create',
      item,
    })
  }

  return (
    <OfficeManagement
      handleOpenModalCreate={handleOpenModalCreate}
      {...props}
    />
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

OfficeManagementContainer.propTypes = {
  setModal: func.isRequired,
  t: func,
}

export default compose(
  withConnect,
  withTranslation()
)(OfficeManagementContainer)
