import React from 'react'
import { func, string } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { OfficeActions } from 'Stores/OfficeManagement/Actions'
import { withTranslation } from 'react-i18next'
import CreateEditOfficeModal from 'Components/organisms/CreateEditOfficeModal'

const CreateEditOfficeModalContainer = ({
  createOffice,
  editOffice,
  action,
  ...props
}) => {
  // Effect get user detail and user roles
  const handleAction = values => {
    switch (action) {
      case 'create':
        createOffice(values)
        break
      case 'edit':
        editOffice(values)
        break
      default:
        break
    }
  }

  return (
    <CreateEditOfficeModal
      action={action}
      handleAction={handleAction}
      {...props}
    />
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  createOffice: values => dispatch(OfficeActions.createItemRequest(values)),
  editOffice: values => dispatch(OfficeActions.editItemRequest(values)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

CreateEditOfficeModalContainer.propTypes = {
  createOffice: func.isRequired,
  editOffice: func.isRequired,
  action: string.isRequired,
}

export default compose(
  withConnect,
  withTranslation()
)(CreateEditOfficeModalContainer)
