import React, { useEffect } from 'react'
import { func, object } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { ModalActions } from 'Stores/Modal/Actions'
// import { UserActions } from 'Stores/User/Actions'
// import { UserSelectors } from 'Stores/User/Selectors'
// import UserTable from 'Components/organisms/UserTable'
import { EquipmentActions } from 'Stores/Equipment/Actions'
import { EquipmentSelectors } from 'Stores/Equipment/Selectors'
import EquipmentTable from 'Components/organisms/EquipmentTable'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import 'Stores/Equipment/Reducers'
import 'Stores/Equipment/Sagas'


const EquipmentTableContainer = ({
  clearListEquipment,
  getListEquipment,
  filter,
  setModal,
  setFilter,
  ...props
}) => {
  // Get list Equipment
  useEffect(() => {
    getListEquipment()
    return () => clearListEquipment()
  }, [getListEquipment, clearListEquipment, filter])

  // Handle Edit Equipment
  const handleEditEquipment = EquipmentId => () => {
    console.log('handleEditEquipment', EquipmentId)
    // setModal('CreateEditUserModal', {
    //   action: 'edit',
    //   EquipmentId,
    // })
  }
  // Handle Delete User
  const handleDeleteEquipment = id => () => {
    setModal('ConfirmationDialog', {
      title: props.t('user:deleteUser'),
      content: props.t('user:confirmDeleteUser'),
      type: 'deleteUser',
      id,
    })
  }
  // Hanlde change limit and page
  const handleChangePage = (e, activePage) =>
    setFilter({ page: activePage + 1 })
  const handleChangeRowsPerPage = e => setFilter({ limit: +e.target.value })
  return (
    <EquipmentTable
      handleDeleteEquipment={handleDeleteEquipment}
      handleEditEquipment={handleEditEquipment}
      filter={filter}
      handleChangePage={handleChangePage}
      handleChangeRowsPerPage={handleChangeRowsPerPage}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  filter: EquipmentSelectors.getFilter(state),
  items: EquipmentSelectors.getItems(state),
  isLoadingList: LoadingSelectors.getLoadingList(state),
  totalCount: EquipmentSelectors.getTotalCount(state),
})

const mapDispatchToProps = dispatch => ({
  getListEquipment: () => dispatch(EquipmentActions.getItemsRequest()),
  clearListEquipment: () => dispatch(EquipmentActions.clearItems()),
  setFilter: filter => dispatch(EquipmentActions.setFilter(filter)),
  // Set modal
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

EquipmentTableContainer.propTypes = {
  clearListEquipment: func.isRequired,
  getListEquipment: func.isRequired,
  filter: object.isRequired,
  setModal: func.isRequired,
  setFilter: func.isRequired,
  t: func,
}

export default compose(withConnect)(EquipmentTableContainer)
