import React, { useEffect } from 'react'
import { func, string, number, object } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { UserActions } from 'Stores/User/Actions'
import { UserSelectors } from 'Stores/User/Selectors'
import { GlobalSelectors } from 'Stores/Global/Selectors'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { withTranslation } from 'react-i18next'

import CreateEditUserModal from 'Components/organisms/CreateEditUserModal'

const CreateEditUserModalContainer = ({
  createUser,
  editUser,
  action,
  getUserInfo,
  getUserRoles,
  userId,
  clearUserInfo,
  clearUserRoles,
  formItem,
  item,
  getUserDepartments,
  getUserOffices,
  clearUserDepartments,
  clearUserOffices,
  ...props
}) => {
  // Effect get user detail and user roles
  useEffect(() => {
    // Check if action is edit user
    if (userId && action === 'edit') {
      // Get user's info and user's roles
      getUserInfo(userId)

      // getUserRoles(userId)
      return () => {
        clearUserInfo()

        // clearUserRoles()
      }
    }
  }, [getUserInfo, getUserRoles, userId])
  const handleAction = values => {
    switch (action) {
      case 'create':
        createUser(values)
        break
      case 'edit':
        editUser(values)
        break
      default:
        break
    }
  }
  // Show and hide modal crop avatar

  return (
    <CreateEditUserModal
      action={action}
      handleAction={handleAction}
      item={action === 'edit' ? formItem : item}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  formItem: UserSelectors.getFormItem(state),
  locationOptions: GlobalSelectors.getLocationOptions(state),
  departmentOptions: GlobalSelectors.getDepartmentOptions(state),
  isLoadingItems: LoadingSelectors.getLoadingItems(state),
})

const mapDispatchToProps = dispatch => ({
  createUser: values => dispatch(UserActions.createItemRequest(values)),
  editUser: values => dispatch(UserActions.editItemRequest(values)),
  // Get user info
  getUserInfo: id => dispatch(UserActions.getUserInfoRequest(id)),
  clearUserInfo: () => dispatch(UserActions.clearUserInfo()),
  // Get user's roles
  getUserRoles: id => dispatch(UserActions.getUserRolesRequest(id)),
  clearUserRoles: () => dispatch(UserActions.clearUserRoles()),
  // Upload avatar
  uploadAvatar: (files, module, callback) =>
    dispatch(UserActions.uploadAvatarRequest(files, module, callback)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

CreateEditUserModalContainer.propTypes = {
  createUser: func.isRequired,
  editUser: func.isRequired,
  action: string.isRequired,
  getUserInfo: func,
  getUserRoles: func,
  userId: number,
  clearUserInfo: func,
  clearUserRoles: func,
  formItem: object,
  item: object,
  getUserDepartments: func,
  getUserOffices: func,
  clearUserDepartments: func,
  clearUserOffices: func,
}

export default compose(
  withConnect,
  withTranslation()
)(CreateEditUserModalContainer)
