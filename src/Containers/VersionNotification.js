import React, { useState, useEffect } from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { string, func } from 'prop-types'

import VersionNotification from 'Components/molecules/VersionNotification'
import { GlobalSelectors } from 'Stores/Global/Selectors'
import { GlobalActions } from 'Stores/Global/Actions'
import { Config } from 'Config'

const VersionNotificationContainer = ({
  currentVersion,
  setVersion,
  ...props
}) => {
  const [isOpen, setOpen] = useState(false)
  // Check current version save in localStorage vs version in config file
  useEffect(() => {
    if (currentVersion !== Config.version) {
      setOpen(true)
      setVersion(Config.version)
    }
  }, [currentVersion, setVersion])
  const onClose = () => {
    setOpen(false)
  }
  const message = `The software was upgraded to a new version ${Config.version}`
  return (
    <VersionNotification
      isOpen={isOpen}
      onClose={onClose}
      message={message}
      {...props}
    />
  )
}

VersionNotificationContainer.propTypes = {
  currentVersion: string.isRequired,
  setVersion: func.isRequired,
}

const mapStateToProps = state => {
  return {
    currentVersion: GlobalSelectors.getCurrentVersion(state),
  }
}

const mapDispatchToProps = dispatch => ({
  setVersion: version => dispatch(GlobalActions.setVersion(version)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

export default compose(withConnect)(VersionNotificationContainer)
