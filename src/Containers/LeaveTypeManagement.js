import React from 'react'
import { connect } from 'react-redux'
import { func } from 'prop-types'
import 'Stores/LeaveTypeManagement/Reducers'
import 'Stores/LeaveTypeManagement/Sagas'
import { compose } from 'redux'
import { useTitle } from 'Hooks/useTitle'
import { ModalActions } from 'Stores/Modal/Actions'
import { withTranslation } from 'react-i18next'
import LeaveTypeManagement from 'Components/pages/LeaveTypeManagement'

const LeaveTypeManagementContainer = ({ setModal, ...props }) => {
  // Set title for page
  useTitle(props.t('leaveType:leaveTypeManagement'))
  const handleOpenModalCreate = () => {
    const item = {
      typeName: '',
      description: '',
      gender: '',
    }
    setModal('CreateEditLeaveTypeModal', {
      action: 'create',
      item,
    })
  }

  return (
    <LeaveTypeManagement
      handleOpenModalCreate={handleOpenModalCreate}
      {...props}
    />
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

LeaveTypeManagementContainer.propTypes = {
  setModal: func.isRequired,
  t: func,
}

export default compose(
  withConnect,
  withTranslation()
)(LeaveTypeManagementContainer)
