import React from 'react'
import { connect } from 'react-redux'
import { func } from 'prop-types'
import 'Stores/DepartmentManagement/Reducers'
import 'Stores/DepartmentManagement/Sagas'
import { compose } from 'redux'
import { useTitle } from 'Hooks/useTitle'
import { ModalActions } from 'Stores/Modal/Actions'
import { withTranslation } from 'react-i18next'
import DeparmentManagement from 'Components/pages/DepartmentManagement'

const DeparmentManagementContainer = ({ setModal, ...props }) => {
  // Set title for page
  useTitle(props.t('user:departmentManagement'))
  const handleOpenModalCreate = () => {
    const item = {
      name: '',
    }
    setModal('CreateEditDepartmentModal', {
      action: 'create',
      item,
    })
  }
  return (
    <DeparmentManagement
      handleOpenModalCreate={handleOpenModalCreate}
      {...props}
    />
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

DeparmentManagementContainer.propTypes = {
  setModal: func.isRequired,
  t: func,
}

export default compose(
  withConnect,
  withTranslation()
)(DeparmentManagementContainer)
