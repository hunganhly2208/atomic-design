import React, { useEffect } from 'react'
import { func, object, number } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { NewActions } from 'Stores/News/Actions'
import { NewSelectors } from 'Stores/News/Selectors'
import { ModalActions } from 'Stores/Modal/Actions'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import { withRouter } from 'react-router-dom'
import AssignUserToCategoryTable from 'Components/organisms/AssignUserToCategoryTable'

const AssignUserToCategoryTableContainer = ({
  clearListCategoryUser,
  getListCategoryUser,
  filter,
  setModal,
  setFilter,
  categoryId,
  ...props
}) => {
  // Get list User
  useEffect(() => {
    getListCategoryUser(categoryId)
    return () => clearListCategoryUser()
  }, [getListCategoryUser, clearListCategoryUser, filter, categoryId])

  // Handle Edit user
  const handleEditCategoryUser = info => () => {
    let item = {
      categoryId: info.get('categoryId'),
      view: Boolean(info.get('view')),
      userName: info.get('userName'),
      post: Boolean(info.get('post')),
      email: Boolean(info.get('email')),
      userId: { value: info.get('userId') },
    }
    setModal('AssignUserToCategoryModal', {
      action: 'edit',
      item,
    })
  }
  // Handle Delete category user
  const handleDeleteCategoryUser = id => () => {
    setModal('ConfirmationDialog', {
      title: props.t('article:removeUserFromCategory'),
      content: props.t('article:confirmRemove'),
      type: 'removeCategoryUser',
      id,
    })
  }

  // Hanlde change limit and page
  const handleChangePage = (e, activePage) =>
    setFilter({ page: activePage + 1 })
  const handleChangeRowsPerPage = e => setFilter({ limit: +e.target.value })
  return (
    <AssignUserToCategoryTable
      handleDeleteCategoryUser={handleDeleteCategoryUser}
      handleEditCategoryUser={handleEditCategoryUser}
      filter={filter}
      handleChangePage={handleChangePage}
      handleChangeRowsPerPage={handleChangeRowsPerPage}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  filter: NewSelectors.getFilter(state),
  items: NewSelectors.getCategoryUsersWithInfos(state),
  isLoadingList: LoadingSelectors.getLoadingList(state),
  totalCount: NewSelectors.getTotalCount(state),
})

const mapDispatchToProps = dispatch => ({
  getListCategoryUser: categoryId =>
    dispatch(NewActions.getListUserOfCategoryRequest(categoryId)),
  clearListCategoryUser: () => dispatch(NewActions.clearListUserOfCategory()),
  setFilter: filter => dispatch(NewActions.setFilter(filter)),
  // Set modal
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

AssignUserToCategoryTableContainer.propTypes = {
  clearListCategoryUser: func.isRequired,
  getListCategoryUser: func.isRequired,
  filter: object.isRequired,
  setModal: func.isRequired,
  setFilter: func.isRequired,
  categoryId: number.isRequired,
  t: func,
}

export default compose(
  withConnect,
  withRouter
)(AssignUserToCategoryTableContainer)
