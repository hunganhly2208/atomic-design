import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { UserActions } from 'Stores/User/Actions'
import { HolidayActions } from 'Stores/HolidayManagement/Actions'
import { NewActions } from 'Stores/News/Actions'
import { LeaveTypeActions } from 'Stores/LeaveTypeManagement/Actions'
import { EquipmentGroupsActions } from 'Stores/EquipmentGroups/Actions'
import { withTranslation } from 'react-i18next'
import ConfirmationDialog from '../Components/molecules/ConfirmationDialog'

const ConfirmationDialogContainer = ({
  type,
  id,
  deleteUser,
  deleteHoliday,
  deleteCategoryUser,
  deleteLeaveType,
  deleteEquipmentGroup,
  ...props
}) => {
  const handleAction = () => {
    switch (type) {
      case 'deleteUser':
        deleteUser(id)
        break
      case 'deleteHoliday':
        deleteHoliday(id)
        break
      case 'removeCategoryUser':
        deleteCategoryUser(id)
        break
      case 'deleteLeaveType':
        deleteLeaveType(id)
        break
      case 'deleteEquipmentGroup':
        deleteEquipmentGroup(id)
        break
      default:
        break
    }
  }
  return <ConfirmationDialog onConfirm={handleAction} {...props} />
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  // Delete user
  deleteUser: id => dispatch(UserActions.deleteItemRequest(id)),
  // Delete Holiday
  deleteHoliday: id => dispatch(HolidayActions.deleteItemRequest(id)),
  // Delete category user
  deleteCategoryUser: id =>
    dispatch(NewActions.deleteUserFromCategoryRequest(id)),
  // Delete leave type
  deleteLeaveType: id => dispatch(LeaveTypeActions.deleteItemRequest(id)),
  // Delete equipment group
  deleteEquipmentGroup: id => dispatch(EquipmentGroupsActions.deleteItemRequest(id)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

ConfirmationDialogContainer.propTypes = {
  type: PropTypes.string,
  id: PropTypes.number,
  deleteProduct: PropTypes.func,
  deleteUser: PropTypes.func,
  deleteHoliday: PropTypes.func,
  deleteCategoryUser: PropTypes.func,
  deleteLeaveType: PropTypes.func,
  deleteEquipmentGroup: PropTypes.func,
}

export default compose(
  withConnect,
  withTranslation()
)(ConfirmationDialogContainer)
