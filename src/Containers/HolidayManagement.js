import React from 'react'
import { connect } from 'react-redux'
import { func } from 'prop-types'
import 'Stores/HolidayManagement/Reducers'
import 'Stores/HolidayManagement/Sagas'
import { compose } from 'redux'
import { useTitle } from 'Hooks/useTitle'
import { ModalActions } from 'Stores/Modal/Actions'
import { HolidayActions } from 'Stores/HolidayManagement/Actions'
import { HolidaySelectors } from 'Stores/HolidayManagement/Selectors'
import { withTranslation } from 'react-i18next'
import HolidayManagement from 'Components/pages/HolidayManagement'

const HolidayManagementContainer = ({ setModal, setFilter, ...props }) => {
  // Set title for page
  useTitle(props.t('user:holidayManagement'))
  const handleOpenModalCreate = () => {
    const item = {
      name: '',
      date: null,
    }
    setModal('CreateEditHolidayModal', {
      action: 'create',
      item,
    })
  }
  const handleChangeYear = year => () => {
    setFilter({ year })
  }

  return (
    <HolidayManagement
      handleOpenModalCreate={handleOpenModalCreate}
      handleChangeYear={handleChangeYear}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  filter: HolidaySelectors.getFilter(state),
})

const mapDispatchToProps = dispatch => ({
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
  // Set filter
  setFilter: filter => dispatch(HolidayActions.setFilter(filter)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

HolidayManagementContainer.propTypes = {
  setModal: func.isRequired,
  setFilter: func.isRequired,
  t: func,
}

export default compose(
  withConnect,
  withTranslation()
)(HolidayManagementContainer)
