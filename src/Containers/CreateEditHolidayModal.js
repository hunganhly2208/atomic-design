import React from 'react'
import { func, string } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { HolidayActions } from 'Stores/HolidayManagement/Actions'
import { withTranslation } from 'react-i18next'
import CreateEditHolidayModal from 'Components/organisms/CreateEditHolidayModal'

const CreateEditHolidayModalContainer = ({
  createHoliday,
  action,
  ...props
}) => {
  const handleAction = values => {
    switch (action) {
      case 'create':
        createHoliday(values)
        break
      default:
        break
    }
  }
  return (
    <CreateEditHolidayModal
      action={action}
      handleAction={handleAction}
      {...props}
    />
  )
}

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  createHoliday: values => dispatch(HolidayActions.createItemRequest(values)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

CreateEditHolidayModalContainer.propTypes = {
  createHoliday: func.isRequired,
  action: string.isRequired,
}

export default compose(
  withConnect,
  withTranslation()
)(CreateEditHolidayModalContainer)
