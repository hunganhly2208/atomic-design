import React, { useEffect } from 'react'
import { func, object } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { DepartmentActions } from 'Stores/DepartmentManagement/Actions'
import { ModalActions } from 'Stores/Modal/Actions'
import { DepartmentSelectors } from 'Stores/DepartmentManagement/Selectors'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import DepartmentTable from 'Components/organisms/DepartmentTable'

const DepartmentTableContainer = ({
  getListDepartment,
  clearListDepartment,
  setModal,
  filter,
  setFilter,
  ...props
}) => {
  // Get and clear list department
  useEffect(() => {
    getListDepartment()
  }, [getListDepartment, filter])
  // Handle Edit Department
  const handleEditDepartment = item => () => {
    setModal('CreateEditDepartmentModal', {
      action: 'edit',
      item: {
        id: item.get('id'),
        name: item.get('name'),
      },
    })
  }

  const handleLoadMore = () => {
    setFilter({ page: filter.get('page') + 1 })
  }

  const handleAssignLeadDepartment = departmentId => () => {
    setModal('AssignLeadDepartmentModal', {
      departmentId,
    })
  }

  return (
    <DepartmentTable
      handleAssignLeadDepartment={handleAssignLeadDepartment}
      handleEditDepartment={handleEditDepartment}
      handleLoadMore={handleLoadMore}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  items: DepartmentSelectors.getItems(state),
  isLoadingList: LoadingSelectors.getLoadingList(state),
  filter: DepartmentSelectors.getFilter(state),
  isShowMore: DepartmentSelectors.getShowMore(state),
})

const mapDispatchToProps = dispatch => ({
  getListDepartment: () => dispatch(DepartmentActions.getItemsRequest()),
  clearListDepartment: () => dispatch(DepartmentActions.clearItems()),
  // Set modal
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
  // Set Filter
  setFilter: filter => dispatch(DepartmentActions.setFilter(filter)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

DepartmentTableContainer.propTypes = {
  getListDepartment: func.isRequired,
  clearListDepartment: func.isRequired,
  setModal: func.isRequired,
  filter: object.isRequired,
  setFilter: func.isRequired,
}

export default compose(withConnect)(DepartmentTableContainer)
