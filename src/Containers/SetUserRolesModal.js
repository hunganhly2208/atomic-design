import React, { useEffect } from 'react'
import { func, number } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { UserActions } from 'Stores/User/Actions'
import { GlobalSelectors } from 'Stores/Global/Selectors'
import { UserSelectors } from 'Stores/User/Selectors'
import { withTranslation } from 'react-i18next'
import SetUserRolesModal from 'Components/organisms/SetUserRolesModal'

const SetUserRolesModalContainer = ({
  getUserRoles,
  clearUserRoles,
  userId,
  setUserRoles,
  ...props
}) => {
  // Effect get list user Roles
  useEffect(() => {
    getUserRoles(userId)
    return () => clearUserRoles()
  }, [userId, getUserRoles, clearUserRoles])

  const handleAction = values => {
    setUserRoles(userId, values)
  }
  return <SetUserRolesModal handleAction={handleAction} {...props} />
}

const mapStateToProps = state => ({
  listRoles: GlobalSelectors.getRoles(state),
  setRolesItem: UserSelectors.getSetRolesItem(state),
})

const mapDispatchToProps = dispatch => ({
  getUserRoles: userId => dispatch(UserActions.getUserRolesRequest(userId)),
  clearUserRoles: () => dispatch(UserActions.clearUserRoles()),
  // Set user roles
  setUserRoles: (userId, values) =>
    dispatch(UserActions.setUserRolesRequest(userId, values)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

SetUserRolesModalContainer.propTypes = {
  getUserRoles: func.isRequired,
  clearUserRoles: func.isRequired,
  userId: number.isRequired,
  setUserRoles: func.isRequired,
}

export default compose(
  withConnect,
  withTranslation()
)(SetUserRolesModalContainer)
