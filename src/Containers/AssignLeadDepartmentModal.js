import React, { useEffect } from 'react'
import { func, string, number } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { DepartmentActions } from 'Stores/DepartmentManagement/Actions'
import { GlobalSelectors } from 'Stores/Global/Selectors'
import { GlobalActions } from 'Stores/Global/Actions'
import { DepartmentSelectors } from 'Stores/DepartmentManagement/Selectors'
import { withTranslation } from 'react-i18next'
import AssignLeadDepartmentModal from 'Components/organisms/AssignLeadDepartmentModal'

const AssignLeadDepartmentModalContainer = ({
  clearLeaders,
  getLeaders,
  departmentId,
  handleSearchUser,
  assignLeaders,
  ...props
}) => {
  // Effect get list leader
  useEffect(() => {
    getLeaders(departmentId)
    return () => clearLeaders()
  }, [getLeaders, clearLeaders, departmentId])
  const handleSearch = (str, actions) => {
    handleSearchUser(str)
  }
  const handleAction = values => {
    assignLeaders(departmentId, values)
  }
  return (
    <AssignLeadDepartmentModal
      handleSearch={handleSearch}
      handleAction={handleAction}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  searchUserOptions: GlobalSelectors.getSearchUserOptions(state),
  assignItem: DepartmentSelectors.getFormAssignItem(state),
})

const mapDispatchToProps = dispatch => ({
  handleSearchUser: keyword =>
    dispatch(GlobalActions.searchUsersRequest(keyword)),
  clearSearchUsers: () => dispatch(GlobalActions.clearSearchUsers()),
  // Get and clear leaders of office
  getLeaders: departmentId =>
    dispatch(DepartmentActions.getLeadersRequest(departmentId)),
  clearLeaders: () => dispatch(DepartmentActions.clearLeaders()),
  // Assign leader
  assignLeaders: (departmentId, values) =>
    dispatch(DepartmentActions.assignLeadersRequest(departmentId, values)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

AssignLeadDepartmentModalContainer.propTypes = {
  getLeaders: func.isRequired,
  clearLeaders: func.isRequired,
  departmentId: number.isRequired,
  handleSearchUser: func.isRequired,
  assignLeaders: func.isRequired,
}

export default compose(
  withConnect,
  withTranslation()
)(AssignLeadDepartmentModalContainer)
