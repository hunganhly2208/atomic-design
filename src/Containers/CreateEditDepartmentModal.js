import React from "react";
import { func, string } from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { DepartmentActions } from "Stores/DepartmentManagement/Actions";
import { withTranslation } from "react-i18next";
import CreateEditDepartmentModal from "Components/organisms/CreateEditDepartmentModal";

const CreateEditDepartmentModalContainer = ({
  createDepartment,
  editDepartment,
  action,
  ...props
}) => {
  // Effect get user detail and user roles
  const handleAction = values => {
    switch (action) {
      case "create":
        createDepartment(values);
        break;
      case "edit":
        editDepartment(values);
        break;
      default:
        break;
    }
  };

  return (
    <CreateEditDepartmentModal
      action={action}
      handleAction={handleAction}
      {...props}
    />
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  createDepartment: values =>
    dispatch(DepartmentActions.createItemRequest(values)),
  editDepartment: values => dispatch(DepartmentActions.editItemRequest(values))
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

CreateEditDepartmentModalContainer.propTypes = {
  createDepartment: func.isRequired,
  editDepartment: func.isRequired,
  action: string.isRequired
};

export default compose(
  withConnect,
  withTranslation()
)(CreateEditDepartmentModalContainer);
