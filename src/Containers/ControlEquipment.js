import React, { useState } from 'react'
import { func, object } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { EquipmentActions } from 'Stores/Equipment/Actions'
import { ModalActions } from 'Stores/Modal/Actions'
import { EquipmentSelectors } from 'Stores/Equipment/Selectors'
import { GlobalSelectors } from 'Stores/Global/Selectors'
import ControlUser from 'Components/organisms/ControlUser'

const ControlEquipmentContainer = ({ setModal, setFilter, filter, ...props }) => {
  const [keyword, setKeyword] = useState(filter.get('keyword'))
  const [office, setOffice] = useState(filter.get('agencyId'))
  const [department, setDepartment] = useState(filter.get('departmentId'))
  const handleSearch = () => {
    const filter = {
      keyword,
      agencyId: office,
      departmentId: department,
      page: 1,
    }
    setFilter(filter)
  }
  // Handle open modal create user
  const handleCreateUser = () => {
    let item = {
      email: '',
      password: '',
      phone: '',
      displayName: '',
      office: '',
      department: '',
      avatar: '',
      joinMonth: null,
      didSaturday: false,
      didSunday: false,
      firstName: '',
      lastName: '',
      jobTitle: '',
      gender: '',
    }
    setModal('CreateEditUserModal', {
      action: 'create',
      item,
    })
  }
  return (
    <ControlUser
      keyword={keyword}
      setKeyword={setKeyword}
      office={office}
      setOffice={setOffice}
      department={department}
      setDepartment={setDepartment}
      handleSearch={handleSearch}
      handleCreateUser={handleCreateUser}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  filter: EquipmentSelectors.getFilter(state),
  departmentOptions: GlobalSelectors.getDepartmentOptions(state),
  officeOptions: GlobalSelectors.getLocationOptions(state),
})

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(EquipmentActions.setFilter(filter)),
  // Set Modal
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

ControlEquipmentContainer.propTypes = {
  setFilter: func.isRequired,
  setModal: func.isRequired,
  filter: object.isRequired,
}

export default compose(withConnect)(ControlEquipmentContainer)
