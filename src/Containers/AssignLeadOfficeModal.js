import React, { useEffect } from 'react'
import { func, number } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { OfficeActions } from 'Stores/OfficeManagement/Actions'
import { GlobalSelectors } from 'Stores/Global/Selectors'
import { GlobalActions } from 'Stores/Global/Actions'

import { OfficeSelectors } from 'Stores/OfficeManagement/Selectors'
import { withTranslation } from 'react-i18next'

import AssignLeadOfficeModal from 'Components/organisms/AssignLeadOfficeModal'

const AssignLeadOfficeModalContainer = ({
  clearLeaders,
  getLeaders,
  officeId,
  handleSearchUser,
  assignLeaders,
  ...props
}) => {
  // Effect get list leader
  useEffect(() => {
    getLeaders(officeId)
    return () => clearLeaders()
  }, [getLeaders, clearLeaders, officeId])

  const handleSearch = (str, actions) => {
    handleSearchUser(str)
  }
  const handleAction = values => {
    assignLeaders(officeId, values)
  }
  return (
    <AssignLeadOfficeModal
      handleSearch={handleSearch}
      handleAction={handleAction}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  searchUserOptions: GlobalSelectors.getSearchUserOptions(state),
  assignItem: OfficeSelectors.getFormAssignItem(state),
})

const mapDispatchToProps = dispatch => ({
  handleSearchUser: keyword =>
    dispatch(GlobalActions.searchUsersRequest(keyword)),
  clearSearchUsers: () => dispatch(GlobalActions.clearSearchUsers()),
  // Get and clear leaders of office
  getLeaders: officeId => dispatch(OfficeActions.getLeadersRequest(officeId)),
  clearLeaders: () => dispatch(OfficeActions.clearLeaders()),
  // Assign leader
  assignLeaders: (officeId, values) =>
    dispatch(OfficeActions.assignLeadersRequest(officeId, values)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

AssignLeadOfficeModalContainer.propTypes = {
  getLeaders: func.isRequired,
  clearLeaders: func.isRequired,
  officeId: number.isRequired,
  handleSearchUser: func.isRequired,
  assignLeaders: func.isRequired,
}

export default compose(
  withConnect,
  withTranslation()
)(AssignLeadOfficeModalContainer)
