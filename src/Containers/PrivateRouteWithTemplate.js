import React, { useContext } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import moment from 'moment'
import { Route } from 'react-router-dom'
import { removeToken } from 'Utils/token'
import AuthTemplate from './AuthTemplate'
import { handleLinkExternal } from 'Utils/handleLinkExternal'
import { UserContext } from 'Contexts/UserData'
import { checkPermissionsByPath } from 'Utils/checkPermission'

const PrivateRoute = ({ component: Component, ...rest }) => {
  // const user = localStorage.getItem('auth')
  const user = useContext(UserContext)
  // Check valid token
  if (user.auth) {
    let endDate = moment.unix(user.auth.exp)
    if (!moment().isBefore(endDate)) {
      // If not valid token, clear in local storage and redirect to login page
      removeToken()
      handleLinkExternal('/login')()
      return
    }
  } else {
    // Don't have user data redirect to login page
    handleLinkExternal('/login')()
    return
  }
  // Check user's permission
  if (!checkPermissionsByPath(user.permissions, rest.path)) {
    handleLinkExternal('/')()
  }
  return (
    <Route
      {...rest}
      render={({ location, history, ...props }) => {
        return (
          <AuthTemplate>
            <Component location={location} history={history} {...props} />
          </AuthTemplate>
        )
      }}
    />
  )
}

PrivateRoute.propTypes = {
  component: PropTypes.any.isRequired,
}

const mapStateToProps = state => {
  return {}
}

export default connect(mapStateToProps)(PrivateRoute)
