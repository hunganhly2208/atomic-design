import React, { useEffect } from 'react'
import { func, object } from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { UserActions } from 'Stores/User/Actions'
import { ModalActions } from 'Stores/Modal/Actions'
import { UserSelectors } from 'Stores/User/Selectors'
import { LoadingSelectors } from 'Stores/Loading/Selectors'
import UserTable from 'Components/organisms/UserTable'

const UserTableContainer = ({
  clearListUser,
  getListUser,
  filter,
  setModal,
  setFilter,
  ...props
}) => {
  // Get list User
  useEffect(() => {
    getListUser()
    return () => clearListUser()
  }, [getListUser, clearListUser, filter])

  // Handle Edit user
  const handleEditUser = userId => () => {
    setModal('CreateEditUserModal', {
      action: 'edit',
      userId,
    })
  }
  // Handle Delete User
  const handleDeleteUser = id => () => {
    setModal('ConfirmationDialog', {
      title: props.t('user:deleteUser'),
      content: props.t('user:confirmDeleteUser'),
      type: 'deleteUser',
      id,
    })
  }
  // Handle Organize user
  const handleOrganizeUser = userId => () => {
    setModal('OrganizeUserModal', {
      userId,
    })
  }
  // Handle set User role
  const handleSetUserRoles = userId => () => {
    setModal('SetUserRolesModal', {
      userId,
    })
  }
  // Hanlde change limit and page
  const handleChangePage = (e, activePage) =>
    setFilter({ page: activePage + 1 })
  const handleChangeRowsPerPage = e => setFilter({ limit: +e.target.value })
  return (
    <UserTable
      handleDeleteUser={handleDeleteUser}
      handleEditUser={handleEditUser}
      filter={filter}
      handleChangePage={handleChangePage}
      handleChangeRowsPerPage={handleChangeRowsPerPage}
      handleOrganizeUser={handleOrganizeUser}
      handleSetUserRoles={handleSetUserRoles}
      {...props}
    />
  )
}

const mapStateToProps = state => ({
  filter: UserSelectors.getFilter(state),
  items: UserSelectors.getItems(state),
  isLoadingList: LoadingSelectors.getLoadingList(state),
  totalCount: UserSelectors.getTotalCount(state),
})

const mapDispatchToProps = dispatch => ({
  getListUser: () => dispatch(UserActions.getItemsRequest()),
  clearListUser: () => dispatch(UserActions.clearItems()),
  setFilter: filter => dispatch(UserActions.setFilter(filter)),
  // Set modal
  setModal: (type, props) => dispatch(ModalActions.setModal(type, props)),
})

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
)

UserTableContainer.propTypes = {
  clearListUser: func.isRequired,
  getListUser: func.isRequired,
  filter: object.isRequired,
  setModal: func.isRequired,
  setFilter: func.isRequired,
  t: func,
}

export default compose(withConnect)(UserTableContainer)
