import { camelizeKeys, decamelizeKeys } from 'humps'
import apiClient from './index'

/**
 * @method Get
 * @description Get list equipment groups
 */
const getListEquipmentGroups = (params = { page: 1, limit: 100 }) =>
  apiClient
    .get('/api/device/categories', params)
    .then(res => camelizeKeys(res.data))

/**
 * @method Get
 * @param {Number} equipment groups id
 * @description Get equipment groups by id
 */
const getEquipmentGroupsById = id =>
  apiClient
    .get(`/api/device/categories/${id}`)
    .then(res => camelizeKeys(res.data))

/**
 * @method Post
 * @param {Object} name
 * @description Create new equipment groups
 */

const createEquipmentGroups = values =>
  apiClient
    .post('/api/device/category/add', decamelizeKeys(values))
    .then(res => camelizeKeys(res.data))

/**
 * @method Put
 * @param {Number} equipmentGroupsId
 * @description Edit equipment groups by id
 */

const editEquipmentGroups = (equipmentGroupsId, values) =>
  apiClient
    .put(`/api/device/category/${equipmentGroupsId}`, decamelizeKeys(values))
    .then(res => camelizeKeys(res.data))

/**
 * @method DELETE
 * @param{Number} equipment groups Id
 * @description Delete equipment groups
 */
const deleteEquipmentGroups = equipmentGroupsId =>
  apiClient
    .delete(`/api/device/category/${equipmentGroupsId}`)
    .then(res => camelizeKeys(res.data))


export const EquipmentGroupsService = {
  getListEquipmentGroups,
  getEquipmentGroupsById,
  createEquipmentGroups,
  editEquipmentGroups,
  deleteEquipmentGroups,
}
