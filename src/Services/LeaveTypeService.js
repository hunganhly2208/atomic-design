import { camelizeKeys, decamelizeKeys } from 'humps'
import apiClient from './index'

/**
 * @method Get
 * @description Get list Leave Type
 * @param {Number} page
 * @param {Number} limit
 */
const defaultParams = {
  page: 1,
  limit: 100,
}

const getListLeaveType = (params = defaultParams) =>
  apiClient
    .get('/api/leave/type/list', decamelizeKeys(params))
    .then(res => camelizeKeys(res.data))

/**
 * @method Post
 * @param {String} abbreviation
 * @param {String} typeName
 * @param {Html} description
 * @param {Number} evidenceRequire
 * @param {Number} leavesDay
 * @description Create new Leave Type
 */

const createLeaveType = values =>
  apiClient
    .post('/api/leave/type/add', decamelizeKeys(values))
    .then(res => camelizeKeys(res.data))

/**
 * @method Put
 * @param {String} abbreviation
 * @param {String} typeName
 * @param {Html} description
 * @param {Number} evidenceRequire
 * @param {Number} leavesDay
 * @description Edit Leave Type
 */

const editLeaveType = (id, values) =>
  apiClient
    .put(`/api/leave/type/${id}`, decamelizeKeys(values))
    .then(res => camelizeKeys(res.data))
/**
 * @method DELETE
 * @param{Number} typeId
 * @description Delete Leave Type
 */
const deleteLeaveType = typeId =>
  apiClient
    .delete(`/api/leave/type/${typeId}`)
    .then(res => camelizeKeys(res.data))

export const LeaveTypeService = {
  getListLeaveType,
  createLeaveType,
  editLeaveType,
  deleteLeaveType,
}
