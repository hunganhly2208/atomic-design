import { camelizeKeys, decamelizeKeys } from 'humps'
import apiClient from './index'

/**
 * @method Post
 * @param {String} username
 * @param {String} password
 * @param {String} display_name
 * @param {Email} email
 * @description: Create new user
 */

/**
 * @method GET
 * @param {Number} page
 * @param {Number} limit
 * @param {String} keyword
 * @description Get list users
 */

const getListEquipment = params =>
  apiClient
    .get('/api/core/users/list', decamelizeKeys(params))
    .then(res => camelizeKeys(res.data))

// /**
//  * @method GET
//  * @param {Number} userId
//  * @description Get user general information *
//  */
// const getEquipmentInfo = id =>
//   apiClient.get(`/api/core/users/${id}`).then(res => camelizeKeys(res.data))

// /**
//  * @method GET
//  * @param {Number} userId
//  * @description Get list user'roles by user id
//  */
// const getEquipmentRoles = id =>
//   apiClient
//     .get(`/api/core/users/${id}/roles`)
//     .then(res => camelizeKeys(res.data))

// /**
//  * @method POST
//  * @param {String} avatar link avatar of user
//  * @param {String} display_name Display name of user
//  * @param {Email} email Email of user
//  * @param {String} password Password
//  * @param {String} phone Phone's number
//  */

// const createEquipment = values =>
//   apiClient
//     .post(`/api/core/users/add`, decamelizeKeys(values))
//     .then(res => camelizeKeys(res.data))
// /**
//  * @method Put
//  * @param {String} avatar link avatar of user
//  * @param {String} display_name Display name of user
//  * @param {Email} email Email of user
//  * @param {String} password Password
//  * @param {String} phone Phone's number
//  */

// const editEquipment = (id, values) =>
//   apiClient
//     .put(`/api/core/users/backend/${id}`, decamelizeKeys(values))
//     .then(res => camelizeKeys(res.data))

// /**
//  * @method DELETE
//  * @param {Number} userId
//  * @description Delete user
//  */
// const deleteEquipment = userId =>
//   apiClient
//     .delete(`/api/core/users/backend/${userId}`)
//     .then(res => camelizeKeys(res.data))
// /**
//  * @method PUT
//  * @param {Array} items List role_id
//  * @param {Number} userId
//  * @description Set Roles for user
//  */
// const setEquipmentRoles = (userId, values) =>
//   apiClient
//     .put(`/api/core/users/roles/assign/${userId}`, decamelizeKeys(values))
//     .then(res => camelizeKeys(res.data))

// /**
//  * @method Get
//  * @param {Number} officeId
//  * @description Get leaders of a office by id
//  *
//  */
// const getLeadersOffice = officeId =>
//   apiClient
//     .get(`/api/core/users/agency/lead/${officeId}`)
//     .then(res => camelizeKeys(res.data))

// /**
//  * @method Get
//  * @param {Number} departmentId
//  * @description Get leaders of a office by id
//  *
//  */
// const getLeadersDepartment = departmentId =>
//   apiClient
//     .get(`/api/core/users/department/lead/${departmentId}`)
//     .then(res => camelizeKeys(res.data))
// /**
//  * @method Get
//  * @param {1,2,3,4 id split by comma} userid list
//  * @description Get list user detail by list userId
//  */

// const getListEquipmentDetail = params =>
//   apiClient
//     .get('/api/core/users/list/info', decamelizeKeys(params))
//     .then(res => camelizeKeys(res.data))

// /**
//  * @method GET
//  * @param {Number} Equipment id
//  * @description Get user's profile infomation
//  */
// const getProfileInfo = userId =>
//   apiClient
//     .get(`/api/core/users/profile/${userId}`)
//     .then(res => camelizeKeys(res.data))
// /**
//  * @method PUT
//  * @param {String} phone
//  * @param {String} taxCode
//  * @param {String} socialInsuranceNumber
//  * @param {String} birthday 'YYYY-MM-DD'
//  * @param {String} identityCard
//  * @param {String} issueDate 'YYYY-MM-DD
//  * @param {String} issusAt
//  * @param {String} residentialAddress
//  * @param {String} address
//  * @param {String} avatar
//  */

// const updateProfile = (userId, values) =>
//   apiClient
//     .put(`/api/core/users/profile/${userId}`, decamelizeKeys(values))
//     .then(res => camelizeKeys(res.data))

export const EquipmentService = {
  getListEquipment,
  // editEquipment,
  // createEquipment,
  // deleteEquipment,
  // getEquipmentInfo,
  // getEquipmentRoles,
  // setEquipmentRoles,
  // getLeadersOffice,
  // getLeadersDepartment,
  // getListEquipmentDetail,
  // getProfileInfo,
  // updateProfile,
}
