import { camelizeKeys, decamelizeKeys } from 'humps'
import apiClient from './index'

/**
 * @method Get
 * @description Get list Holiday
 * @param {Date} fromDate
 * @param {Date} toDate
 */

const getListHoliday = params =>
  apiClient
    .get('/api/leave/holiday/list', decamelizeKeys(params))
    .then(res => camelizeKeys(res.data))

/**
 * @method Post
 * @param {String} name
 * @param {Date} 'YYYY-MM-DD' date
 * @description Create new holiday
 */

const createHoliday = values =>
  apiClient
    .post('/api/leave/holiday/add', decamelizeKeys(values))
    .then(res => camelizeKeys(res.data))
/**
 * @method DELETE
 * @param{Number} holidayId
 * @description Delete holiday
 */
const deleteHoliday = holidayId =>
  apiClient
    .delete(`/api/leave/holiday/${holidayId}`)
    .then(res => camelizeKeys(res.data))

export const HolidayService = {
  getListHoliday,
  createHoliday,
  deleteHoliday,
}
