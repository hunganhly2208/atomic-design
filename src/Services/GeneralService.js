import { camelizeKeys } from 'humps'
import apiClient from './'
/**
 * @method POST
 * @param {Array} List files upload
 * @param {String} Module's name (News | Stationery | Users)
 * @description: Upload file
 */
const uploadFile = (files, module) => {
  const data = new FormData()
  const ins = files.length
  for (let x = 0; x < ins; x++) {
    data.append('files[]', files[x])
  }
  data.append('module', module)
  return apiClient
    .post(`/api/core/files/upload`, data)
    .then(res => camelizeKeys(res.data))
}

export const GeneralService = {
  uploadFile,
}
