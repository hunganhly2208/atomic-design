import apiClient from 'Services'
import { camelizeKeys } from 'humps'

// import apiClient from '.'

const getGlobalData = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        result: 'success',
        item: {
          configs: [],
          themes: {
            mainColor: 'blue',
          },
        },
      })
    }, 1000)
  })
}

// Get data with apiClient
// const getGlobalData = () => apiClient.get('/global')
const getListRoles = () =>
  apiClient
    .get('/api/core/users/roles/list')
    .then(res => camelizeKeys(res.data))

export const GlobalService = {
  getGlobalData,
  getListRoles,
}
