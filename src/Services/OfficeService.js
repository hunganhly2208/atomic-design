import { camelizeKeys, decamelizeKeys } from 'humps'
import apiClient from './index'

/**
 * @method Get
 * @description Get list office
 */

const getListOffice = (params = { page: 1, limit: 100 }) =>
  apiClient
    .get('/api/core/agency/list', params)
    .then(res => camelizeKeys(res.data))

/**
 * @method DELETE
 * @param{Number} officeId
 * @description Delete office
 */

/**
 * @method Get
 * @param {Number} User id
 * @description Get list Office of a user
 */
const getListUserOffice = id =>
  apiClient
    .get(`/api/core/agency/user/${id}`)
    .then(res => camelizeKeys(res.data))

/**
 * @method Post
 * @param {String} name
 * @description Create new Ofice
 */

const createOffice = values =>
  apiClient
    .post('/api/core/agency/add', decamelizeKeys(values))
    .then(res => camelizeKeys(res.data))

/**
 * @method Put
 * @param {String} name
 * @param {Number} oficeId
 * @description Edit Office by id
 */

const editOffice = (officeId, values) =>
  apiClient
    .put(`/api/core/agency/${officeId}`, decamelizeKeys(values))
    .then(res => camelizeKeys(res.data))

/**
 * @method Put
 * @param {Number} officeId
 * @param {Array} List user id
 * @description Assign users become leaders of a office
 */
const assignLeadersOffice = (officeId, values) =>
  apiClient
    .put(
      `/api/core/agency/assign/user/lead/${officeId}`,
      decamelizeKeys(values)
    )
    .then(res => camelizeKeys(res.data))

export const OfficeService = {
  getListOffice,
  getListUserOffice,
  createOffice,
  editOffice,
  assignLeadersOffice,
}
