import { camelizeKeys, decamelizeKeys } from 'humps'
import apiClient from './index'

/**
 * @method Get
 * @description Get list department
 */

const getListDepartment = (params = { page: 1, limit: 100 }) =>
  apiClient
    .get('/api/core/department/list', params)
    .then(res => camelizeKeys(res.data))
/**
 * @method Get
 * @param {Number} User id
 * @description Get list department of a user
 */
const getListUserDepartment = id =>
  apiClient
    .get(`/api/core/department/user/${id}`)
    .then(res => camelizeKeys(res.data))

/**
 * @method Post
 * @param {String} name
 * @description Create new department
 */

const createDepartment = values =>
  apiClient
    .post('/api/core/department/add', decamelizeKeys(values))
    .then(res => camelizeKeys(res.data))

/**
 * @method Put
 * @param {String} name
 * @param {Number} departmentId
 * @description Edit department by id
 */

const editDepartment = (departmentId, values) =>
  apiClient
    .put(`/api/core/department/${departmentId}`, decamelizeKeys(values))
    .then(res => camelizeKeys(res.data))
/**
 * @method Put
 * @param {Number} departmentId
 * @param {Array} List user id
 * @description Assign users become leaders of a Department
 */
const assignLeadersDepartment = (departmentId, values) =>
  apiClient
    .put(
      `/api/core/department/assign/user/lead/${departmentId}`,
      decamelizeKeys(values)
    )
    .then(res => camelizeKeys(res.data))

export const DepartmentService = {
  getListDepartment,
  getListUserDepartment,
  createDepartment,
  editDepartment,
  assignLeadersDepartment,
}
