import apiClient from 'Services'
import { camelizeKeys, decamelizeKeys } from 'humps'

/**
 * @method Get
 * @param {Number} page
 * @param {Number} limit
 * @param {Number} userId
 * @description Get list User in category
 */

const getListCategoryUsers = params =>
  apiClient
    .get('/api/news/categories/roles', decamelizeKeys(params))
    .then(res => camelizeKeys(res.data))
/**
 * @method Post
 * @param {Number} userId
 * @param {Number} categoryId
 * @param {1 or 0} view
 * @param {1 or 0} post
 * @param {1 or 0} email
 * @description Assign user's permisson  to a category
 */

const assignUserToCategory = values =>
  apiClient
    .post('/api/news/categories/roles', decamelizeKeys(values))
    .then(res => camelizeKeys(res.data))

/**
 * @method Delete
 * @param {Number} id of list category Users
 * @description Remove user's permission from a category
 */

const deleteUserFromCategory = id =>
  apiClient
    .delete(`/api/news/categories/roles/${id}`)
    .then(res => camelizeKeys(res.data))

export const NewService = {
  getListCategoryUsers,
  assignUserToCategory,
  deleteUserFromCategory,
}
