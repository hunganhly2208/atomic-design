import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    boxSizing: 'border-box',
    background: '#f5f5f5',
    paddingBottom: 30,
  },
  contentWrapper: {
    // padding: 16,
    height: 'calc(100% - 64px)',
    marginTop: 64,
    position: 'relative',
    padding: '0 16px',
    paddingLeft: ({ viewMode }) => (viewMode === 1 ? 321 : 16),
  },
}))
export default useStyles
