import React, { Suspense } from 'react'
import PropTypes from 'prop-types'
import Drawer from '../../organisms/Drawer'
import AppBar from '../../organisms/AppBar'
import useStyles from './styles'

// Use react lazy to component splitting
const Notification = React.lazy(() => import('Containers/Notification'))
const ProfileDrawer = React.lazy(() => import('../../organisms/ProfileDrawer'))
const Modal = React.lazy(() => import('Containers/ModalManager'))

const AuthTemplate = ({
  children,
  doLogout,
  handleShowDrawer,
  handleHideDrawer,
  isShowDrawer,
  viewMode,
  isShowProfileDrawer,
  handleShowProfileDrawer,
  handleHideProfileDrawer,
  userData,
}) => {
  const classes = useStyles({ viewMode })
  return (
    <div className={classes.wrapper}>
      <AppBar
        handleShowDrawer={handleShowDrawer}
        isShowDrawer={isShowDrawer}
        handleHideDrawer={handleHideDrawer}
        userData={userData}
        handleSignOut={doLogout}
        viewMode={viewMode}
        handleShowProfileDrawer={handleShowProfileDrawer}
        handleHideProfileDrawer={handleHideProfileDrawer}
      />
      <Drawer
        isShowDrawer={isShowDrawer}
        handleHideDrawer={handleHideDrawer}
        viewMode={viewMode}
      />
      <Suspense fallback={<div>Loading...</div>}>
        <ProfileDrawer
          isShowDrawer={isShowProfileDrawer}
          handleHideDrawer={handleHideProfileDrawer}
          userData={userData}
          doLogout={doLogout}
        />
      </Suspense>
      <main className={classes.contentWrapper}>{children}</main>
      <Suspense fallback={<div>Loading...</div>}>
        <Notification />
      </Suspense>
      <Suspense fallback={<div>Loading...</div>}>
        <Modal />
      </Suspense>
    </div>
  )
}

AuthTemplate.propTypes = {
  children: PropTypes.node,
  doLogout: PropTypes.func,
  handleShowDrawer: PropTypes.func,
  handleHideDrawer: PropTypes.func,
  isShowDrawer: PropTypes.bool,
  userData: PropTypes.object,
  viewMode: PropTypes.number,
  isShowProfileDrawer: PropTypes.bool,
  handleShowProfileDrawer: PropTypes.func,
  handleHideProfileDrawer: PropTypes.func,
}

export default AuthTemplate
