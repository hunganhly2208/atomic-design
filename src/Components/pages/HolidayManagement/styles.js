import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  btnCreate: {
    position: 'fixed',
    bottom: 20,
    right: 20,
  },
  controlWrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 16,
  },
}))

export default useStyles
