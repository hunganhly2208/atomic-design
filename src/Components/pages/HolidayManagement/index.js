import React from 'react'
import { func, object } from 'prop-types'
import {
  Grid,
  Fab,
  Tooltip,
  Paper,
  IconButton,
  Typography,
} from '@material-ui/core'
import { Add, ArrowBack, ArrowForward } from '@material-ui/icons'
import HolidayTable from 'Containers/HolidayTable'
import useStyles from './styles'

const HolidayManagement = ({
  handleOpenModalCreate,
  filter,
  handleChangeYear,
  t,
}) => {
  const classes = useStyles()
  return (
    <Grid container justify="center">
      <Grid item md={8} xs={12}>
        <Paper className={classes.controlWrapper}>
          <Tooltip title={t('user:previousYear')} placement="top">
            <IconButton
              color="primary"
              onClick={handleChangeYear(filter.get('year') - 1)}>
              <ArrowBack />
            </IconButton>
          </Tooltip>
          <Typography>{filter.get('year')}</Typography>
          <Tooltip title={t('user:nextYear')} placement="top">
            <IconButton
              color="primary"
              onClick={handleChangeYear(filter.get('year') + 1)}>
              <ArrowForward />
            </IconButton>
          </Tooltip>
        </Paper>
        <HolidayTable t={t} />
      </Grid>
      <Tooltip placement="top" title={t('user:createHoliday')}>
        <Fab
          color="primary"
          size="large"
          className={classes.btnCreate}
          onClick={handleOpenModalCreate}>
          <Add />
        </Fab>
      </Tooltip>
    </Grid>
  )
}
HolidayManagement.propTypes = {
  handleOpenModalCreate: func.isRequired,
  filter: object.isRequired,
  handleChangeYear: func.isRequired,
  t: func,
}
export default HolidayManagement
