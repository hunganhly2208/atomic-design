import React from 'react'
import { func } from 'prop-types'
import { Grid, Fab, Tooltip } from '@material-ui/core'
import { Add } from '@material-ui/icons'
import LeaveTypeTable from 'Containers/LeaveTypeTable'
import useStyles from './styles'

const LeaveTypeManagement = ({ handleOpenModalCreate, t }) => {
  const classes = useStyles()
  return (
    <Grid container justify="center">
      <Grid item xs={8}>
        <LeaveTypeTable t={t} />
      </Grid>
      {/* <Tooltip placement="top" title={t('leaveType:createLeaveType')}>
        <Fab
          color="primary"
          size="large"
          className={classes.btnCreate}
          onClick={handleOpenModalCreate}>
          <Add />
        </Fab>
      </Tooltip> */}
    </Grid>
  )
}
LeaveTypeManagement.propTypes = {
  handleOpenModalCreate: func.isRequired,
  t: func,
}
export default LeaveTypeManagement
