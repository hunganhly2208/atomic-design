import React from 'react'
import { func } from 'prop-types'
import { Grid, Fab, Tooltip } from '@material-ui/core'
import { Add } from '@material-ui/icons'
import EquipmentGroupsTable from 'Components/organisms/EquipmentGroupsTable'
import useStyles from './styles'

const EquipmentGroups = ({
  t,
  items,
  handleAddEquipmentGroup,
  handleEditEquipmentGroup,
  handleDeleteEquipmentGroup,
  handleChangePage,
  handleChangeRowsPerPage,
  totalCount,
  isLoadingList,
  filter,
}) => {
  const classes = useStyles()
  return (
    <>
      <Grid container justify="center">
        <Grid item md={8} xs={12}>
          <EquipmentGroupsTable
            t={t}
            items={items}
            handleEditEquipmentGroup={handleEditEquipmentGroup}
            handleDeleteEquipmentGroup={handleDeleteEquipmentGroup}
            filter={filter}
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
            totalCount={totalCount}
            isLoadingList={isLoadingList}
          />
        </Grid>
        <Tooltip placement="top" title={t('common:create')}>
          <Fab
            color="primary"
            size="large"
            className={classes.btnCreate}
            onClick={handleAddEquipmentGroup}>
            <Add />
          </Fab>
        </Tooltip>
      </Grid>
    </>
  )
}
EquipmentGroups.propTypes = {
  handleOpenModalCreate: func.isRequired,
  t: func,
}
export default EquipmentGroups
