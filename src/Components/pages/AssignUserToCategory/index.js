import React from 'react'
import { object } from 'prop-types'
import useStyles from './styles'
import AssignUserToCategoryTable from 'Containers/AssignUserToCategoryTable'
import ControlAssignUserToCategory from 'Containers/ControlAssignUserToCategory'

const AssignUserToCategory = ({ match, t }) => {
  const classes = useStyles()
  return (
    <div>
      <ControlAssignUserToCategory
        categoryId={+match.params.categoryId}
        t={t}
      />
      <AssignUserToCategoryTable categoryId={+match.params.categoryId} t={t} />
    </div>
  )
}

AssignUserToCategory.propTypes = {
  match: object.isRequired,
}

export default AssignUserToCategory
