import React from 'react'
import { func } from 'prop-types'
import { Grid, Fab, Tooltip } from '@material-ui/core'
import { Add } from '@material-ui/icons'
import OfficeTable from 'Containers/OfficeTable'
import useStyles from './styles'

const OfficeManagement = ({ handleOpenModalCreate, t }) => {
  const classes = useStyles()
  return (
    <Grid container justify="center">
      <Grid item md={8} xs={12}>
        <OfficeTable t={t} />
      </Grid>
      <Tooltip placement="top" title={t('user:createOffice')}>
        <Fab
          color="primary"
          size="large"
          className={classes.btnCreate}
          onClick={handleOpenModalCreate}>
          <Add />
        </Fab>
      </Tooltip>
    </Grid>
  )
}
OfficeManagement.propTypes = {
  handleOpenModalCreate: func.isRequired,
  t: func,
}
export default OfficeManagement
