import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  btnCreate: {
    position: 'fixed',
    bottom: 20,
    right: 20,
  },
}))

export default useStyles
