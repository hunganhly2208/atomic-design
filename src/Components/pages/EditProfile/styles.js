import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  textField: {
    marginBottom: 16,
  },
}))

export default useStyles
