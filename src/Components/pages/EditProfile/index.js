import React from 'react'
import { func, object, bool } from 'prop-types'
import {
  Grid,
  Paper,
  Typography,
  TextField,
  Button,
  CircularProgress,
} from '@material-ui/core'
import { Formik } from 'formik'
import * as Yup from 'yup'
import UploadAvatar from '../../molecules/UploadAvatar'
import { DatePicker } from '@material-ui/pickers'

import useStyles from './styles'

const EditProfile = ({
  isLoadingItems,
  uploadAvatar,
  formItem,
  userInfo,
  handleEditProfile,
  isLoadingAction,
  handleGoBack,
  t,
}) => {
  const classes = useStyles()
  return (
    <Paper style={{ padding: 16 }}>
      <Formik
        initialValues={formItem.toJS()}
        enableReinitialize
        validationSchema={Yup.object().shape({
          phone: Yup.string().required('Please input phone number'),
          taxCode: Yup.string().required('Please input Tax number'),
          socialInsuranceNumber: Yup.string().required(
            'Please input Insurance number'
          ),
          birthday: Yup.string()
            .required('Please Choose birth day')
            .nullable(),
          identityCard: Yup.string().required('Please input identity number'),
          issueDate: Yup.string()
            .required('Please Choose Issue date')
            .nullable(),
          issueAt: Yup.string().required('Please input issue at'),
          placeOfBirth: Yup.string().required('Please input Place of birth'),
          residentialAddress: Yup.string().required(
            'Please input residential address'
          ),
        })}
        onSubmit={values => handleEditProfile(values)}
        render={({
          values,
          setFieldTouched,
          setFieldValue,
          errors,
          touched,
          handleChange,
          handleSubmit,
          handleBlur,
        }) => {
          return (
            <form style={{ width: '100%' }} onSubmit={handleSubmit}>
              <Grid container justify="center">
                <Grid item xs={8}>
                  <Typography
                    align="center"
                    variant="h6"
                    color="primary"
                    gutterBottom>
                    {userInfo.get('displayName')}
                  </Typography>
                  <UploadAvatar
                    isLoadingItems={isLoadingItems}
                    uploadAvatar={uploadAvatar}
                    avatar={values.avatar}
                    setFieldValue={setFieldValue}
                  />
                  <TextField
                    id="phone"
                    className={classes.textField}
                    fullWidth
                    value={values.phone}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={touched.phone && Boolean(errors.phone)}
                    helperText={touched.phone && errors.phone}
                    label={t('profile:phone')}
                    placeholder={t('profile:phone')}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <TextField
                    id="taxCode"
                    className={classes.textField}
                    fullWidth
                    value={values.taxCode}
                    onChange={handleChange}
                    error={touched.taxCode && Boolean(errors.taxCode)}
                    helperText={touched.taxCode && errors.taxCode}
                    onBlur={handleBlur}
                    label={t('profile:taxCode')}
                    placeholder={t('profile:taxCode')}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <TextField
                    id="socialInsuranceNumber"
                    className={classes.textField}
                    fullWidth
                    value={values.socialInsuranceNumber}
                    error={
                      touched.socialInsuranceNumber &&
                      Boolean(errors.socialInsuranceNumber)
                    }
                    helperText={
                      touched.socialInsuranceNumber &&
                      errors.socialInsuranceNumber
                    }
                    onChange={handleChange}
                    onBlur={handleBlur}
                    label={t('profile:insuranceNumber')}
                    placeholder={t('profile:insuranceNumber')}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <TextField
                    id="placeOfBirth"
                    className={classes.textField}
                    fullWidth
                    value={values.placeOfBirth}
                    onChange={handleChange}
                    error={touched.placeOfBirth && Boolean(errors.placeOfBirth)}
                    helperText={touched.placeOfBirth && errors.placeOfBirth}
                    onBlur={handleBlur}
                    label={t('profile:placeOfBirth')}
                    placeholder={t('profile:placeOfBirth')}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <DatePicker
                    className={classes.textField}
                    autoOk
                    id="birthday"
                    error={touched.birthday && Boolean(errors.birthday)}
                    helperText={touched.birthday && errors.birthday}
                    variant="inline"
                    label={t('profile:birthDay')}
                    emptyLabel={t('profile:birthDay')}
                    inputVariant="outlined"
                    format="DD/MM/YYYY"
                    value={values.birthday}
                    fullWidth
                    onChange={date => setFieldValue('birthday', date)}
                  />
                  <TextField
                    id="identityCard"
                    className={classes.textField}
                    fullWidth
                    error={touched.identityCard && Boolean(errors.identityCard)}
                    helperText={touched.identityCard && errors.identityCard}
                    value={values.identityCard}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    label={t('profile:identityCard')}
                    placeholder={t('profile:identityCard')}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <Grid container spacing={2}>
                    <Grid item xs={6}>
                      <DatePicker
                        className={classes.textField}
                        autoOk
                        id="issueDate"
                        variant="inline"
                        error={touched.issueDate && Boolean(errors.issueDate)}
                        helperText={touched.issueDate && errors.issueDate}
                        label={t('profile:issueDate')}
                        emptyLabel={t('profile:issueDate')}
                        inputVariant="outlined"
                        format="DD/MM/YYYY"
                        value={values.issueDate}
                        fullWidth
                        onChange={date => setFieldValue('issueDate', date)}
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        id="issueAt"
                        className={classes.textField}
                        fullWidth
                        error={touched.issueAt && Boolean(errors.issueAt)}
                        helperText={touched.issueAt && errors.issueAt}
                        value={values.issueAt}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        label={t('profile:issueAt')}
                        placeholder={t('profile:issueAt')}
                        variant="outlined"
                        InputLabelProps={{
                          shrink: true,
                        }}
                      />
                    </Grid>
                  </Grid>
                  <TextField
                    id="residentialAddress"
                    multiline
                    rows={2}
                    error={
                      touched.residentialAddress &&
                      Boolean(errors.residentialAddress)
                    }
                    helperText={
                      touched.residentialAddress && errors.residentialAddress
                    }
                    className={classes.textField}
                    fullWidth
                    value={values.residentialAddress}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    label={t('profile:residentialAddress')}
                    placeholder={t('profile:residentialAddress')}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <TextField
                    id="address"
                    multiline
                    rows={2}
                    className={classes.textField}
                    fullWidth
                    value={values.address}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    label={t('profile:address')}
                    placeholder={t('profile:address')}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                </Grid>
                <Grid container justify="center">
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    disabled={isLoadingAction}
                    style={{ marginRight: 8 }}>
                    {t('common:save')}
                    {isLoadingAction && (
                      <CircularProgress style={{ marginLeft: 8 }} size={16} />
                    )}
                  </Button>
                  <Button
                    type="button"
                    variant="outlined"
                    onClick={handleGoBack}>
                    {t('common:back')}
                  </Button>
                </Grid>
              </Grid>
            </form>
          )
        }}
      />
    </Paper>
  )
}
EditProfile.propTypes = {
  isLoadingItems: object.isRequired,
  uploadAvatar: func.isRequired,
  formItem: object.isRequired,
  userInfo: object.isRequired,
  handleEditProfile: func.isRequired,
  isLoadingAction: bool.isRequired,
  handleGoBack: func.isRequired,
  t: func,
}
export default EditProfile
