import React from 'react'
import { func } from 'prop-types'
import { Grid, Fab, Tooltip } from '@material-ui/core'
import { Add } from '@material-ui/icons'
import DepartmentTable from 'Containers/DepartmentTable'
import useStyles from './styles'

const DepartmentManagement = ({ handleOpenModalCreate, t }) => {
  const classes = useStyles()
  return (
    <Grid container justify="center">
      <Grid item md={8} xs={12}>
        <DepartmentTable t={t} />
      </Grid>
      <Tooltip placement="top" title={t('user:createDepartment')}>
        <Fab
          color="primary"
          size="large"
          className={classes.btnCreate}
          onClick={handleOpenModalCreate}>
          <Add />
        </Fab>
      </Tooltip>
    </Grid>
  )
}
DepartmentManagement.propTypes = {
  handleOpenModalCreate: func.isRequired,
  t: func,
}
export default DepartmentManagement
