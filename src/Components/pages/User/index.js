import React from 'react'
import { func } from 'prop-types'
import ControlUser from 'Containers/ControlUser'
import UserTable from 'Containers/UserTable'

const User = ({ t }) => {
  return (
    <div>
      <ControlUser t={t} />
      <UserTable t={t} />
    </div>
  )
}

User.propTypes = {
  t: func,
}

export default User
