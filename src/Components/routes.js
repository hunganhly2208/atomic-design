import Loadable from 'react-loadable'

import LoadingPage from './pages/LoadingPage'

// Not Found Page
export const AsyncNotFound = Loadable({
  loader: () => import('./pages/NotFound' /* webpackChunkName: "NotFound" */),
  loading: LoadingPage,
})

// User management page
export const AsyncUser = Loadable({
  loader: () => import('Containers/User'),
  loading: LoadingPage,
})

// Office management page
export const AsyncOfficeManagement = Loadable({
  loader: () => import('Containers/OfficeManagement'),
  loading: LoadingPage,
})

// Department management page
export const AsyncDepartmentManagement = Loadable({
  loader: () => import('Containers/DepartmentManagement'),
  loading: LoadingPage,
})

// Holiday management page
export const AsyncHolidayManagement = Loadable({
  loader: () => import('Containers/HolidayManagement'),
  loading: LoadingPage,
})

// Assign user to category page
export const AsyncAssignUserToCategory = Loadable({
  loader: () => import('Containers/AssignUserToCategory'),
  loading: LoadingPage,
})

// Edit user's profile page
export const AsyncEditProfile = Loadable({
  loader: () => import('Containers/EditProfile'),
  loading: LoadingPage,
})

// Leave type management
export const AsyncLeaveTypeManagement = Loadable({
  loader: () => import('Containers/LeaveTypeManagement'),
  loading: LoadingPage,
})

// Equipment
export const AsyncEquipment = Loadable({
  loader: () => import('Components/pages/EquipmentManament'),
  loading: LoadingPage,
})

// Equipment Groups
export const AsyncEquipmentGroups = Loadable({
  loader: () => import('Containers/EquipmentGroups'),
  loading: LoadingPage,
})

// Test loading page
// export const AsyncLoading = Loadable({
//   loader: () => import('./pages/LoadingPage'),
//   loading: LoadingPage,
// })
