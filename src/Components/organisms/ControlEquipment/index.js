import React from 'react'
import { string, func, array } from 'prop-types'
import { Button, TextField, Tooltip, Paper } from '@material-ui/core'
import { Search } from '@material-ui/icons'
import OutlineSelect from 'Components/atoms/OutlineSelect'
import useStyles from './styles'

const ControlEquipment = ({
  keyword,
  setKeyword,
  department,
  setDepartment,
  office,
  setOffice,
  handleSearch,
  handleCreateUser,
  departmentOptions,
  officeOptions,
  t,
}) => {
  const classes = useStyles()
  return (
    <Paper style={{ marginTop: 16, padding: 16 }}>
      ControlEquipment - organisms
      {/* <div className={classes.filterWrap}>
        <TextField
          name="keyword"
          value={keyword}
          variant="outlined"
          placeholder={t('user:searchByName')}
          onChange={e => setKeyword(e.target.value)}
          className={classes.filterItem}
          label={t('user:searchByName')}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <OutlineSelect
          options={departmentOptions}
          className={classes.filterItem}
          placeholder="By Department"
          label={t('user:department')}
          name="status"
          value={department}
          handleChange={e => setDepartment(e.target.value)}
        />
        <OutlineSelect
          options={officeOptions}
          className={classes.filterItem}
          placeholder="By Office"
          label={t('user:office')}
          name="status"
          value={office}
          handleChange={e => setOffice(e.target.value)}
        />
        <Button variant="contained" onClick={handleSearch} color="primary">
          {t('common:search')}
        </Button>
        <span style={{ flexGrow: 1 }} />
        <Tooltip title={t('user:createUser')} placement="top">
          <Button variant="outlined" onClick={handleCreateUser}>
            {t('user:newUser')}
          </Button>
        </Tooltip>
      </div> */}
    </Paper>
  )
}

ControlEquipment.propTypes = {
  keyword: string.isRequired,
  setKeyword: func.isRequired,
  office: string.isRequired,
  setOffice: func.isRequired,
  department: string.isRequired,
  setDepartment: func.isRequired,
  handleSearch: func.isRequired,
  handleCreateUser: func.isRequired,
  departmentOptions: array.isRequired,
  officeOptions: array.isRequired,
  t: func,
}

export default ControlEquipment
