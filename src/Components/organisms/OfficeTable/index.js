import React from 'react'
import { object, bool, func, number } from 'prop-types'
import {
  Paper,
  Table,
  TableBody,
  TableHead,
  TableRow,
  TableCell,
  Tooltip,
  IconButton,
  CircularProgress,
} from '@material-ui/core'
import { Edit, Assignment } from '@material-ui/icons'

import useStyles from './styles'

const OfficeTable = ({
  items,
  isLoadingList,
  handleEditOffice,
  handleAssignLeadOffice,
  t,
}) => {
  const classes = useStyles()
  return (
    <Paper className={classes.tableWrap}>
      {isLoadingList && (
        <div className={classes.loadingWrap}>
          <CircularProgress size={56} />
        </div>
      )}
      {!isLoadingList && (
        <React.Fragment>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{t('user:no')}</TableCell>
                <TableCell>{t('common:name')}</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {items && items.size !== 0 ? (
                items.map((item, index) => (
                  <TableRow key={item.get('id')}>
                    <TableCell>{index + 1}</TableCell>
                    <TableCell>{item.get('name')}</TableCell>
                    <TableCell>
                      <Tooltip
                        placement="top"
                        title={t('user:assignOfficeLead')}>
                        <IconButton
                          color="primary"
                          onClick={handleAssignLeadOffice(item.get('id'))}>
                          <Assignment />
                        </IconButton>
                      </Tooltip>
                      <Tooltip placement="top" title={t('user:editOffice')}>
                        <IconButton
                          color="primary"
                          onClick={handleEditOffice(item)}>
                          <Edit />
                        </IconButton>
                      </Tooltip>
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                <TableRow>
                  <TableCell colSpan="6" align="center">
                    {t('user:noOffice')}
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </React.Fragment>
      )}
    </Paper>
  )
}

OfficeTable.propTypes = {
  items: object.isRequired,
  isLoadingList: bool.isRequired,
  handleEditOffice: func.isRequired,
  handleAssignLeadOffice: func,
  t: func,
}
export default OfficeTable
