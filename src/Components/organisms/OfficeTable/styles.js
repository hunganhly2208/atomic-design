import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  tableWrap: {
    padding: 16,
    marginTop: 16,
  },
  loadingWrap: {
    textAlign: 'center',
  },
}))

export default useStyles
