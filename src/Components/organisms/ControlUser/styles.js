import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  filterWrap: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    // marginTop: 32,
  },
  filterItem: {
    width: '20%',
    margin: '0 8px',
  },
  btnWrap: {
    position: 'fixed',
    top: 100,
    right: 50,
  },
}))

export default useStyles
