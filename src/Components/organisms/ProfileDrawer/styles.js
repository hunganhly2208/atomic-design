import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  drawerPaper: {
    width: 256,
  },
  ProfileInfo: {
    backgroundColor: theme.palette.primary.main,
    background: `@include
    filter-gradient(
      ${theme.palette.primary.main},
      ${theme.palette.primary.main},
      vertical
    )`,
    background: `@include
    background-image(
      linear-gradient(
        top,
        ${theme.palette.primary.main} 0%,
        ${theme.palette.primary.main} 61%,
        ${theme.palette.primary.main} 76%
      )
    )`,
    color: `#fff`,
    textAlign: 'center',
  },
  btnProfile: {
    background: '#fff',
    color: theme.palette.primary.main,
    marginBottom: 16,
  },
}))

export default useStyles
