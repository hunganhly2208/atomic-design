import React from 'react'
import { func, bool, object } from 'prop-types'
import { Drawer as MuiDrawer, Button } from '@material-ui/core'
import { PermContactCalendar } from '@material-ui/icons'
import useStyles from './styles'
import MenuDrawer from '../../molecules/MenuDrawer'
import InfoCardProfile from '../../molecules/InfoCardProfile'
import { getImageUrl } from 'Utils/getImageUrl'
import { handleLinkExternal } from 'Utils/handleLinkExternal'
import { withTranslation } from 'react-i18next'

const ProfileDrawer = ({
  handleHideDrawer,
  isShowDrawer,
  doLogout,
  userData,
  t,
}) => {
  let classes = useStyles()
  return (
    <MuiDrawer
      anchor="right"
      open={isShowDrawer}
      onClose={handleHideDrawer}
      classes={{ paper: classes.drawerPaper }}>
      <div className={classes.ProfileInfo}>
        <InfoCardProfile
          staffName={userData.displayName}
          email={userData.email}
          avatar={getImageUrl(userData.avatar)}
        />
        <Button
          variant="contained"
          className={classes.btnProfile}
          onClick={handleLinkExternal(`/profile/${userData.id}`)}>
          <PermContactCalendar />
          {t('drawer:viewProfile')}
        </Button>
      </div>
      <br />
      <MenuDrawer
        isButton
        label={t('drawer:addNewPost')}
        handleClick={handleLinkExternal('/news/updateArticles/')}
      />
      <MenuDrawer
        label={t('drawer:viewMyPost')}
        isButton
        handleClick={handleLinkExternal(
          `/news-article/list/?profileId=${userData.id}`
        )}
      />
      <MenuDrawer
        isButton
        label={t('drawer:changePassword')}
        handleClick={handleLinkExternal(`/change-password/${userData.id}`)}
      />
      <MenuDrawer label={t('drawer:logOut')} handleClick={doLogout} />
    </MuiDrawer>
  )
}

ProfileDrawer.propTypes = {
  handleHideDrawer: func.isRequired,
  isShowDrawer: bool.isRequired,
  doLogout: func.isRequired,
  userData: object,
  t: func,
}

export default withTranslation()(ProfileDrawer)
