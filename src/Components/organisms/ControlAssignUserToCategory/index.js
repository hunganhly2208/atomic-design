import React from 'react'
import { string, func, array } from 'prop-types'
import {
  Button,
  TextField,
  Tooltip,
  Paper,
  InputAdornment,
  Grid,
} from '@material-ui/core'
import { Search } from '@material-ui/icons'
import useStyles from './styles'

const ControlAssignUserToCategory = ({
  keyword,
  setKeyword,
  handleAssignUser,
  handleSearch,
  t,
}) => {
  const classes = useStyles()
  return (
    <Paper style={{ marginTop: 16, padding: 16 }}>
      <Grid container justify="space-between" alignItems="center">
        <Grid item xs={4}>
          <TextField
            name="keyword"
            value={keyword}
            variant="outlined"
            placeholder={t('common:searchByName')}
            onChange={e => setKeyword(e.target.value)}
            className={classes.filterItem}
            label={t('common:searchByName')}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Search
                    color="primary"
                    className={classes.btnSearch}
                    onClick={handleSearch}
                  />
                </InputAdornment>
              ),
            }}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Grid>
        <Grid item xs={2}>
          <Tooltip title={t('article:addUser')} placement="top">
            <Button variant="outlined" onClick={handleAssignUser}>
              {t('article:addUser')}
            </Button>
          </Tooltip>
        </Grid>
      </Grid>
    </Paper>
  )
}

ControlAssignUserToCategory.propTypes = {
  keyword: string.isRequired,
  setKeyword: func.isRequired,
  handleAssignUser: func,
  handleSearch: func,
  t: func,
}

export default ControlAssignUserToCategory
