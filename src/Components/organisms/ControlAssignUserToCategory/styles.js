import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  filterWrap: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    // marginTop: 32,
  },
  btnSearch: {
    cursor: 'pointer',
  },
}))

export default useStyles
