import React from 'react'
import { object, bool, func } from 'prop-types'
import {
  Paper,
  Table,
  TableBody,
  TableHead,
  TableRow,
  TableCell,
  Tooltip,
  IconButton,
  CircularProgress,
} from '@material-ui/core'

import { Edit } from '@material-ui/icons'
import useStyles from './styles'

const HolidayTable = ({
  items,
  isLoadingList,
  // handleDeleteLeaveType,
  handleEditLeaveType,
  t,
}) => {
  const classes = useStyles()
  return (
    <Paper className={classes.tableWrap}>
      {isLoadingList && (
        <div className={classes.loadingWrap}>
          <CircularProgress size={56} />
        </div>
      )}
      {!isLoadingList && (
        <React.Fragment>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{t('leaveType:typeName')}</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {items && items.size !== 0 ? (
                items.map((item, index) => (
                  <TableRow key={item.get('typeId')}>
                    <TableCell>{item.get('typeName')}</TableCell>
                    <TableCell>
                      <Tooltip
                        placement="top"
                        title={t('leaveType:editLeaveType')}>
                        <IconButton
                          color="primary"
                          onClick={handleEditLeaveType(item)}>
                          <Edit />
                        </IconButton>
                      </Tooltip>
                      {/* <Tooltip
                        placement="top"
                        title={t('leaveType:deleteLeaveType')}>
                        <IconButton
                          color="secondary"
                          onClick={handleDeleteLeaveType(item.get('typeId'))}>
                          <Delete />
                        </IconButton>
                      </Tooltip> */}
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                <TableRow>
                  <TableCell colSpan="6" align="center">
                    {t('leaveType:noLeaveType')}
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </React.Fragment>
      )}
    </Paper>
  )
}

HolidayTable.propTypes = {
  items: object.isRequired,
  isLoadingList: bool.isRequired,
  // handleDeleteLeaveType: func.isRequired,
  handleEditLeaveType: func.isRequired,
  t: func,
}
export default HolidayTable
