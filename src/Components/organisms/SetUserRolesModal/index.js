import React from 'react'
import { Formik } from 'formik'
import { func, bool, object } from 'prop-types'

import {
  Slide,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  Typography,
  CircularProgress,
  Grid,
  Checkbox,
  Divider,
} from '@material-ui/core'

import useStyles from './styles'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const SetUserRolesModal = ({
  handleClose,
  isLoadingAction,
  handleAction,
  listRoles,
  setRolesItem,
  t,
}) => {
  const classes = useStyles()
  return (
    <Dialog
      open
      TransitionComponent={Transition}
      // onClose={handleClose}
      fullWidth
      maxWidth="sm">
      <DialogTitle disableTypography>
        <Typography gutterBottom variant="h5" align="center" color="primary">
          {t('user:userPermission')}
        </Typography>
      </DialogTitle>
      <Formik
        initialValues={setRolesItem}
        onSubmit={values => handleAction(values)}
        enableReinitialize
        render={({
          values,
          errors,
          handleBlur,
          handleChange,
          touched,
          handleSubmit,
          setFieldValue,
          setFieldTouched,
        }) => (
          <form onSubmit={handleSubmit}>
            <DialogContent>
              <Typography gutterBottom color="primary" variant="h6">
                {t('user:chooseUserPermission')}
              </Typography>
              <Grid container>
                <Grid item xs={4}>
                  {t('user:permissionName')}
                </Grid>
                <Grid item xs={4} />
              </Grid>
              <Divider />
              {listRoles &&
                listRoles.map(role => (
                  <Grid container key={role.get('roleId')} alignItems="center">
                    <Grid item xs={8}>
                      {role.get('name')}
                    </Grid>
                    <Grid>
                      <Checkbox
                        color="primary"
                        checked={values.roles.includes(role.get('roleId'))}
                        value={role.get('roleId')}
                        onChange={e => {
                          let value = e.target.checked ? +e.target.value : ''
                          let newRoles
                          if (value) {
                            newRoles = [...values.roles, value]
                          } else {
                            newRoles = values.roles.filter(
                              r => r !== role.get('roleId')
                            )
                          }
                          setFieldValue('roles', newRoles)
                        }}
                      />
                    </Grid>
                  </Grid>
                ))}
            </DialogContent>
            <DialogActions>
              <Button
                type="button"
                onClick={handleClose}
                variant="contained"
                fullWidth>
                {t('common:cancel')}
              </Button>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={isLoadingAction}>
                {!isLoadingAction && t('common:save')}
                {isLoadingAction && (
                  <CircularProgress size={32} className={classes.loadingItem} />
                )}
              </Button>
            </DialogActions>
          </form>
        )}
      />
    </Dialog>
  )
}

SetUserRolesModal.propTypes = {
  handleClose: func.isRequired,
  isLoadingAction: bool.isRequired,
  setRolesItem: object.isRequired,
  handleAction: func.isRequired,
  listRoles: object.isRequired,
  t: func,
}

export default SetUserRolesModal
