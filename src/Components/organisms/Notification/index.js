import React from 'react'
import { bool, string, func, number } from 'prop-types'
import { Snackbar, IconButton } from '@material-ui/core'
import Close from '@material-ui/icons/Close'
import { withTranslation } from 'react-i18next'

const Notification = ({ isOpen, message, duration, onClose, t }) => {
  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}
      open={isOpen}
      autoHideDuration={duration}
      onClose={onClose}
      message={t(message)}
      action={
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          onClick={onClose}>
          <Close />
        </IconButton>
      }
    />
  )
}

Notification.propTypes = {
  isOpen: bool,
  message: string.isRequired,
  duration: number.isRequired,
  onClose: func.isRequired,
}

export default withTranslation()(Notification)
