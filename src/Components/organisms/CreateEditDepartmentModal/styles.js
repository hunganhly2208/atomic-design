import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  textField: {
    margin: '8px 0',
  },
}))

export default useStyles
