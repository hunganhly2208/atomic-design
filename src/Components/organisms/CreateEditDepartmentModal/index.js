import React from 'react'
import { Formik } from 'formik'
import { func, bool, object, string, array } from 'prop-types'
import * as Yup from 'yup'
import {
  Slide,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Button,
  Typography,
  CircularProgress,
} from '@material-ui/core'
import useStyles from './styles'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const CreateEditDepartmentModal = ({
  handleClose,
  isLoadingAction,
  item,
  handleAction,
  action,
  t,
}) => {
  const classes = useStyles()
  return (
    <Dialog
      open
      TransitionComponent={Transition}
      // onClose={handleClose}
      fullWidth
      maxWidth="sm">
      <DialogTitle disableTypography>
        <Typography gutterBottom variant="h5" align="center" color="primary">
          {action === 'create'
            ? t('user:createDepartment')
            : t('user:editDepartment')}
        </Typography>
      </DialogTitle>
      <Formik
        initialValues={item}
        validationSchema={Yup.object().shape({
          name: Yup.string().required('Please input department name'),
        })}
        onSubmit={values => handleAction(values)}
        enableReinitialize
        render={({
          values,
          errors,
          handleBlur,
          handleChange,
          touched,
          handleSubmit,
          setFieldValue,
          setFieldTouched,
        }) => (
          <form onSubmit={handleSubmit}>
            <DialogContent>
              <TextField
                id="name"
                required
                className={classes.textField}
                fullWidth
                value={values.name}
                onChange={handleChange}
                onBlur={handleBlur}
                label={t('common:name')}
                error={touched.name && Boolean(errors.name)}
                helperText={touched.name && errors.name}
                placeholder={t('common:name')}
                variant="outlined"
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </DialogContent>
            <DialogActions>
              <Button
                type="button"
                onClick={handleClose}
                variant="contained"
                fullWidth>
                {t('common:cancel')}
              </Button>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={isLoadingAction}>
                {!isLoadingAction &&
                  (action === 'create' ? t('common:create') : t('common:save'))}
                {isLoadingAction && (
                  <CircularProgress size={32} className={classes.loadingItem} />
                )}
              </Button>
            </DialogActions>
          </form>
        )}
      />
    </Dialog>
  )
}

CreateEditDepartmentModal.propTypes = {
  handleClose: func.isRequired,
  isLoadingAction: bool.isRequired,
  item: object.isRequired,
  handleAction: func.isRequired,
  action: string.isRequired,
  t: func,
}

export default CreateEditDepartmentModal
