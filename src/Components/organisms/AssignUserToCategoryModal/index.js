import React from 'react'
import { Formik } from 'formik'
import { func, bool, object, string, array } from 'prop-types'

import {
  Slide,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  Typography,
  CircularProgress,
  FormGroup,
  FormControlLabel,
  Checkbox,
} from '@material-ui/core'
import SelectWithFormik from 'Components/molecules/SelectWithFormik'
import * as Yup from 'yup'

import useStyles from './styles'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const AssignUserToCategoryModal = ({
  handleClose,
  isLoadingAction,
  handleAction,
  item,
  searchUserOptions,
  handleSearch,
  clearSearchUsers,
  action,
  t,
}) => {
  const classes = useStyles()
  return (
    <Dialog
      open
      TransitionComponent={Transition}
      // onClose={handleClose}
      fullWidth
      maxWidth="sm">
      <DialogTitle disableTypography>
        <Typography gutterBottom variant="h5" align="center" color="primary">
          {t('article:assignUserToCategory')}
        </Typography>
      </DialogTitle>
      <Formik
        initialValues={item}
        onSubmit={values => handleAction(values)}
        enableReinitialize
        validationSchema={Yup.object().shape({
          userId: Yup.object().required('Please select User'),
          havePermission: Yup.boolean().when(['view', 'post', 'email'], {
            is: (view, post, email) => view || post || email,
            then: Yup.boolean().nullable(),
            otherwise: Yup.boolean().required(
              'Plese choose atleat one permission'
            ),
          }),
        })}
        render={({
          values,
          errors,
          handleBlur,
          handleChange,
          touched,
          handleSubmit,
          setFieldValue,
          setFieldTouched,
        }) => (
          <form onSubmit={handleSubmit}>
            <DialogContent>
              <Typography gutterBottom color="primary" variant="h6">
                {t('article:assignUserToCategory')}
              </Typography>
              {action === 'create' ? (
                <SelectWithFormik
                  name="userId"
                  value={values.userId}
                  onChange={setFieldValue}
                  onBlur={(name, isValidate) => {
                    clearSearchUsers()
                    setFieldTouched(name, isValidate)
                  }}
                  label={t('article:chooseUser')}
                  options={searchUserOptions}
                  error={touched.userId && errors.userId}
                  menuPlacement="top"
                  menuPosition="fixed"
                  onInputChange={handleSearch}
                  noOptionsMessage={str => 'No User Found'}
                />
              ) : (
                <Typography>{values.userName}</Typography>
              )}
              <FormGroup row>
                <FormControlLabel
                  control={
                    <Checkbox
                      name="view"
                      checked={values.view}
                      onChange={handleChange}
                      value={1}
                    />
                  }
                  label={t('article:view')}
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="post"
                      checked={values.post}
                      onChange={handleChange}
                      value={1}
                    />
                  }
                  label={t('article:post')}
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      name="email"
                      checked={values.email}
                      onChange={handleChange}
                      value={1}
                    />
                  }
                  label={t('article:email')}
                />
              </FormGroup>
              {errors.havePermission && (
                <Typography
                  align="center"
                  style={{ color: 'red' }}
                  variant="body2">
                  {errors.havePermission}
                </Typography>
              )}
            </DialogContent>
            <DialogActions>
              <Button
                type="button"
                onClick={handleClose}
                variant="contained"
                fullWidth>
                {t('common:cancel')}
              </Button>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={isLoadingAction}>
                {!isLoadingAction && action === 'create'
                  ? t('article:addUser')
                  : t('common:save')}
                {isLoadingAction && (
                  <CircularProgress size={32} className={classes.loadingItem} />
                )}
              </Button>
            </DialogActions>
          </form>
        )}
      />
    </Dialog>
  )
}

AssignUserToCategoryModal.propTypes = {
  handleClose: func.isRequired,
  isLoadingAction: bool.isRequired,
  handleAction: func.isRequired,
  item: object.isRequired,
  searchUserOptions: array.isRequired,
  handleSearch: func.isRequired,
  clearSearchUsers: func,
}

export default AssignUserToCategoryModal
