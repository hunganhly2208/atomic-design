import React from 'react'
import { object, bool, func, number } from 'prop-types'
import {
  Paper,
  Table,
  TableBody,
  TableHead,
  TableRow,
  TableCell,
  Tooltip,
  IconButton,
  CircularProgress,
  Button,
} from '@material-ui/core'
import { Delete, Edit, Assignment } from '@material-ui/icons'

import useStyles from './styles'

const DepartmentTable = ({
  items,
  isLoadingList,
  handleEditDepartment,
  handleAssignLeadDepartment,
  handleLoadMore,
  isShowMore,
  t,
}) => {
  const classes = useStyles()
  return (
    <Paper className={classes.tableWrap}>
      {isLoadingList && items.size === 0 && (
        <div className={classes.loadingWrap}>
          <CircularProgress size={56} />
        </div>
      )}
      {(!isLoadingList || items.size !== 0) && (
        <React.Fragment>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{t('user:no')}</TableCell>
                <TableCell>{t('common:name')}</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {items && items.size !== 0 ? (
                items.map((item, index) => (
                  <TableRow key={item.get('id')}>
                    <TableCell>{index + 1}</TableCell>
                    <TableCell>{item.get('name')}</TableCell>
                    <TableCell>
                      <Tooltip
                        placement="top"
                        title={t('user:assignDepartmentLead')}>
                        <IconButton
                          color="primary"
                          onClick={handleAssignLeadDepartment(item.get('id'))}>
                          <Assignment />
                        </IconButton>
                      </Tooltip>
                      <Tooltip placement="top" title={t('user:editDepartment')}>
                        <IconButton
                          color="primary"
                          onClick={handleEditDepartment(item)}>
                          <Edit />
                        </IconButton>
                      </Tooltip>
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                <TableRow>
                  <TableCell colSpan="6" align="center">
                    {t('user:noDepartment')}
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
          {isShowMore && (
            <div style={{ textAlign: 'center', marginTop: 8 }}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleLoadMore}>
                {t('common:loadMore')}
              </Button>
            </div>
          )}
        </React.Fragment>
      )}
    </Paper>
  )
}

DepartmentTable.propTypes = {
  items: object.isRequired,
  isLoadingList: bool.isRequired,
  handleEditDepartment: func.isRequired,
  handleAssignLeadDepartment: func.isRequired,
  handleLoadMore: func.isRequired,
  isShowMore: bool.isRequired,
  t: func,
}

export default DepartmentTable
