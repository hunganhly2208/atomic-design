import React from 'react'
import { object, bool, func, number } from 'prop-types'
import {
  Paper,
  Table,
  TableBody,
  TableHead,
  TableRow,
  TableCell,
  Tooltip,
  IconButton,
  CircularProgress,
} from '@material-ui/core'
import { Delete, Edit, Assignment } from '@material-ui/icons'
import Pagination from 'Components/molecules/Pagination'

import useStyles from './styles'

const UserTable = ({
  items,
  isLoadingList,
  handleEditUser,
  handleDeleteUser,
  filter,
  totalCount,
  handleChangePage,
  handleChangeRowsPerPage,
  handleSetUserRoles,
  handleOrganizeUser,
  t,
}) => {
  const classes = useStyles()
  return (
    <Paper className={classes.tableWrap}>
      {isLoadingList && (
        <div className={classes.loadingWrap}>
          <CircularProgress size={56} />
        </div>
      )}
      {!isLoadingList && (
        <React.Fragment>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{t('common:name')}</TableCell>
                <TableCell>{t('profile:email')}</TableCell>
                <TableCell>{t('profile:phone')}</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {items && items.size !== 0 ? (
                items.map((item, index) => (
                  <TableRow key={item.get('id')}>
                    <TableCell>{item.get('displayName')}</TableCell>
                    <TableCell>{item.get('email')}</TableCell>
                    <TableCell>{item.get('phone')}</TableCell>
                    <TableCell>
                      <Tooltip placement="top" title={t('user:setUserRole')}>
                        <IconButton
                          color="primary"
                          onClick={handleSetUserRoles(item.get('id'))}>
                          <Assignment />
                        </IconButton>
                      </Tooltip>
                      <Tooltip placement="top" title={t('user:organizeUser')}>
                        <IconButton
                          color="primary"
                          onClick={handleOrganizeUser(item.get('id'))}>
                          <Assignment />
                        </IconButton>
                      </Tooltip>
                      <Tooltip placement="top" title={t('user:editUser')}>
                        <IconButton
                          color="primary"
                          onClick={handleEditUser(item.get('id'))}>
                          <Edit />
                        </IconButton>
                      </Tooltip>
                      <Tooltip placement="top" title={t('user:deleteUser')}>
                        <IconButton
                          color="secondary"
                          onClick={handleDeleteUser(item.get('id'))}>
                          <Delete />
                        </IconButton>
                      </Tooltip>
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                <TableRow>
                  <TableCell colSpan="6" align="center">
                    {t('user:noUser')}
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
          <Pagination
            options={[5, 10, 15]}
            page={filter.get('page') - 1}
            rowsPerPage={filter.get('limit')}
            count={totalCount}
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </React.Fragment>
      )}
    </Paper>
  )
}

UserTable.propTypes = {
  items: object.isRequired,
  isLoadingList: bool.isRequired,
  handleEditUser: func.isRequired,
  handleDeleteUser: func.isRequired,
  filter: object.isRequired,
  totalCount: number.isRequired,
  handleChangePage: func.isRequired,
  handleChangeRowsPerPage: func.isRequired,
  handleOrganizeUser: func.isRequired,
  handleSetUserRoles: func.isRequired,
  t: func,
}

export default UserTable
