import React from 'react'
import { object, bool, func, number } from 'prop-types'
import {
  Paper,
  Table,
  TableBody,
  TableHead,
  TableRow,
  TableCell,
  Tooltip,
  IconButton,
  CircularProgress,
} from '@material-ui/core'
import { Edit, Delete } from '@material-ui/icons'
import Pagination from 'Components/molecules/Pagination'
import useStyles from './styles'

const EquipmentGroupsTable = ({
  items,
  isLoadingList,
  handleEditEquipmentGroup,
  handleDeleteEquipmentGroup,
  filter,
  totalCount,
  handleChangePage,
  handleChangeRowsPerPage,
  t,
}) => {
  const classes = useStyles()
  return (
    <Paper className={classes.tableWrap}>
      {isLoadingList && (
        <div className={classes.loadingWrap}>
          <CircularProgress size={56} />
        </div>
      )}
      {!isLoadingList && (
        <React.Fragment>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{t('user:no')}</TableCell>
                <TableCell>{t('common:name')}</TableCell>
                <TableCell>{t('common:status')}</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {items && items.size !== 0 ? (
                items.map((item, index) => (
                  <TableRow key={item.get('id')}>
                    <TableCell>{index + 1}</TableCell>
                    <TableCell>{item.get('name')}</TableCell>
                    <TableCell>
                      {item.get('status') == 1
                        ? t('equipmentGroup:active')
                        : t('equipmentGroup:inactive')}
                    </TableCell>
                    <TableCell>
                      <Tooltip placement="top" title={t('common:edit')}>
                        <IconButton color="primary" onClick={handleEditEquipmentGroup(item)}>
                          <Edit />
                        </IconButton>
                      </Tooltip>
                      <Tooltip placement="top" title={t('common:delete')}>
                        <IconButton color="secondary" onClick={handleDeleteEquipmentGroup(item.get('id'))}>
                          <Delete />
                        </IconButton>
                      </Tooltip>
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                  <TableRow>
                    <TableCell colSpan="6" align="center">
                      {t('equipmentGroup:noData')}
                    </TableCell>
                  </TableRow>
                )}
            </TableBody>
          </Table>
          <Pagination
            options={[5, 10, 15]}
            page={filter.get('page') - 1}
            rowsPerPage={filter.get('limit')}
            count={totalCount}
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </React.Fragment>
      )}
    </Paper>
  )
}

EquipmentGroupsTable.propTypes = {
  items: object.isRequired,
  isLoadingList: bool.isRequired,
  handleEditEquipmentGroups: func.isRequired,
  handleDeleteEquipmentGroups: func.isRequired,
  filter: object.isRequired,
  totalCount: number.isRequired,
  handleChangePage: func.isRequired,
  handleChangeRowsPerPage: func.isRequired,
  t: func,
}
export default EquipmentGroupsTable
