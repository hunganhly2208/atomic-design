import React from 'react'
import { Formik } from 'formik'
import { func, bool, object, string, array } from 'prop-types'
import * as Yup from 'yup'
import { DatePicker } from '@material-ui/pickers'
import {
  Slide,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Button,
  Typography,
  Grid,
  CircularProgress,
  FormGroup,
  Checkbox,
  FormControlLabel,
} from '@material-ui/core'
import useStyles from './styles'
import SelectWithFormik from 'Components/molecules/SelectWithFormik'
import UploadAvatar from '../../molecules/UploadAvatar'
import OutlineSelect from 'Components/atoms/OutlineSelect'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const CreateEditUserModal = ({
  handleClose,
  isLoadingAction,
  item,
  handleAction,
  action,
  locationOptions,
  departmentOptions,
  isLoadingItems,
  uploadAvatar,
  t,
}) => {
  const classes = useStyles()
  return (
    <Dialog
      open
      TransitionComponent={Transition}
      // onClose={handleClose}
      fullWidth
      maxWidth="sm">
      <DialogTitle disableTypography>
        <Typography gutterBottom variant="h5" align="center" color="primary">
          {action === 'create' ? t('user:createUser') : t('user:editUser')}
        </Typography>
      </DialogTitle>
      <Formik
        initialValues={item}
        validationSchema={Yup.object().shape({
          email: Yup.string()
            .required('Please input your email')
            .email('Invalid email'),
          password:
            action === 'create'
              ? Yup.string().required('Please input email')
              : Yup.string(),
          phone: Yup.string()
            .required('Please input phone number')
            .matches(
              /\b(086|096|097|098|032|033|034|035|036|037|038|039|089|090|093|070|079|077|076|078|088|091|094|083|084|085|081|082|092|056|058|099|059)+(\d{7})\b/,
              { excludeEmptyString: true, message: 'Invalid phone number' }
            ),
          displayName: Yup.string().required('Please input User Name'),
          jobTitle: Yup.string().required('Please input job title'),
        })}
        onSubmit={values => handleAction(values)}
        enableReinitialize
        render={({
          values,
          errors,
          handleBlur,
          handleChange,
          touched,
          handleSubmit,
          setFieldValue,
          setFieldTouched,
        }) => (
          <form onSubmit={handleSubmit}>
            <DialogContent>
              <div>
                <UploadAvatar
                  isLoadingItems={isLoadingItems}
                  uploadAvatar={uploadAvatar}
                  avatar={values.avatar}
                  setFieldValue={setFieldValue}
                />
                <Typography variant="h6" gutterBottom>
                  {t('user:userInformation')}
                </Typography>
                <TextField
                  id="email"
                  required
                  className={classes.textField}
                  fullWidth
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  label={t('profile:email')}
                  error={touched.email && Boolean(errors.email)}
                  helperText={touched.email && errors.email}
                  placeholder={t('profile:email')}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <TextField
                  id="phone"
                  required
                  className={classes.textField}
                  fullWidth
                  value={values.phone}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  label={t('profile:phone')}
                  error={touched.phone && Boolean(errors.phone)}
                  helperText={touched.phone && errors.phone}
                  placeholder={t('profile:phone')}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                {action === 'create' && (
                  <TextField
                    id="password"
                    required
                    className={classes.textField}
                    fullWidth
                    value={values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    label={t('profile:password')}
                    type="password"
                    error={touched.password && Boolean(errors.password)}
                    helperText={touched.password && errors.password}
                    placeholder={t('profile:password')}
                    variant="outlined"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}
                <TextField
                  id="displayName"
                  required
                  className={classes.textField}
                  fullWidth
                  value={values.displayName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  label={t('user:displayName')}
                  error={touched.displayName && Boolean(errors.displayName)}
                  helperText={touched.displayName && errors.displayName}
                  placeholder={t('common:name')}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <Grid container spacing={2}>
                  <Grid item xs={6}>
                    <TextField
                      id="firstName"
                      required
                      className={classes.textField}
                      fullWidth
                      value={values.firstName}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      label={t('user:firstName')}
                      error={touched.firstName && Boolean(errors.firstName)}
                      helperText={touched.firstName && errors.firstName}
                      placeholder={t('user:firstName')}
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <TextField
                      id="lastName"
                      required
                      className={classes.textField}
                      fullWidth
                      value={values.lastName}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      label={t('user:lastName')}
                      error={touched.lastName && Boolean(errors.lastName)}
                      helperText={touched.lastName && errors.lastName}
                      placeholder={t('user:lastName')}
                      variant="outlined"
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                  </Grid>
                </Grid>
                <Grid container spacing={2} alignItems="center">
                  <Grid item xs={6}>
                    <OutlineSelect
                      fullWidth
                      options={[
                        {
                          value: '',
                          label: t('user:selectGender'),
                          disabled: true,
                        },
                        { value: 'female', label: t('user:female') },
                        { value: 'male', label: t('user:male') },
                      ]}
                      className={classes.filterItem}
                      placeholder={t('user:selectGender')}
                      label={t('profile:gender')}
                      name="gender"
                      value={values.gender}
                      handleChange={handleChange}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <DatePicker
                      className={classes.textField}
                      autoOk
                      id="joinMonth"
                      variant="inline"
                      label={t('user:joinDate')}
                      required
                      emptyLabel={t('user:chooseJoinDate')}
                      inputVariant="outlined"
                      format="DD/MM/YYYY"
                      error={touched.joinMonth && Boolean(errors.joinMonth)}
                      helperText={touched.joinMonth && errors.joinMonth}
                      value={values.joinMonth}
                      fullWidth
                      onChange={date => setFieldValue('joinMonth', date)}
                    />
                  </Grid>
                </Grid>
                <TextField
                  id="jobTitle"
                  required
                  className={classes.textField}
                  fullWidth
                  value={values.jobTitle}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  label={t('profile:jobTitle')}
                  error={touched.jobTitle && Boolean(errors.jobTitle)}
                  helperText={touched.jobTitle && errors.jobTitle}
                  placeholder={t('profile:jobTitle')}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />

                {action === 'create' && (
                  <SelectWithFormik
                    name="office"
                    value={values.office}
                    onChange={setFieldValue}
                    onBlur={setFieldTouched}
                    label={t('user:office')}
                    options={locationOptions}
                    menuPlacement="top"
                  />
                )}
                {action === 'create' && (
                  <SelectWithFormik
                    name="department"
                    value={values.department}
                    onChange={setFieldValue}
                    onBlur={setFieldTouched}
                    label={t('user:department')}
                    options={departmentOptions}
                    menuPlacement="top"
                  />
                )}
                <FormGroup row style={{ justifyContent: 'center' }}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        name="didSaturday"
                        checked={values.didSaturday}
                        onChange={handleChange}
                        color="primary"
                        value={1}
                      />
                    }
                    label={t('user:workInSaturday')}
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        name="didSunday"
                        checked={values.didSunday}
                        onChange={handleChange}
                        color="primary"
                        value={1}
                      />
                    }
                    label={t('user:workInSunday')}
                  />
                </FormGroup>
              </div>
            </DialogContent>
            <DialogActions>
              <Button
                type="button"
                onClick={handleClose}
                variant="contained"
                fullWidth>
                {t('common:cancel')}
              </Button>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={isLoadingAction}>
                {!isLoadingAction &&
                  (action === 'create' ? t('common:create') : t('common:save'))}
                {isLoadingAction && (
                  <CircularProgress size={32} className={classes.loadingItem} />
                )}
              </Button>
            </DialogActions>
          </form>
        )}
      />
    </Dialog>
  )
}

CreateEditUserModal.propTypes = {
  handleClose: func.isRequired,
  isLoadingAction: bool.isRequired,
  item: object.isRequired,
  handleAction: func.isRequired,
  action: string.isRequired,
  locationOptions: array.isRequired,
  departmentOptions: array.isRequired,
  isLoadingItems: object.isRequired,
  uploadAvatar: func.isRequired,
  t: func,
}

export default CreateEditUserModal
