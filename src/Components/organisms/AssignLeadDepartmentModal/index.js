import React from 'react'
import { Formik } from 'formik'
import { func, bool, object, string, array } from 'prop-types'

import {
  Slide,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  Typography,
  CircularProgress,
} from '@material-ui/core'
import SelectWithFormik from 'Components/molecules/SelectWithFormik'

import useStyles from './styles'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const AssignLeadDepartmentModal = ({
  handleClose,
  isLoadingAction,
  handleAction,
  assignItem,
  searchUserOptions,
  handleSearch,
  clearSearchUsers,
  t,
}) => {
  const classes = useStyles()
  return (
    <Dialog
      open
      TransitionComponent={Transition}
      // onClose={handleClose}
      fullWidth
      maxWidth="sm">
      <DialogTitle disableTypography>
        <Typography gutterBottom variant="h5" align="center" color="primary">
          {t('user:assignDepartmentLead')}
        </Typography>
      </DialogTitle>
      <Formik
        initialValues={assignItem}
        onSubmit={values => handleAction(values)}
        enableReinitialize
        render={({
          values,
          errors,
          handleBlur,
          handleChange,
          touched,
          handleSubmit,
          setFieldValue,
          setFieldTouched,
        }) => (
          <form onSubmit={handleSubmit}>
            <DialogContent>
              <Typography gutterBottom color="primary" variant="h6">
                {t('user:chooseDepartmentLeader')}
              </Typography>
              <SelectWithFormik
                name="leaders"
                value={values.leaders}
                onChange={setFieldValue}
                onBlur={(name, isValidate) => {
                  clearSearchUsers()
                  setFieldTouched(name, isValidate)
                }}
                label={t('user:chooseDepartmentLeader')}
                options={searchUserOptions}
                menuPlacement="top"
                menuPosition="fixed"
                onInputChange={handleSearch}
                noOptionsMessage={str => 'No User Found'}
                isMulti
              />
            </DialogContent>
            <DialogActions>
              <Button
                type="button"
                onClick={handleClose}
                variant="contained"
                fullWidth>
                {t('common:cancel')}
              </Button>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={isLoadingAction}>
                {!isLoadingAction && t('common:save')}
                {isLoadingAction && (
                  <CircularProgress size={32} className={classes.loadingItem} />
                )}
              </Button>
            </DialogActions>
          </form>
        )}
      />
    </Dialog>
  )
}

AssignLeadDepartmentModal.propTypes = {
  handleClose: func.isRequired,
  isLoadingAction: bool.isRequired,
  handleAction: func.isRequired,
  assignItem: object.isRequired,
  searchUserOptions: array.isRequired,
  handleSearch: func.isRequired,
  clearSearchUsers: func.isRequired,
  t: func,
}

export default AssignLeadDepartmentModal
