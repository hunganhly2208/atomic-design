import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Tooltip,
  Menu as MuiMenu,
  MenuItem,
  Fade,
} from '@material-ui/core'
import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'
import { Menu, Language } from '@material-ui/icons'
import PropTypes from 'prop-types'
import { getImageUrl } from 'Utils/getImageUrl'
import { handleLinkExternal } from 'Utils/handleLinkExternal'
import { withTranslation } from 'react-i18next'
import styles from './styles'
import defaultAvatar from 'Assets/images/male.png'
import logo from 'Assets/images/logo_hyec.png'
import { getLang, setLang } from 'Utils/token'

function NavBar({
  handleShowDrawer,
  classes,
  viewMode,
  handleShowProfileDrawer,
  userData,
  i18n,
  t,
}) {
  // Check language
  useEffect(() => {
    const lang = getLang()
    if (lang !== i18n.language) {
      i18n.changeLanguage(lang)
    }
  }, [getLang])
  const handleChangeLanguage = lang => () => {
    i18n.changeLanguage(lang)
    setLang(lang)
    setAnchorEl(null)
  }
  const [anchorEl, setAnchorEl] = useState(null)
  const open = Boolean(anchorEl)
  function handleOpenMenuLanguage(event) {
    setAnchorEl(event.currentTarget)
  }

  function handleClose(e) {
    setAnchorEl(null)
  }

  return (
    <AppBar position="fixed">
      <Toolbar>
        {viewMode !== 1 && (
          <IconButton onClick={handleShowDrawer} className={classes.icon}>
            <Menu />
          </IconButton>
        )}
        <div className={classes.appBarLogo} onClick={handleLinkExternal('/')}>
          <img src={logo} alt="MySquare Logo" />
        </div>
        <Tooltip title={t('common:changeLanguage')}>
          <IconButton
            aria-controls="fade-menu"
            aria-haspopup="true"
            color="inherit"
            onClick={handleOpenMenuLanguage}>
            <Language />
          </IconButton>
        </Tooltip>
        <MuiMenu
          id="fade-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open}
          onClose={handleClose}
          TransitionComponent={Fade}>
          <MenuItem onClick={handleChangeLanguage('vn')}>Việt Nam</MenuItem>
          <MenuItem onClick={handleChangeLanguage('en')}>English</MenuItem>
        </MuiMenu>
        <Typography style={{ marginRight: 16, textTransform: 'uppercase' }}>
          {i18n.language}
        </Typography>
        {userData && (
          <Typography style={{ marginRight: 8 }}>
            {userData.displayName}
          </Typography>
        )}
        {userData && (
          <div>
            <IconButton
              aria-haspopup="true"
              aria-label="Account of current user"
              onClick={handleShowProfileDrawer}
              style={{ padding: 0, fontSize: 0 }}
              color="inherit"
              aria-controls="show-profile">
              <div style={{ textAlign: 'center' }}>
                <img
                  alt="Avatar"
                  src={getImageUrl(userData.avatar) || defaultAvatar}
                  style={{
                    objectFit: 'cover',
                    color: '#fff',
                    backgroundColor: 'none',
                    userSelect: 'none',
                    display: 'inline-flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    fontSize: '20px',
                    borderRadius: '50%',
                    height: '40px',
                    width: '40px',
                    padding: '3px',
                    border: '1px solid #fff',
                    boxSshadow: 'rgb(0, 143, 195) 0px 0px 10px 0px',
                  }}
                />
              </div>
            </IconButton>
          </div>
        )}
      </Toolbar>
    </AppBar>
  )
}

NavBar.propTypes = {
  handleShowDrawer: PropTypes.func,
  classes: PropTypes.object,
  viewMode: PropTypes.number,
  handleShowProfileDrawer: PropTypes.func,
  userData: PropTypes.object,
  i18n: PropTypes.object,
  t: PropTypes.func,
}

export default compose(
  withRouter,
  withStyles(styles),
  withTranslation()
)(NavBar)
