const styles = theme => ({
  textField: {
    margin: '8px 0',
  },
  // title: {
  //   color: '#fff',
  //   textTransform: 'uppercase',
  //   fontWeight: 'bold',
  //   marginLeft: 10,
  //   flexGrow: 1,
  // },
  icon: {
    color: '#fff',
  },
  appBarLogo: {
    textAlign: `center`,
    height: `100%`,
    flexGrow: 1,
    display: `flex`,
    alignItems: `center`,
    justifyContent: `center`,
    '& img': {
      maxWidth: 190,
      cursor: 'pointer',
      objectFit: 'cover',
    },
    '@media (min-width: 768px)': {
      textAlign: 'left',
      justifyContent: 'start',
    },
  },
})

export default styles
