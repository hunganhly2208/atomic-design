import React from 'react'
import { object, bool, func, number } from 'prop-types'
import {
  Paper,
  Table,
  TableBody,
  TableHead,
  TableRow,
  TableCell,
  Tooltip,
  IconButton,
  CircularProgress,
} from '@material-ui/core'
import moment from 'moment'
import { Delete } from '@material-ui/icons'

import useStyles from './styles'

const HolidayTable = ({ items, isLoadingList, handeDeleteHoliday, t }) => {
  const classes = useStyles()
  return (
    <Paper className={classes.tableWrap}>
      {isLoadingList && (
        <div className={classes.loadingWrap}>
          <CircularProgress size={56} />
        </div>
      )}
      {!isLoadingList && (
        <React.Fragment>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{t('common:name')}</TableCell>
                <TableCell>{t('common:date')}</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {items && items.size !== 0 ? (
                items.map((item, index) => (
                  <TableRow key={item.get('id')}>
                    <TableCell>{item.get('name')}</TableCell>
                    <TableCell>
                      {moment(item.get('date')).format('DD-MM-YYYY')}
                    </TableCell>
                    <TableCell>
                      <Tooltip placement="top" title={t('user:deleteHoliday')}>
                        <IconButton
                          color="secondary"
                          onClick={handeDeleteHoliday(item.get('id'))}>
                          <Delete />
                        </IconButton>
                      </Tooltip>
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                <TableRow>
                  <TableCell colSpan="6" align="center">
                    {t('user:noHoliday')}
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </React.Fragment>
      )}
    </Paper>
  )
}

HolidayTable.propTypes = {
  items: object.isRequired,
  isLoadingList: bool.isRequired,
  handeDeleteHoliday: func.isRequired,
  t: func,
}
export default HolidayTable
