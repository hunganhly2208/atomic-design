import React from 'react'
import { Formik } from 'formik'
import { func, bool, object, string } from 'prop-types'
import * as Yup from 'yup'
import {
  Slide,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Button,
  Typography,
  CircularProgress,
} from '@material-ui/core'
import useStyles from './styles'
import OutlineSelect from 'Components/atoms/OutlineSelect'
import RteEditor from '../../molecules/RteEditor'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const CreateEditLeaveTypeModal = ({
  handleClose,
  isLoadingAction,
  item,
  handleAction,
  action,
  t,
}) => {
  const classes = useStyles()
  return (
    <Dialog
      open
      TransitionComponent={Transition}
      // onClose={handleClose}
      fullWidth
      maxWidth="sm">
      <DialogTitle disableTypography>
        <Typography gutterBottom variant="h5" align="center" color="primary">
          {action === 'create'
            ? t('leaveType:createLeaveType')
            : t('leaveType:editLeaveType')}
        </Typography>
      </DialogTitle>
      <Formik
        initialValues={item}
        validationSchema={Yup.object().shape({
          typeName: Yup.string().required('Please input type name'),
        })}
        onSubmit={values => handleAction(values)}
        enableReinitialize
        render={({
          values,
          errors,
          handleBlur,
          handleChange,
          touched,
          handleSubmit,
          setFieldValue,
          setFieldTouched,
        }) => (
          <form onSubmit={handleSubmit}>
            <DialogContent>
              <div>
                <TextField
                  id="typeName"
                  required
                  className={classes.textField}
                  fullWidth
                  value={values.typeName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  label={t('leaveType:typeName')}
                  error={touched.typeName && Boolean(errors.typeName)}
                  helperText={touched.typeName && errors.typeName}
                  placeholder={t('leaveType:typeName')}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                <RteEditor
                  value={values.description}
                  handleChange={value => {
                    setFieldValue('description', value)
                  }}
                />
                <OutlineSelect
                  fullWidth
                  className={classes.textField}
                  options={[
                    {
                      value: '',
                      label: t('leaveType:both'),
                      disabled: true,
                    },
                    { value: 'female', label: t('user:female') },
                    { value: 'male', label: t('user:male') },
                  ]}
                  placeholder={t('user:selectGender')}
                  label={t('profile:gender')}
                  name="gender"
                  value={values.gender}
                  handleChange={handleChange}
                />
              </div>
            </DialogContent>
            <DialogActions>
              <Button
                type="button"
                onClick={handleClose}
                variant="contained"
                fullWidth>
                {t('common:cancel')}
              </Button>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={isLoadingAction}>
                {!isLoadingAction &&
                  (action === 'create' ? t('common:create') : t('common:save'))}
                {isLoadingAction && (
                  <CircularProgress size={32} className={classes.loadingItem} />
                )}
              </Button>
            </DialogActions>
          </form>
        )}
      />
    </Dialog>
  )
}

CreateEditLeaveTypeModal.propTypes = {
  handleClose: func.isRequired,
  isLoadingAction: bool.isRequired,
  item: object.isRequired,
  handleAction: func.isRequired,
  action: string.isRequired,
  t: func,
}

export default CreateEditLeaveTypeModal
