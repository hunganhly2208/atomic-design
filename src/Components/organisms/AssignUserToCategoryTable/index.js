import React from 'react'
import { object, bool, func, number } from 'prop-types'
import {
  Paper,
  Table,
  TableBody,
  TableHead,
  TableRow,
  TableCell,
  Tooltip,
  IconButton,
  CircularProgress,
} from '@material-ui/core'
import { Delete, Edit, Check, Close } from '@material-ui/icons'
import Pagination from 'Components/molecules/Pagination'

import useStyles from './styles'

const AssignUserToCategoryTable = ({
  items,
  isLoadingList,
  filter,
  totalCount,
  handleDeleteCategoryUser,
  handleEditCategoryUser,
  handleChangePage,
  handleChangeRowsPerPage,
  t,
}) => {
  const classes = useStyles()
  return (
    <Paper className={classes.tableWrap}>
      {isLoadingList && (
        <div className={classes.loadingWrap}>
          <CircularProgress size={56} />
        </div>
      )}
      {!isLoadingList && (
        <React.Fragment>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{t('common:name')}</TableCell>
                <TableCell>{t('article:view')}</TableCell>
                <TableCell>{t('article:post')}</TableCell>
                <TableCell>{t('article:email')}</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {items && items.size !== 0 ? (
                items.map((item, index) => (
                  <TableRow key={item.get('id')}>
                    <TableCell>{item.get('userName')}</TableCell>
                    <TableCell>
                      {item.get('view') === 1 ? (
                        <Check color="primary" />
                      ) : (
                        <Close />
                      )}
                    </TableCell>
                    <TableCell>
                      {item.get('post') === 1 ? (
                        <Check color="primary" />
                      ) : (
                        <Close />
                      )}
                    </TableCell>
                    <TableCell>
                      {item.get('email') === 1 ? (
                        <Check color="primary" />
                      ) : (
                        <Close />
                      )}
                    </TableCell>
                    <TableCell>
                      <Tooltip placement="top" title={t('common:edit')}>
                        <IconButton
                          color="primary"
                          onClick={handleEditCategoryUser(item)}>
                          <Edit />
                        </IconButton>
                      </Tooltip>
                      <Tooltip placement="top" title={t('common:delete')}>
                        <IconButton
                          color="secondary"
                          onClick={handleDeleteCategoryUser(item.get('id'))}>
                          <Delete />
                        </IconButton>
                      </Tooltip>
                    </TableCell>
                  </TableRow>
                ))
              ) : (
                <TableRow>
                  <TableCell colSpan="6" align="center">
                    {t('user:noUser')}
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
          <Pagination
            options={[5, 10, 15]}
            page={filter.get('page') - 1}
            rowsPerPage={filter.get('limit')}
            count={totalCount}
            handleChangePage={handleChangePage}
            handleChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </React.Fragment>
      )}
    </Paper>
  )
}

AssignUserToCategoryTable.propTypes = {
  items: object.isRequired,
  isLoadingList: bool.isRequired,
  filter: object.isRequired,
  totalCount: number.isRequired,
  handleChangePage: func.isRequired,
  handleChangeRowsPerPage: func.isRequired,
  handleDeleteCategoryUser: func,
  handleEditCategoryUser: func,
  t: func,
}

export default AssignUserToCategoryTable
