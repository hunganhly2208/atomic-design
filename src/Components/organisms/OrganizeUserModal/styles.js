import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  textField: {
    margin: '8px 0',
  },
  officeRow: {
    borderBottom: '1px solid #cacaca',
  },
}))

export default useStyles
