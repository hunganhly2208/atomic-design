import React from 'react'
import { Formik } from 'formik'
import { func, bool, object, array } from 'prop-types'
import {
  Slide,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
  Typography,
  CircularProgress,
  Grid,
  Checkbox,
} from '@material-ui/core'
import SelectWithFormik from 'Components/molecules/SelectWithFormik'
import useStyles from './styles'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />
})

const OrganizeUserModal = ({
  handleClose,
  isLoadingAction,
  formAssignItem,
  handleAction,
  listOffices,
  departmentOptions,
  t,
}) => {
  const getSubDepartmentOptions = (options, mainDepartment) => {
    return options.filter(option => option.value !== mainDepartment.value)
  }
  const classes = useStyles()
  return (
    <Dialog
      open
      TransitionComponent={Transition}
      // onClose={handleClose}
      fullWidth
      maxWidth="sm">
      <DialogTitle disableTypography>
        <Typography gutterBottom variant="h5" align="center" color="primary">
          {t('user:organization')}
        </Typography>
      </DialogTitle>
      <Formik
        initialValues={formAssignItem}
        onSubmit={values => handleAction(values)}
        enableReinitialize
        render={({
          values,
          errors,
          handleBlur,
          handleChange,
          touched,
          handleSubmit,
          setFieldValue,
          setFieldTouched,
        }) => (
          <form onSubmit={handleSubmit}>
            <DialogContent>
              <Typography color="primary" gutterBottom variant="h6">
                {t('user:office')}
              </Typography>
              <Grid container className={classes.officeRow}>
                <Grid item xs={6}>
                  <Typography>{t('user:officeName')}</Typography>
                </Grid>
                <Grid item xs={3}>
                  <Typography>{t('user:mainOffice')}</Typography>
                </Grid>
                <Grid item xs={3}>
                  <Typography>{t('user:subOffice')}</Typography>
                </Grid>
              </Grid>
              {listOffices &&
                listOffices.map(office => (
                  <Grid container key={office.get('id')} alignItems="center">
                    <Grid item xs={6}>
                      <Typography>{office.get('name')}</Typography>
                    </Grid>
                    <Grid item xs={3}>
                      <Checkbox
                        checked={values.mainOffice === office.get('id')}
                        value={office.get('id')}
                        color="primary"
                        onChange={e => {
                          let value = e.target.checked ? +e.target.value : ''
                          // Set main office new value
                          setFieldValue('mainOffice', value)
                          // Filter subValues
                          if (value) {
                            let newSubOffices = values.subOffices.filter(
                              subOffice => subOffice !== value
                            )
                            setFieldValue('subOffices', newSubOffices)
                          }
                        }}
                      />
                    </Grid>
                    <Typography item xs={3}>
                      <Checkbox
                        checked={values.subOffices.includes(office.get('id'))}
                        value={office.get('id')}
                        color="primary"
                        onChange={e => {
                          // Check if value same as mainOffice
                          let value = e.target.checked ? +e.target.value : ''
                          if (value === values.mainOffice) {
                            setFieldValue('mainOffice', '')
                          }
                          let newSubOffices
                          if (value) {
                            newSubOffices = [...values.subOffices, value]
                          } else {
                            newSubOffices = values.subOffices.filter(
                              subOffice => subOffice !== office.get('id')
                            )
                          }
                          setFieldValue('subOffices', newSubOffices)
                        }}
                      />
                    </Typography>
                  </Grid>
                ))}
              <Typography variant="h6" gutterBottom color="primary">
                {t('user:department')}
              </Typography>
              <SelectWithFormik
                name="mainDepartment"
                value={values.mainDepartment}
                onChange={(name, value) => {
                  let newSubDepartments = values.subDepartments.filter(
                    subDepartment => subDepartment.value !== value.value
                  )
                  setFieldValue('subDepartments', newSubDepartments)
                  setFieldValue(name, value)
                }}
                onBlur={setFieldTouched}
                label={t('user:mainDepartment')}
                options={departmentOptions}
                menuPosition="fixed"
                menuPlacement="top"
              />
              <SelectWithFormik
                name="subDepartments"
                value={values.subDepartments}
                onChange={setFieldValue}
                onBlur={setFieldTouched}
                label={t('user:subDepartment')}
                options={getSubDepartmentOptions(
                  departmentOptions,
                  values.mainDepartment
                )}
                menuPlacement="top"
                isMulti
              />
            </DialogContent>
            <DialogActions>
              <Button
                type="button"
                onClick={handleClose}
                variant="contained"
                fullWidth>
                {t('common:cancel')}
              </Button>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={isLoadingAction}>
                {!isLoadingAction && t('common:save')}
                {isLoadingAction && (
                  <CircularProgress size={32} className={classes.loadingItem} />
                )}
              </Button>
            </DialogActions>
          </form>
        )}
      />
    </Dialog>
  )
}

OrganizeUserModal.propTypes = {
  handleClose: func.isRequired,
  isLoadingAction: bool.isRequired,
  formAssignItem: object.isRequired,
  handleAction: func.isRequired,
  listOffices: object.isRequired,
  departmentOptions: array.isRequired,
  t: func,
}

export default OrganizeUserModal
