import { makeStyles } from '@material-ui/styles'
const styles = theme => ({
  drawerPaper: {
    width: 305,
    padding: 0,
    border: 'none',
    // backgroundColor: theme.palette.primary.light,
  },
  menuHeader: {
    flex: `0 0 64px`,
    display: `flex`,
    alignItems: `center`,
    justifyContent: `center`,
    backgroundColor: theme.palette.primary.main,
    '& img': {
      width: 190,
      cursor: 'pointer',
      objectFit: 'cover',
    },
  },
  menuItemWrap: {
    flex: 1,
    overflow: `auto`,
    paddingBottom: 15,
  },
})

export default makeStyles(styles)
