import React, { useContext } from 'react'
import { func, bool, number } from 'prop-types'
import { Drawer as MuiDrawer, List, Divider } from '@material-ui/core'
import useStyles from './styles'
import MenuDrawer from '../../molecules/MenuDrawer'
import Copyright from 'Components/molecules/Copyright'
import logo from 'Assets/images/logo_hyec.png'
import { handleLinkExternal } from 'Utils/handleLinkExternal'
import { withTranslation } from 'react-i18next'
import { UserContext } from 'Contexts/UserData'
import { canAccess } from 'Utils/checkPermission'

const Drawer = ({ handleHideDrawer, isShowDrawer, viewMode, t }) => {
  let classes = useStyles({ viewMode })
  const user = useContext(UserContext)
  return (
    <MuiDrawer
      open={isShowDrawer}
      onClose={handleHideDrawer}
      variant={viewMode === 1 ? 'permanent' : ''}
      classes={{ paper: classes.drawerPaper }}>
      <div className={classes.menuHeader} onClick={handleLinkExternal('/')}>
        <img src={logo} alt="MySquare Logo" />
      </div>
      <List style={{ padding: 0 }}>
        <MenuDrawer
          isButton
          label={t('drawer:homepage')}
          handleClick={handleLinkExternal('/')}
        />
        <MenuDrawer
          isButton
          handleClick={handleLinkExternal('/my-colleagues')}
          label={t('drawer:myColleagues')}
        />
        <MenuDrawer
          isButton
          label={t('drawer:meetingRoomBooking')}
          handleClick={handleLinkExternal('/meeting-room-booking')}
        />
        <MenuDrawer
          isButton
          label={t('drawer:leaveRequest')}
          handleClick={handleLinkExternal('/leave-request/l')}
        />
        {user.permissions && canAccess(user.permissions, 'leave.manage') && (
          <MenuDrawer
            isButton
            label={t('drawer:leaveRequestManager')}
            handleClick={handleLinkExternal('/leave-request/manager')}
          />
        )}
        <MenuDrawer
          isButton
          handleClick={handleLinkExternal('/staffs-calendar')}
          label={t('drawer:staffCalendar')}
        />
        <MenuDrawer
          isButton
          handleClick={handleLinkExternal('/stationery-booking/s/')}
          label={t('drawer:stationeryBooking')}
        />
        <MenuDrawer
          isButton
          handleClick={handleLinkExternal('/stationery-requests')}
          label={t('drawer:stationeryRequestItem')}
        />
        {/* <MenuDrawer
          isButton
          handleClick={handleLinkExternal('/help')}
          label="Help"
        /> */}
        <Divider style={{ margin: '10px 0' }} />
        {user.permissions && user.permissions.length !== 0 && (
          <MenuDrawer
            isButton
            handleClick={handleLinkExternal('/admin-tools')}
            label={t('drawer:managementTools')}
          />
        )}
        <Copyright t={t} color="#9b9b9b" fontSize="11px" />
      </List>
    </MuiDrawer>
  )
}

Drawer.propTypes = {
  handleHideDrawer: func.isRequired,
  isShowDrawer: bool.isRequired,
  viewMode: number.isRequired,
}

export default withTranslation()(Drawer)
