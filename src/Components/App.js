import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { CssBaseline } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
import PrivateRouteWithTemplate from '../Containers/PrivateRouteWithTemplate'
import ErrorBoundary from './pages/ErrorBoundary'
import * as routes from './routes'
import theme from '../Theme'

const App = () => (
  <MuiThemeProvider theme={theme}>
    <CssBaseline />
    <ErrorBoundary>
      <Switch>
        <Redirect exact from="/" to="/user" />
        <PrivateRouteWithTemplate
          path="/user"
          exact
          component={routes.AsyncUser}
        />
        <PrivateRouteWithTemplate
          path="/office-management"
          exact
          component={routes.AsyncOfficeManagement}
        />
        <PrivateRouteWithTemplate
          path="/department-management"
          exact
          component={routes.AsyncDepartmentManagement}
        />
        <PrivateRouteWithTemplate
          path="/holiday-management"
          exact
          component={routes.AsyncHolidayManagement}
        />
        <PrivateRouteWithTemplate
          path="/news-category/assign/:categoryId"
          exact
          component={routes.AsyncAssignUserToCategory}
        />
        <PrivateRouteWithTemplate
          path="/profile/edit/:userId"
          exact
          component={routes.AsyncEditProfile}
        />
        <PrivateRouteWithTemplate
          path="/leave-type-management"
          exact
          component={routes.AsyncLeaveTypeManagement}
        />
        <PrivateRouteWithTemplate
          path="/equipment-groups"
          exact
          component={routes.AsyncEquipmentGroups}
        />
        <PrivateRouteWithTemplate
          path="/equipment"
          exact
          component={routes.AsyncEquipment}
        />

        {/* <Route path="/loading" component={routes.AsyncLoading} /> */}
        <Route component={routes.AsyncNotFound} />
      </Switch>
    </ErrorBoundary>
  </MuiThemeProvider>
)

export default App
