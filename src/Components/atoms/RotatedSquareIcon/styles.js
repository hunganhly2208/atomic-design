import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  icon: {
    position: 'absolute',
    width: `11px !important`,
    height: `11px !important`,
    top: `50% !important`,
    left: `25px !important`,
    borderRadius: `3px`,
    border: `solid 2px ${theme.palette.primary.main}`,
    transform: `rotate(-20deg) translateY(-50%)`,
    transition: `all 0.2s ease`,
    // &.active {
    //   background-color: ${props => props.theme.palette.primary1Color} !important;
    // }
  },
}))

export default useStyles
