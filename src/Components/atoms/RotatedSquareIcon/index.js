import React from 'react'
import PropTypes from 'prop-types'
import useStyles from './styles'

const RotatedSquareIcon = props => {
  let classes = useStyles()
  return <span className={classes.icon} {...props} />
}

RotatedSquareIcon.propTypes = {
  className: PropTypes.string,
}

export default RotatedSquareIcon
