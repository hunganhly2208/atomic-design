import React, { useRef, useState, useEffect } from 'react'
import { array, string, func, bool } from 'prop-types'
import {
  FormControl,
  InputLabel,
  Select,
  OutlinedInput,
  FormHelperText,
} from '@material-ui/core'
import useStyles from './styles'

const OutlineSelect = ({
  options,
  placeholder,
  label,
  name,
  value,
  handleChange,
  handleBlur,
  error,
  helperText,
  ...props
}) => {
  const inputLabel = useRef(null)
  const [labelWith, setLabelWith] = useState(0)
  useEffect(() => {
    setLabelWith(inputLabel.current.offsetWidth)
  }, [])
  const classes = useStyles()
  return (
    <FormControl
      variant="outlined"
      error={error}
      className={classes.formControl}
      {...props}>
      <InputLabel ref={inputLabel} htmlFor="outlined-select" shrink>
        {label}
      </InputLabel>
      <Select
        native
        value={value}
        onChange={handleChange}
        onBlur={handleBlur}
        displayEmpty
        input={
          <OutlinedInput
            name={name}
            labelWidth={labelWith}
            id="outlined-select"
            notched
          />
        }>
        {/* <option value="" disabled className={classes.placeholder}>
          {placeholder}
        </option> */}
        {options &&
          options.map((option, index) => (
            <option value={option.value} key={index}>
              {option.label}
            </option>
          ))}
      </Select>
      {error && helperText && <FormHelperText>{helperText}</FormHelperText>}
    </FormControl>
  )
}

OutlineSelect.propTypes = {
  options: array.isRequired,
  placeholder: string,
  label: string,
  name: string,
  value: string,
  handleChange: func,
  handleBlur: func,
  error: bool,
  helperText: string,
}

export default OutlineSelect
