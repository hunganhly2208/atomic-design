import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  placeholder: {
    opacity: 0.5,
  },
}))

export default useStyles
