import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  textField: {
    margin: '8px 0',
  },
  uploadWrap: {
    display: 'flex',
    alignItems: 'center',
    margin: '8px 0 32px',
    justifyContent: 'center',
  },
  imageWrapper: {
    display: 'block',
    position: 'relative',
    height: 100,
    width: 100,
    // border: '1px solid #ccc',
    borderRadius: '50%',
    cursor: 'pointer',
    backgroundColor: theme.palette.grey[100],
    marginRight: theme.spacing(2),
    overFlow: 'hidden',
  },
  img: {
    width: '100%',
    display: 'inline-block',
    borderRadius: '50%',
    height: '100%',
  },
  dropArea: {
    width: '100%',
    height: '100%',
    border: '1px solid #ccc',
    cursor: 'pointer',
    position: 'absolute !important',
    borderRadius: '50%',
    boxShadow: '1px 1px rgba(0,0,0,0.1)',
    top: 0,
    left: 0,
  },
  uploadProfile: {
    cursor: 'pointer',
    textDecoration: 'underline',
  },
  LoadingUpload: {
    position: 'absolute',
    top: 26,
    left: 26,
  },
  // loadingItem: {
  //   marginLeft: 16,
  // },
}))

export default useStyles
