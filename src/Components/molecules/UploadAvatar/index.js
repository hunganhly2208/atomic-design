import React, { useState } from 'react'
import { func, object, string } from 'prop-types'
import { Config } from 'Config'
import Dropzone from 'react-dropzone'
import { CircularProgress, Typography } from '@material-ui/core'
import useStyles from './styles'
import ModalCropImage from '../ModalCropImage'
import { withTranslation } from 'react-i18next'

const UploadAvatar = ({
  isLoadingItems,
  uploadAvatar,
  avatar,
  setFieldValue,
  t,
}) => {
  const [isOpenModalCrop, setOpenModalCrop] = useState(false)
  const handleOpenModalCrop = () => setOpenModalCrop(true)
  const handleCloseModalCrop = () => setOpenModalCrop(false)
  const classes = useStyles()
  let dropzoneRef
  const [file, setFile] = useState(null)
  const handleCropImage = file => {
    setFile(file)
    handleOpenModalCrop()
  }
  return (
    <div className={classes.uploadWrap}>
      <div
        onClick={() => {
          dropzoneRef.open()
        }}
        className={classes.imageWrapper}>
        {avatar && (
          <img
            src={Config.baseImageUrl + avatar}
            alt="logo"
            className={classes.img}
          />
        )}
        <Dropzone
          className={classes.dropArea}
          multiple={false}
          accept="image/*"
          ref={node => {
            dropzoneRef = node
          }}
          onDrop={file => handleCropImage(file[0])}
        />
        {isLoadingItems.get('uploadAvatar') && (
          <CircularProgress size={48} className={classes.LoadingUpload} />
        )}
      </div>
      <div>
        {avatar ? (
          <Typography
            color="primary"
            className={classes.uploadProfile}
            onClick={() => {
              dropzoneRef.open()
            }}>
            {t('user:changeAvatar')}
          </Typography>
        ) : (
          <Typography
            color="primary"
            className={classes.uploadProfile}
            onClick={() => {
              dropzoneRef.open()
            }}>
            {t('user:uploadAvatar')}
          </Typography>
        )}
      </div>
      {isOpenModalCrop && (
        <ModalCropImage
          isOpen={isOpenModalCrop}
          handleClose={handleCloseModalCrop}
          avatar={file}
          uploadAvatar={uploadAvatar}
          setFieldValue={setFieldValue}
        />
      )}
    </div>
  )
}

UploadAvatar.propTypes = {
  isLoadingItems: object.isRequired,
  uploadAvatar: func.isRequired,
  avatar: string,
  setFieldValue: func,
  t: func,
}

export default withTranslation()(UploadAvatar)
