import React from 'react'
import { func } from 'prop-types'

const makeStyles = props => ({
  color: props.color,
  fontFamily: 'Roboto',
  fontSize: props.fontSize,
  textAlign: 'center',
  fontWeight: 300,
  margin: '0 auto',
  marginTop: 25,
  lineHeight: 1.5,
})

const Copyright = props => (
  <div style={makeStyles(props)}>
    {props.t('common:aProductOf')} <br /> HYEC
  </div>
)

Copyright.propTypes = {
  t: func,
}
Copyright.defaultProps = {
  color: '#eee',
  fontSize: '12px',
}

export default Copyright
