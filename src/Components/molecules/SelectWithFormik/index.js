import React from 'react'
import {
  object,
  func,
  array,
  bool,
  string,
  any,
  oneOfType,
  shape,
  node,
  oneOf,
} from 'prop-types'
import { TextField } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import Select from 'react-select'
// import 'react-select/dist/react-select.css'

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />
}

inputComponent.propTypes = {
  inputRef: oneOfType([
    func,
    shape({
      current: any.isRequired,
    }),
  ]),
}

function Control(props) {
  const {
    children,
    innerProps,
    innerRef,
    selectProps: { classes, TextFieldProps },
  } = props

  return (
    <TextField
      fullWidth
      variant="outlined"
      helperText="test"
      error
      InputProps={{
        inputComponent,
        inputProps: {
          className: classes.input,
          ref: innerRef,
          children,
          ...innerProps,
        },
      }}
      InputLabelProps={{
        shrink: true,
      }}
      {...TextFieldProps}
    />
  )
}

Control.propTypes = {
  /**
   * Children to render.
   */
  children: node,
  /**
   * The mouse down event and the innerRef to pass down to the controller element.
   */
  innerProps: shape({
    onMouseDown: func.isRequired,
  }).isRequired,
  innerRef: oneOfType([
    oneOf([null]),
    func,
    shape({
      current: any.isRequired,
    }),
  ]).isRequired,
  selectProps: object.isRequired,
}

const styles = theme => ({
  input: {
    display: 'flex',
    padding: 0,
    height: 'auto',
  },
  formControl: {
    minWidth: 120,
  },
  label: {
    color: '#39393A',
    fontSize: '0.75rem',
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    margin: '0 0 10px 0',
    '& > p': {
      lineheight: 0,
    },
  },
  error: {
    color: 'red',
    fontSize: '0.75rem',
    fontFamily: 'Roboto, Helvetica, Arial, sans-serif',
  },
  chip: {
    marginRight: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  inputError: {
    '& .Select-control': {
      border: `1px solid ${theme.palette.secondary.main}`,
    },
  },
})

const SelectWithFormik = ({
  disabled,
  label,
  error,
  touched,
  name,
  isMulti,
  options,
  onChange,
  onBlur,
  onInputChange,
  value,
  classes,
  required,
  menuPlacement,
  noOptionsMessage,
  menuPosition,
}) => {
  // handle change
  const handleChange = value => {
    onChange(name, value)
  }
  // handle blur
  const handleBlur = () => {
    onBlur(name, true)
  }
  return (
    <Select
      TextFieldProps={{
        label,
        required: required,
        error: !!error,
        helperText: !!error && error,
      }}
      classes={classes}
      id={name}
      clearable={false}
      disabled={disabled}
      components={{
        Control,
      }}
      isMulti={isMulti}
      options={options}
      onChange={handleChange}
      onBlur={handleBlur}
      onInputChange={onInputChange}
      value={value}
      menuPlacement={menuPlacement}
      noOptionsMessage={noOptionsMessage}
      menuPosition={menuPosition}
      styles={{
        container: root => ({
          ...root,
          margin: '12px 0',
        }),
        menu: root => ({
          ...root,
          margin: 0,
        }),
        input: root => ({
          ...root,
          padding: '12px 8px',
        }),
      }}
    />
  )
}

SelectWithFormik.propTypes = {
  classes: object.isRequired,
  onChange: func.isRequired,
  onBlur: func,
  onInputChange: func,
  isMulti: bool,
  disabled: bool,
  options: array,
  name: string.isRequired,
  label: string,
  error: string,
  touched: bool,
  value: any,
  required: bool,
  menuPlacement: string,
  noOptionsMessage: func,
  menuPosition: string,
}

export default withStyles(styles)(SelectWithFormik)
