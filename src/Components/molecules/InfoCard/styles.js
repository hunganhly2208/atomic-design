import React from 'react'
import { styled, makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  infoCardWrap: {
    padding: `15px 15px 50px 15px`,
    minHeight: `350px`,
  },
  avatarWrap: {
    textAlign: props => props.avatarAlignment,
    '& img': {
      backgroundColor: '#ccc',
      '@media (min-width: 320px)': {
        width: `5rem,`,
        height: `5rem`,
        objectFit: `cover`,
        borderRadius: `50%`,
        border: `1px solid #fff`,
        padding: 2,
        marginTop: 0,
        boxShadow: `0px 0px 4px 1px #e5e5e5`,
        transition: `0.5s`,
      },
      '@media (min-width: 992px)': {
        width: `6rem`,
        height: `6rem`,
        marginTop: `1rem`,
      },
    },
  },
  infoWrap: {
    marginTop: `0.5rem`,
    textAlign: props => props.contentAlignment,
  },
  infoBottom: {
    display: 'flex',
    justifyContent: `space-around`,
  },
  staffId: {
    fontSize: 15,
    color: `#0093cd`,
  },
  staffName: {
    color: `#0093cd`,
    fontWeight: 700,
    fontSize: 18,
    whiteSpace: `nowrap`,
    overflow: `hidden`,
    textOverflow: `ellipsis`,
  },
  jobTitle: {
    color: `rgba(0, 0, 0, 1)`,
    fontSize: `1rem`,
    fontFamily: `serif`,
    fontStyle: `italic`,
    padding: `5px 0`,
    whiteSpace: `normal`,
  },
  department: {
    color: `rgba(255, 255, 255, 1)`,
    fontSize: `0.9rem`,
    marginTop: 5,
    whiteSpace: `nowrap`,
    overflow: `hidden`,
    textOverflow: `ellipsis`,
    textTransform: `uppercase`,
    fontWeight: 700,
  },
  email: {
    fontSize: 15,
    marginTop: 5,
    whiteSpace: `nowrap`,
    overflow: `hidden`,
    textOverflow: `ellipsis`,
    '& a': {
      color: theme.palette.primary.main,
      textDecoration: `none`,
    },
  },
  infoBottomContent: {
    textAlign: `center`,
    marginTop: 15,
  },
  number: {
    fontSize: 20,
    fontWeight: 700,
    color: `#888`,
    '& a': {
      color: theme.palette.primary.main,
      textDecoration: `none`,
    },
  },
  label: {
    fontSize: 14,
    color: `rgba(0, 0, 0, 0.3)`,
  },
  companyAbbr: {
    position: `absolute`,
    top: 15,
    right: 30,
    fontWeight: 700,
    color: theme.palette.primary.main,
  },
  extraInfoWrap: {
    marginTop: `1em`,
  },
  extraInfoGroup: {
    display: `flex`,
    justifyContent: `space-between`,
    margin: `8px 0`,
  },
  extraInfoItem: {
    display: `flex`,
    justifyContent: `flext-start`,
    alignItems: `center`,
    margin: `8px 0`,
  },
  extraInfoGroupItem: {
    display: `flex`,
    justifyContent: `flext-start`,
    alignItems: `center`,
  },
  infoLabel: {
    fontSize: `0.9em`,
    color: `#686868`,
    margin: `0 5px 0 5px`,
  },
  infoContent: {
    fontSize: `0.9em`,
    color: `#000`,
  },
  linkContent: {
    fontSize: `0.9em`,
    color: `#000`,
    textOverflow: `ellipsis`,
    display: `inline-block`,
    overflow: `hidden`,
  },
}))

export default useStyles
