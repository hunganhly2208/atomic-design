import React from 'react'
import PropTypes from 'prop-types'
import user from 'Assets/images/male.png'
import useStyles from './styles'

const InfoCard = ({
  avatar = user,
  avatarAlignment = 'center',
  contentAlignment = 'center',
  staffName,
  staffId,
  homePhone,
  jobTitle,
  email,
  phone,
  ext,
  companyAbbr,
}) => {
  const classes = useStyles({ avatarAlignment, contentAlignment })
  return (
    <div className={classes.infoCardWrap}>
      <div className={classes.avatarWrap}>
        <img
          src={avatar}
          alt="Avatar"
          onError={e => {
            e.target.src = user
          }}
        />
      </div>
      <div className={classes.infoWrap}>
        <div className={classes.staffName} title={staffName}>
          {staffName}
        </div>
        <div className={classes.jobTitle} title={jobTitle}>
          {jobTitle}
        </div>
        <div className={classes.staffId}>{staffId}</div>
      </div>
      <div className={classes.extraInfoWrap}>
        <div className={classes.extraInfoItem}>
          <label className={classes.infoLabel}>E:</label>
          <a
            className={classes.linkContent}
            href={`mailto:${email}`}
            title={email}>
            {email || 'unknown'}
          </a>
        </div>
        <div className={classes.extraInfoItem}>
          <label className={classes.infoLabel}>MB:</label>
          <a className={classes.linkContent} href={`tel:${phone}`}>
            {phone || 'unknown'}
          </a>
        </div>
        <div className={classes.extraInfoGroup}>
          <div className={classes.extraInfoGroupItem}>
            <label className={classes.infoLabel}>Phone:</label>
            <a className={classes.linkContent}>{homePhone || '--'}</a>
          </div>
          <div className={classes.extraInfoGroupItem}>
            <label className={classes.infoLabel}>Ext:</label>
            <a className={classes.linkContent}>{ext || '--'}</a>
          </div>
        </div>
      </div>
      {companyAbbr && (
        <div className={classes.companyAbbr} style={{}}>
          {companyAbbr}
        </div>
      )}
    </div>
  )
}

InfoCard.propTypes = {
  avatar: PropTypes.node,
  staffId: PropTypes.string,
  staffName: PropTypes.string,
  jobTitle: PropTypes.string,
  avatarAlignment: PropTypes.oneOf(['left', 'right', 'center']),
  contentAlignment: PropTypes.oneOf(['left', 'right', 'center']),
  email: PropTypes.string,
  ext: PropTypes.string,
  phone: PropTypes.string,
  homePhone: PropTypes.string,
  companyAbbr: PropTypes.string,
}

export default InfoCard
