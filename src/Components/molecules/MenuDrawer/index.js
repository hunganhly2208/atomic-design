import React from 'react'
import { ListItem, ListItemText } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { node, func, string, object, bool } from 'prop-types'
import styles from './styles'
import RotatedSquareIcon from '../../atoms/RotatedSquareIcon'

const MenuDrawer = ({ classes, isButton, handleClick, href, label }) => (
  <ListItem
    onClick={handleClick}
    className={classes.linkItem}
    button={isButton}
    href={href}>
    <RotatedSquareIcon />
    <ListItemText>{label}</ListItemText>
  </ListItem>
)

MenuDrawer.propTypes = {
  classes: object,
  // icon: node,
  handleClick: func,
  // path: string,
  label: string,
  isButton: bool,
  href: string,
}

export default withStyles(styles)(MenuDrawer)
