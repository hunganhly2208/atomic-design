import React from 'react'
import { bool, string, func } from 'prop-types'
import { Snackbar, IconButton } from '@material-ui/core'
import Close from '@material-ui/icons/Close'

const VersionNotification = ({ isOpen, message, onClose }) => {
  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      open={isOpen}
      onClose={onClose}
      message={message}
      action={
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          onClick={onClose}>
          <Close />
        </IconButton>
      }
    />
  )
}

VersionNotification.propTypes = {
  isOpen: bool,
  message: string.isRequired,
  onClose: func.isRequired,
}

export default VersionNotification
