import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles(theme => ({
  InfoCardWrap: {
    padding: 15,
  },
  AvatarWrap: {
    textAlign: props => props.avatarAlignment,
    ' & img': {
      width: 96,
      height: 96,
      objectFit: 'cover',
      borderRadius: '50%',
    },
  },
  InfoWrap: {
    marginTop: 15,
    textAlign: props => props.contentAlignment,
  },
  InfoBottom: {
    display: 'flex',
    justifyContent: `space-around`,
  },
  StaffID: {
    fontSize: `1rem`,
    color: `rgba(255, 255, 255, 1)`,
    marginTop: 15,
    padding: `2% 0`,
  },
  StaffName: {
    color: `rgba(255, 255, 255, 1)`,
    fontWeight: 700,
    fontSize: `1.3rem`,
    whiteSpace: `nowrap`,
    overflow: 'hidden',
    textOerflow: `ellipsis`,
  },
  JobTitle: {
    color: `rgba(255, 255, 255, 1)`,
    fontSize: `1rem`,
    fontFamily: `serif`,
    fontStyle: `italic`,
    padding: `5px 0`,
    whiteSpace: `nowrap`,
    overflow: `hidden`,
    textOverflow: `ellipsis`,
  },
  Department: {
    color: `rgba(255, 255, 255, 1)`,
    fontSize: `0.9rem`,
    marginTop: 5,
    whiteSpace: `nowrap`,
    overflow: `hidden`,
    textOverflow: `ellipsis`,
    textTransform: `uppercase`,
    fontWeight: `bolder`,
  },
  Email: {
    fontSize: `0.9rem`,
    marginTop: 5,
    whiteSpace: `nowrap`,
    overflow: `hidden`,
    textOverflow: `ellipsis`,
    '& a': {
      color: theme.palette.primary.main,
      textDecoration: `none`,
    },
  },
  InfoBottomContent: {
    textAlign: `center`,
    marginTop: 15,
  },
  Number: {
    fontSize: 20,
    fontWeight: 700,
    color: `#fff`,
  },
  CompanyAbbr: {
    position: `absolute`,
    top: 15,
    right: 30,
    fontWeight: `bold`,
    color: `#fff`,
  },
}))

export default useStyles
