import React from 'react'
import PropTypes from 'prop-types'
import user from 'Assets/images/male.png'
import useStyles from './styles'

const InfoCardProfile = ({
  avatar = user,
  avatarAlignment = 'center',
  contentAlignment = 'center',
  staffName,
  staffId,
  jobTitle,
  email,
  department,
  phone,
  ext,
  companyAbbr,
}) => {
  const classes = useStyles({ avatarAlignment, contentAlignment })
  return (
    <div className={classes.InfoCardWrap}>
      <div className={classes.AvatarWrap}>
        <img src={avatar} alt="Avatar" />
      </div>
      <div className={classes.InfoWrap}>
        <div className={classes.StaffID}>{staffId}</div>
        <div className={classes.StaffName} title={staffName}>
          {staffName}
        </div>
        <div className={classes.JobTitle} title={jobTitle}>
          {jobTitle}
        </div>
        <div className={classes.Email} title={email}>
          {email}
        </div>
        <div className={classes.Department} title={department}>
          {department}
        </div>
      </div>

      <div className={classes.InfoBottom}>
        {ext && (
          <div className={classes.InfoBottomContent}>
            <div className={classes.Number}>{ext}</div>
            <div>Ext</div>
          </div>
        )}
        {phone && (
          <div className={classes.InfoBottomContent}>
            <div className={classes.Number}>{phone}</div>
            <div>Mobile</div>
          </div>
        )}
      </div>

      {companyAbbr && <div className={classes.CompanyAbbr}>{companyAbbr}</div>}
    </div>
  )
}

InfoCardProfile.propTypes = {
  avatar: PropTypes.node,
  staffId: PropTypes.string,
  staffName: PropTypes.string,
  jobTitle: PropTypes.string,
  avatarAlignment: PropTypes.oneOf(['left', 'right', 'center']),
  contentAlignment: PropTypes.oneOf(['left', 'right', 'center']),
  email: PropTypes.string,
  ext: PropTypes.string,
  phone: PropTypes.string,
  department: PropTypes.string,
  companyAbbr: PropTypes.string,
}

export default InfoCardProfile
