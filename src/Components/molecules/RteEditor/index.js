import React from 'react'
import { func, string } from 'prop-types'
import ReactQuill from 'react-quill'
import './styles.css'
import 'react-quill/dist/quill.snow.css'

const RteEditor = ({ value, handleChange }) => {
  return <ReactQuill value={value} onChange={handleChange} />
}

RteEditor.propTypes = {
  value: string,
  handleChange: func.isRequired,
}

export default RteEditor
