const styles = theme => ({
  linkItem: {
    paddingLeft: 50,
    color: theme.palette.common.black,
  },
  linkActive: {
    color: theme.palette.common.black,
    background: '#ccc',
  },
  itemIcon: {
    color: theme.palette.common.white,
  },
})

export default styles
