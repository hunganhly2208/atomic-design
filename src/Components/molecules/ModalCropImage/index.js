import React, { useRef, useState } from 'react'
import { object, func, bool } from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import {
  Dialog,
  DialogContent,
  Button,
  DialogActions,
  Slide,
} from '@material-ui/core'

import AvatarEditor from 'react-avatar-editor'

const Transition = React.forwardRef((props, ref) => {
  return <Slide direction="up" {...props} ref={ref} />
})

const ModalCropImage = ({
  classes,
  handleClose,
  avatar,
  isOpen,
  uploadAvatar,
  setFieldValue,
}) => {
  const cropperRef = useRef(null)
  const [scale, setScale] = useState(1)
  // Change scale
  const handleChangeSclae = e => setScale(+e.target.value)
  const handleSaveCrop = () => {
    if (cropperRef.current) {
      // const canvas = cropperRef.current.getImage()
      const canvasScaled = cropperRef.current.getImageScaledToCanvas()
      canvasScaled.toBlob(blob => {
        let fileUpload = new File([blob], avatar.name)
        setFieldValue('avatar', '')
        uploadAvatar([fileUpload], 'Users', setFieldValue)
      })
    }
    handleClose()
  }
  return (
    <Dialog
      onClose={handleClose}
      open={isOpen}
      TransitionComponent={Transition}>
      <DialogContent className={classes.dialogContent}>
        <div className={classes.editorWrap}>
          <AvatarEditor
            image={avatar}
            width={200}
            height={200}
            borderRadius={100}
            scale={scale}
            color={[255, 255, 255, 0.8]}
            rotate={0}
            ref={cropperRef}
          />
        </div>
        <div className={classes.sliderWrapper}>
          <input
            type="range"
            min="1"
            max="5"
            value={scale}
            onChange={handleChangeSclae}
          />
        </div>
      </DialogContent>
      <DialogActions style={{ justifyContent: 'center' }}>
        <Button
          variant="contained"
          type="submit"
          color="primary"
          fullWidth
          onClick={handleSaveCrop}>
          Save
        </Button>
        <Button onClick={handleClose} fullWidth>
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  )
}

ModalCropImage.propTypes = {
  classes: object.isRequired,
  handleClose: func.isRequired,
  avatar: object.isRequired,
  isOpen: bool.isRequired,
  uploadAvatar: func.isRequired,
  setFieldValue: func.isRequired,
}

export default withStyles(styles)(ModalCropImage)
