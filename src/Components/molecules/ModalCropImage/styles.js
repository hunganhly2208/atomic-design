const styles = theme => ({
  dialogContent: {
    textAlign: 'center',
  },
  sliderWrapper: {
    marginTop: 8,
    '& input': {
      width: '100%',
    },
  },
})

export default styles
