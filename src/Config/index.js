export const Config = {
  QA_URL: 'https://hyec.dsquare.vn',
  PRO_URL: 'https://hyec.dsquare.vn',
  version: '1.0.0',
  baseUrl: process.env.PUBLIC_URL + '/v2',
  baseImageUrl:
    'https://s3-ap-southeast-1.amazonaws.com/cdn.haiyeneyecare.com/',
  homePage: 'https://hyec.dsquare.vn',
}
