import { MODULE_NAME } from './InitialState'
import { createSelector } from 'reselect'

const getItems = state => state[MODULE_NAME].get('items')

const getLeaders = state => state[MODULE_NAME].get('leaders')

const getFormAssignItem = createSelector(
  getLeaders,
  leaders => {
    let leaderValues = leaders.map(leader => ({
      value: leader.get('id'),
      label: leader.get('displayName'),
    }))
    return {
      leaders: leaderValues.toJS(),
    }
  }
)

export const OfficeSelectors = {
  getItems,
  getFormAssignItem,
}
