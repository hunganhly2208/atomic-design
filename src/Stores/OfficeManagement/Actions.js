import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List Office actions
    getItemsRequest: null,
    getItemsSuccess: ['items'],
    getItemsFailure: null,
    clearItems: null,
    // Edit Office actions
    editItemRequest: ['values'],
    editItemSuccess: ['item'],
    editItemFailure: null,
    // Create Office actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Delete Office actions
    getLeadersRequest: ['officeId'],
    getLeadersSuccess: ['items'],
    getLeaderFailure: null,
    clearLeaders: null,
    // Assign leaders
    assignLeadersRequest: ['officeId', 'values'],
    assignLeadersSuccess: null,
    assignLeadersFailure: null,
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const OfficeTypes = Types
export const OfficeActions = Creators
