import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { OfficeActions, OfficeTypes } from './Actions'
import { OfficeSelectors } from './Selectors'
import { NotificationActions } from 'Stores/Notification/Actions'
import { ModalActions } from '../Modal/Actions'
import { OfficeService } from '../../Services/OfficeService'
import { UserService } from 'Services/UserService'
import { LoadingActions } from '../Loading/Actions'
import sagaRegistry from '../Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { getValueFromSelect } from 'Utils/getValueFromSelect'
import { checkResponseError } from 'Utils/checkResponseError'

function* getListOfficeWorker() {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(OfficeService.getListOffice)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: OfficeTypes.GET_ITEMS_SUCCESS,
      items: response.items,
    })
  } catch (error) {
    yield put({
      type: OfficeTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

// Create Product worker
function* createOfficeWorker({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(OfficeService.createOffice, values)
    checkResponseError(response)
    yield put({
      type: OfficeTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(NotificationActions.showNotification('Create User Success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(OfficeActions.getItemsRequest())
  } catch (error) {
    yield put({
      type: OfficeTypes.CREATE_ITEM_FAILURE,
    })
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}
// Edit Product worker
function* editOfficeWorker({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const { id, ...data } = values
    const response = yield call(OfficeService.editOffice, id, data)
    checkResponseError(response)
    yield put({
      type: OfficeTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(NotificationActions.showNotification('Edit User success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(OfficeActions.getItemsRequest())
  } catch (error) {
    yield put({
      type: OfficeTypes.EDIT_ITEM_FAILURE,
    })
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

// Get Leader Office
function* getLeadersWorker({ officeId }) {
  try {
    const response = yield call(UserService.getLeadersOffice, officeId)
    checkResponseError(response)
    yield put(OfficeActions.getLeadersSuccess(response.items))
  } catch (error) {
    yield put(OfficeActions.getLeadersFailure())
    yield put(NotificationActions.showNotification(error.message))
  }
}

// Assign Leaders office
function* assignLeadersWorker({ officeId, values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const users = values.leaders ? values.leaders.map(getValueFromSelect) : []
    let data = { items: JSON.stringify(users) }
    const response = yield call(
      OfficeService.assignLeadersOffice,
      officeId,
      data
    )
    checkResponseError(response)
    yield put(OfficeActions.assignLeadersSuccess())
    yield put(NotificationActions.showNotification('Assign Leaders success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(OfficeActions.getItemsRequest())
  } catch (error) {
    yield put(OfficeActions.assignLeadersFailure())
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(OfficeTypes.GET_ITEMS_REQUEST, getListOfficeWorker),
    takeLatest(OfficeTypes.CREATE_ITEM_REQUEST, createOfficeWorker),
    takeLatest(OfficeTypes.EDIT_ITEM_REQUEST, editOfficeWorker),
    takeLatest(OfficeTypes.GET_LEADERS_REQUEST, getLeadersWorker),
    takeLatest(OfficeTypes.ASSIGN_LEADERS_REQUEST, assignLeadersWorker),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
