import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { OfficeTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state =>
  state.merge(
    fromJS({
      items: [],
    })
  )

const setItems = (state, { items }) =>
  state.merge(
    fromJS({
      items,
    })
  )
// Set and clear leaders
const setLeaders = (state, { items }) => state.set('leaders', fromJS(items))
const clearLeaders = state => state.set('leaders', fromJS([]))

const reducer = createReducer(INITIAL_STATE, {
  // List items action handler
  [OfficeTypes.GET_ITEMS_SUCCESS]: setItems,
  [OfficeTypes.CLEAR_ITEMS]: clearItems,
  // Set and clear leaders
  [OfficeTypes.GET_LEADERS_SUCCESS]: setLeaders,
  [OfficeTypes.CLEAR_LEADERS]: clearLeaders,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
