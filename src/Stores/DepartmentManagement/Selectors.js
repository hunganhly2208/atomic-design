import { MODULE_NAME } from './InitialState'
import { createSelector } from 'reselect'

const getItems = state => state[MODULE_NAME].get('items')

const getLeaders = state => state[MODULE_NAME].get('leaders')

const getFilter = state => state[MODULE_NAME].get('filter')

const getShowMore = state => state[MODULE_NAME].get('isShowMore')

const getFormAssignItem = createSelector(
  getLeaders,
  leaders => {
    let leaderValues = leaders.map(leader => ({
      value: leader.get('id'),
      label: leader.get('displayName'),
    }))
    return {
      leaders: leaderValues.toJS(),
    }
  }
)

export const DepartmentSelectors = {
  getItems,
  getFormAssignItem,
  getFilter,
  getShowMore,
}
