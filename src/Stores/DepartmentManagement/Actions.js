import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List Office actions
    getItemsRequest: ['filter'],
    getItemsSuccess: ['items', 'totalCount'],
    getItemsFailure: null,
    clearItems: null,
    // Edit Office actions
    editItemRequest: ['values'],
    editItemSuccess: null,
    editItemFailure: null,
    // Create Office actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Delete Office actions
    deleteItemRequest: ['id'],
    deleteItemSuccess: null,
    deleteItemFailure: null,
    // Get Leaders actions
    getLeadersRequest: ['departmentId'],
    getLeadersSuccess: ['items'],
    getLeaderFailure: null,
    clearLeaders: null,
    // Assign leaders
    assignLeadersRequest: ['departmentId', 'values'],
    assignLeadersSuccess: null,
    assignLeadersFailure: null,
    // Set filter
    setFilter: ['filter'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const DepartmentTypes = Types
export const DepartmentActions = Creators
