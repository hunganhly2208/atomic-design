import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { DepartmentActions, DepartmentTypes } from './Actions'
import { DepartmentSelectors } from './Selectors'
import { NotificationActions } from 'Stores/Notification/Actions'
import { ModalActions } from '../Modal/Actions'
import { DepartmentService } from '../../Services/DepartmentService'
import { UserService } from 'Services/UserService'
import { LoadingActions } from '../Loading/Actions'
import sagaRegistry from '../Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { getValueFromSelect } from 'Utils/getValueFromSelect'

function* getListDepartmentWorker() {
  try {
    yield put(LoadingActions.showLoadingList())
    const filter = yield select(DepartmentSelectors.getFilter)
    const response = yield call(
      DepartmentService.getListDepartment,
      filter.toJS()
    )
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: DepartmentTypes.GET_ITEMS_SUCCESS,
      items: response.items,
      totalCount: response.totalCount,
    })
  } catch (error) {
    yield put({
      type: DepartmentTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

// Create Product worker
function* createDepartmentWorker({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(DepartmentService.createDepartment, values)
    checkResponseError(response)
    yield put({
      type: DepartmentTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(NotificationActions.showNotification('Create Department Success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(DepartmentActions.getItemsRequest())
  } catch (error) {
    yield put({
      type: DepartmentTypes.CREATE_ITEM_FAILURE,
    })
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}
// Edit Department worker
function* editDepartmentWorker({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const { id, ...data } = values
    const response = yield call(DepartmentService.editDepartment, id, data)
    checkResponseError(response)
    yield put({
      type: DepartmentTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(NotificationActions.showNotification('Edit Department success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(DepartmentActions.getItemsRequest())
  } catch (error) {
    yield put({
      type: DepartmentTypes.EDIT_ITEM_FAILURE,
    })
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

// Get Leader Department
function* getLeadersWorker({ departmentId }) {
  try {
    const response = yield call(UserService.getLeadersDepartment, departmentId)
    checkResponseError(response)
    yield put(DepartmentActions.getLeadersSuccess(response.items))
  } catch (error) {
    yield put(DepartmentActions.getLeadersFailure())
    yield put(NotificationActions.showNotification(error.message))
  }
}

// Assign Leaders office
function* assignLeadersWorker({ departmentId, values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const users = values.leaders ? values.leaders.map(getValueFromSelect) : []
    let data = { items: JSON.stringify(users) }
    const response = yield call(
      DepartmentService.assignLeadersDepartment,
      departmentId,
      data
    )
    checkResponseError(response)
    yield put(DepartmentActions.assignLeadersSuccess())
    yield put(NotificationActions.showNotification('Assign Leaders success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(DepartmentActions.getItemsRequest())
  } catch (error) {
    yield put(DepartmentActions.assignLeadersFailure())
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(DepartmentTypes.GET_ITEMS_REQUEST, getListDepartmentWorker),
    takeLatest(DepartmentTypes.CREATE_ITEM_REQUEST, createDepartmentWorker),
    takeLatest(DepartmentTypes.EDIT_ITEM_REQUEST, editDepartmentWorker),
    takeLatest(DepartmentTypes.GET_LEADERS_REQUEST, getLeadersWorker),
    takeLatest(DepartmentTypes.ASSIGN_LEADERS_REQUEST, assignLeadersWorker),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
