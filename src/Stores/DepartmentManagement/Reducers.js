import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { DepartmentTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state =>
  state.merge(
    fromJS({
      items: [],
    })
  )

const setItems = (state, { items, totalCount }) => {
  let oldItems = state.get('items')
  let newItems = oldItems.concat(fromJS(items))
  let isLoadMore = totalCount > newItems.size
  return state.set('items', newItems).set('isShowMore', isLoadMore)
}
// Set and clear leaders
const setLeaders = (state, { items }) => state.set('leaders', fromJS(items))
const clearLeaders = state => state.set('leaders', fromJS([]))

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INITIAL_STATE, {
  // List items action handler
  [DepartmentTypes.GET_ITEMS_SUCCESS]: setItems,
  [DepartmentTypes.CLEAR_ITEMS]: clearItems,
  // Set and clear leaders
  [DepartmentTypes.GET_LEADERS_SUCCESS]: setLeaders,
  [DepartmentTypes.CLEAR_LEADERS]: clearLeaders,
  // Filter
  [DepartmentTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
