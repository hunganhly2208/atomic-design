import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'OfficeManagement'

export const INITIAL_STATE = fromJS({
  items: [],
  item: {},
  leaders: [],
  filter: {
    page: 1,
    limit: 10,
  },
  isShowMore: false,
})
