import { combineReducers } from 'redux'
import * as auth from '../Authentication/InitialState'
import * as global from '../Global/InitialState'
import * as modal from '../Modal/InitialState'

import storage from 'redux-persist/lib/storage'
import immutableTransform from 'redux-persist-transform-immutable'
import { persistReducer } from 'redux-persist'

const persistConfig = {
  transforms: [immutableTransform()],
  key: 'root',
  keyPrefix: 'HaiYen-v2', // Key prefix when save to local storage, need declare to avoid conflict between projects
  storage: storage,
  /**
   * White list State . These reducer will be persisted by redux-persist
   */
  whitelist: ['authentication', 'global', 'modal'],
}

// Persist reducer need to declare here
export const initialState = {
  [auth.MODULE_NAME]: auth.INITIAL_STATE,
  [global.MODULE_NAME]: global.INITIAL_STATE,
  [modal.MODULE_NAME]: modal.INITIAL_STATE,
}
export const combine = reducers => {
  const reducerNames = Object.keys(reducers)
  Object.keys(initialState).forEach(item => {
    if (reducerNames.indexOf(item) === -1) {
      reducers[item] = (state = null) => state
    }
  })
  return persistReducer(persistConfig, combineReducers(reducers))
}
