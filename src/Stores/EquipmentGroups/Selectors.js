import { MODULE_NAME } from './InitialState'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

const getTotalPages = state => state[MODULE_NAME].get('totalPages')

export const EquipmentGroupsSelectors = {
  getFilter,
  getItems,
  getTotalCount,
  getTotalPages,
}
