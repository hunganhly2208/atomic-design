import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { EquipmentGroupsTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state =>
  state.merge(
    fromJS({
      items: [],
      totalCount: 0,
      totalPage: 0,
    })
  )

const setItems = (state, { items, totalCount, }) =>
  state.merge(
    fromJS({
      items,
      totalCount,
    })
  )

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INITIAL_STATE, {
  // List items action handler
  [EquipmentGroupsTypes.GET_ITEMS_SUCCESS]: setItems,
  [EquipmentGroupsTypes.CLEAR_ITEMS]: clearItems,
  // Set filters handler
  [EquipmentGroupsTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
