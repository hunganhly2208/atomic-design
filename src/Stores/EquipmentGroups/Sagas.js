import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { EquipmentGroupsActions, EquipmentGroupsTypes } from './Actions'
import { EquipmentGroupsSelectors } from './Selectors'
import { NotificationActions } from 'Stores/Notification/Actions'
import { ModalActions } from '../Modal/Actions'
import { EquipmentGroupsService } from '../../Services/EquipmentGroupsService'
import { UserService } from 'Services/UserService'
import { LoadingActions } from '../Loading/Actions'
import sagaRegistry from '../Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { getValueFromSelect } from 'Utils/getValueFromSelect'
import { checkResponseError } from 'Utils/checkResponseError'

function* getListEquipmentGroupsWorker() {
  try {
    const filter = yield select(EquipmentGroupsSelectors.getFilter)
    yield put(LoadingActions.showLoadingList())
    const response = yield call(EquipmentGroupsService.getListEquipmentGroups, filter.toJS())
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: EquipmentGroupsTypes.GET_ITEMS_SUCCESS,
      items: response.items,
      totalCount: response.totalCount,
    })
  } catch (error) {
    yield put({
      type: EquipmentGroupsTypes.GET_ITEMS_FAILURE,
    })
    yield put(NotificationActions.showNotification('notify:fail'))
    yield put(LoadingActions.hideLoadingList())
  }
}

function* getEquipmentGroupsDetailWorker(id) {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(EquipmentGroupsService.getEquipmentGroupsById, id)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: EquipmentGroupsTypes.GET_ITEM_DETAIL_SUCCESS,
      items: response.items,
    })
  } catch (error) {
    yield put({
      type: EquipmentGroupsTypes.GET_ITEM_DETAIL_FAILURE,
    })
    yield put(NotificationActions.showNotification('notify:fail'))
    yield put(LoadingActions.hideLoadingList())
  }
}

// Create Equipment Groups worker
function* createEquipmentGroupsWorker({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(EquipmentGroupsService.createEquipmentGroups, values)
    checkResponseError(response)
    yield put({
      type: EquipmentGroupsTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(NotificationActions.showNotification('notify:success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(EquipmentGroupsActions.getItemsRequest())
  } catch (error) {
    yield put({
      type: EquipmentGroupsTypes.CREATE_ITEM_FAILURE,
    })
    yield put(NotificationActions.showNotification('notify:fail'))
    yield put(LoadingActions.hideLoadingAction())
  }
}

// Edit Equipment Group worker
function* editEquipmentGroupsWorker({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const { id, ...data } = values
    const response = yield call(EquipmentGroupsService.editEquipmentGroups, id, data)
    checkResponseError(response)
    yield put({
      type: EquipmentGroupsTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(NotificationActions.showNotification('notify:success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(EquipmentGroupsActions.getItemsRequest())
  } catch (error) {
    yield put({
      type: EquipmentGroupsTypes.EDIT_ITEM_FAILURE,
    })
    yield put(NotificationActions.showNotification('notify:fail'))
    yield put(LoadingActions.hideLoadingAction())
  }
}

// Delete Equipment Group worker
function* deleteEquipmentGroupWorker({ id }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(EquipmentGroupsService.deleteEquipmentGroups, id)
    checkResponseError(respones)
    yield put({
      type: EquipmentGroupsTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(EquipmentGroupsActions.getItemsRequest())
    yield put(NotificationActions.showNotification('notify:success'))
  } catch (error) {
    yield put({
      type: EquipmentGroupsTypes.DELETE_ITEM_FAILURE,
    })
    yield put(NotificationActions.showNotification('notify:fail'))
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(EquipmentGroupsTypes.GET_ITEMS_REQUEST, getListEquipmentGroupsWorker),
    takeLatest(EquipmentGroupsTypes.GET_ITEM_DETAIL_REQUEST, getEquipmentGroupsDetailWorker),
    takeLatest(EquipmentGroupsTypes.CREATE_ITEM_REQUEST, createEquipmentGroupsWorker),
    takeLatest(EquipmentGroupsTypes.EDIT_ITEM_REQUEST, editEquipmentGroupsWorker),
    takeLatest(EquipmentGroupsTypes.DELETE_ITEM_REQUEST, deleteEquipmentGroupWorker),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
