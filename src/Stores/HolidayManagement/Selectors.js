import { MODULE_NAME } from './InitialState'
import { createSelector } from 'reselect'

const getItems = state => state[MODULE_NAME].get('items')
const getFilter = state => state[MODULE_NAME].get('filter')

export const HolidaySelectors = {
  getItems,
  getFilter,
}
