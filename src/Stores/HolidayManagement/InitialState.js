import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'HolidayManagement'

export const INITIAL_STATE = fromJS({
  items: [],
  filter: {
    year: new Date().getFullYear(),
  },
})
