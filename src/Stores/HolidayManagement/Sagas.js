import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { HolidayActions, HolidayTypes } from './Actions'
import { HolidaySelectors } from './Selectors'
import { NotificationActions } from 'Stores/Notification/Actions'
import { ModalActions } from '../Modal/Actions'
import { HolidayService } from 'Services/HolidayService'
import { LoadingActions } from '../Loading/Actions'
import sagaRegistry from '../Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import moment from 'moment'

function* getListHolidayWorker() {
  try {
    yield put(LoadingActions.showLoadingList())
    const filter = yield select(HolidaySelectors.getFilter)
    let params = {
      fromDate: `${filter.get('year')}-01-01`,
      toDate: `${filter.get('year')}-12-31`,
    }
    const response = yield call(HolidayService.getListHoliday, params)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: HolidayTypes.GET_ITEMS_SUCCESS,
      items: response.items,
    })
  } catch (error) {
    yield put({
      type: HolidayTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

// Create Holiday worker
function* createHolidayWorker({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const data = {
      name: values.name,
      date: moment(values.date).format('YYYY-MM-DD'),
    }
    const response = yield call(HolidayService.createHoliday, data)
    checkResponseError(response)
    yield put({
      type: HolidayTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(NotificationActions.showNotification('Create Holiday Success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(HolidayActions.getItemsRequest())
  } catch (error) {
    yield put({
      type: HolidayTypes.CREATE_ITEM_FAILURE,
    })
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

// Delete Holiday worker
function* deleteHolidayWorker({ id }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(HolidayService.deleteHoliday, id)
    checkResponseError(response)
    yield put(HolidayActions.deleteItemSuccess())
    yield put(NotificationActions.showNotification('Delete Holiday Success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(HolidayActions.getItemsRequest())
  } catch (error) {
    yield put(HolidayActions.deleteItemFailure())
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(HolidayTypes.GET_ITEMS_REQUEST, getListHolidayWorker),
    takeLatest(HolidayTypes.CREATE_ITEM_REQUEST, createHolidayWorker),
    takeLatest(HolidayTypes.DELETE_ITEM_REQUEST, deleteHolidayWorker),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
