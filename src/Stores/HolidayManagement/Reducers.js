import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { HolidayTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state => state.set('items', fromJS([]))

const setItems = (state, { items }) => state.set('items', fromJS(items))

// Set filter
const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INITIAL_STATE, {
  // List items action handler
  [HolidayTypes.GET_ITEMS_SUCCESS]: setItems,
  [HolidayTypes.CLEAR_ITEMS]: clearItems,
  // Set filter
  [HolidayTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
