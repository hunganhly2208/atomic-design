import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get user info
    getProfileInfoRequest: ['userId'],
    getProfileInfoSuccess: ['item', 'formItem'],
    getProfileInfoFailure: null,
    clearProfileInfo: null,
    // Edit Profile
    editProfileRequest: ['userId', 'values'],
    editProfileSuccess: null,
    editProfileFailure: null,
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const ProfileTypes = Types
export const ProfileActions = Creators
