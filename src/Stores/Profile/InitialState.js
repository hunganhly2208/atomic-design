import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'userProfile'

export const INITIAL_STATE = fromJS({
  profileInfo: {},
  formItem: {
    phone: '',
    taxCode: '',
    socialInsuranceNumber: '',
    placeOfBirth: '',
    identityCard: '',
    issueDate: null,
    issueAt: '',
    residentialAddress: '',
    address: '',
    avatar: '',
    birthday: null,
  },
})
