import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { ProfileActions, ProfileTypes } from './Actions'
import {} from './Selectors'
import { NotificationActions } from 'Stores/Notification/Actions'
import { ModalActions } from '../Modal/Actions'
import { UserService } from 'Services/UserService'
import { LoadingActions } from '../Loading/Actions'
import sagaRegistry from '../Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { AuthActions } from 'Stores/Authentication/Actions'
import { checkResponseError } from 'Utils/checkResponseError'
import moment from 'moment'

function* getProfileInfoWorker({ userId }) {
  try {
    const response = yield call(UserService.getProfileInfo, userId)
    checkResponseError(response)
    const item = response.item
    const formItem = {
      phone: item.phone || '',
      taxCode: item.taxCode || '',
      socialInsuranceNumber: item.socialInsuranceNumber || '',
      placeOfBirth: item.placeOfBirth || '',
      birthday: item.birthday || null,
      identityCard: item.identityCard || '',
      issueDate: item.issueDate || null,
      issueAt: item.issueAt || '',
      residentialAddress: item.residentialAddress || '',
      address: item.address || '',
      avatar: item.avatar || '',
      userId,
    }
    yield put(ProfileActions.getProfileInfoSuccess(item, formItem))
  } catch (error) {
    yield put(ProfileActions.getProfileInfoFailure())
  }
}

function* editProfileWorker({ userId, values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    console.log('userId', userId)
    console.log(values)
    let submitData = {
      ...values,
      birthday: values.birthday
        ? moment(values.birthday).format('YYYY-MM-DD')
        : '',
      issueDate: values.issueDate
        ? moment(values.issueDate).format('YYYY-MM-DD')
        : '',
    }
    const response = yield call(UserService.updateProfile, userId, submitData)
    checkResponseError(response)
    yield put(ProfileActions.editProfileSuccess())
    yield put(NotificationActions.showNotification('Edit Profile Success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(AuthActions.getRolesRequest())
  } catch (error) {
    yield put(ProfileActions.editProfileFailure())
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(ProfileTypes.GET_PROFILE_INFO_REQUEST, getProfileInfoWorker),
    takeLatest(ProfileTypes.EDIT_PROFILE_REQUEST, editProfileWorker),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
