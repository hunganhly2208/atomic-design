import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { ProfileTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

// Set and clear profileInfo
const setProfileInfo = (state, { item, formItem }) =>
  state.set('profileInfo', fromJS(item)).set('formItem', fromJS(formItem))
const clearProfileInfo = state => state.set('profileInfo', fromJS({}))

const reducer = createReducer(INITIAL_STATE, {
  // Set and clear leaders
  [ProfileTypes.GET_PROFILE_INFO_SUCCESS]: setProfileInfo,
  [ProfileTypes.CLEAR_PROFILE_INFO]: clearProfileInfo,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
