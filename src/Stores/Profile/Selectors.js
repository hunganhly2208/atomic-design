import { MODULE_NAME } from './InitialState'
import { createSelector } from 'reselect'

const getProfileInfo = state => state[MODULE_NAME].get('profileInfo')

const getFormItem = state => state[MODULE_NAME].get('formItem')

export const ProfileSelectors = {
  getProfileInfo,
  getFormItem,
}
