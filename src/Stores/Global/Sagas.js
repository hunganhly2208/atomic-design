import { put, call, all, takeLatest } from 'redux-saga/effects'
import { GlobalActions, GlobalTypes } from 'Stores/Global/Actions'
import { GlobalService } from 'Services/GlobalService'
import { UserService } from 'Services/UserService'
import { DepartmentService } from 'Services/DepartmentService'
import { OfficeService } from 'Services/OfficeService'
import { checkResponseError } from 'Utils/checkResponseError'

function* getGlobalDataWorker() {
  try {
    const [listDepartment, listLocation, listRole] = yield all([
      call(DepartmentService.getListDepartment),
      call(OfficeService.getListOffice),
      call(GlobalService.getListRoles),
    ])
    yield put(
      GlobalActions.getGlobalDataSuccess(
        listDepartment.items,
        listLocation.items,
        listRole.items
      )
    )
  } catch (error) {
    yield put(GlobalActions.getGlobalDataFailure())
  }
}

function* searchUsersWorker({ keyword }) {
  try {
    const response = yield call(UserService.getListUser, { keyword, limit: 10 })
    checkResponseError(response)
    yield put(GlobalActions.searchUsersSuccess(response.items))
  } catch (error) {
    yield put(GlobalActions.searchUsersFailure())
    yield put(GlobalActions.showNotification(error.message))
  }
}

export default function* watcher() {
  yield all([
    takeLatest(GlobalTypes.GET_GLOBAL_DATA_REQUEST, getGlobalDataWorker),
    takeLatest(GlobalTypes.SEARCH_USERS_REQUEST, searchUsersWorker),
  ])
}
