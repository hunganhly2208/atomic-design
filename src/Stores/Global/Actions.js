import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    getGlobalDataRequest: null,
    getGlobalDataSuccess: ['departments', 'locations', 'roles'],
    getGlobalDataFailure: null,
    clearGlobalData: null,

    // Search User By keyword to autocomple
    searchUsersRequest: ['keyword'],
    searchUsersSuccess: ['items'],
    searchUsersFailure: null,
    clearSearchUsers: null,
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const GlobalTypes = Types
export const GlobalActions = Creators
