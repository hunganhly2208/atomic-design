import { INITIAL_STATE } from './InitialState'
import { fromJS } from 'immutable'
import { createReducer } from 'reduxsauce'
import { GlobalTypes } from './Actions'

const setGlobalData = (state, { departments, locations, roles }) =>
  state
    .set('departments', fromJS(departments))
    .set('locations', fromJS(locations))
    .set('roles', fromJS(roles))

// Set and clear list users seaching
const setSearchUsers = (state, { items }) =>
  state.set('searchUsers', fromJS(items))

const clearSearchUsers = state => state.set('searchUsers', fromJS([]))

const reducer = createReducer(INITIAL_STATE, {
  [GlobalTypes.GET_GLOBAL_DATA_SUCCESS]: setGlobalData,
  // Set and clear list user
  [GlobalTypes.SEARCH_USERS_SUCCESS]: setSearchUsers,
  [GlobalTypes.CLEAR_SEARCH_USERS]: clearSearchUsers,
})

export default reducer
