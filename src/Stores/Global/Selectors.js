import { MODULE_NAME } from './InitialState'
import { createSelector } from 'reselect'

const getCurrentVersion = state => state[MODULE_NAME].get('currentVersion')

const getDepartments = state => state[MODULE_NAME].get('departments')

const getLocations = state => state[MODULE_NAME].get('locations')

const getRoles = state => state[MODULE_NAME].get('roles')

const getSearchUsers = state => state[MODULE_NAME].get('searchUsers')

const getLocationOptions = createSelector(
  getLocations,
  locations => {
    let first = [{ value: '', label: 'Choose Office' }]
    let options = locations.map(location => ({
      value: location.get('id'),
      label: location.get('name'),
    }))
    return first.concat(options.toJS())
  }
)

const getDepartmentOptions = createSelector(
  getDepartments,
  departments => {
    let first = [{ value: '', label: 'Choose Department' }]
    let options = departments.map(department => ({
      value: department.get('id'),
      label: department.get('name'),
    }))
    return first.concat(options.toJS())
  }
)

const getRoleOptions = createSelector(
  getRoles,
  roles => {
    let first = [{ value: '', label: 'Choose All' }]
    let options = roles.map(role => ({
      value: role.get('roleId'),
      label: role.get('name'),
    }))
    return first.concat(options)
  }
)

const getSearchUserOptions = createSelector(
  getSearchUsers,
  searchUsers =>
    searchUsers
      .map(searchUser => ({
        value: searchUser.get('id'),
        label: searchUser.get('displayName'),
      }))
      .toJS()
)

export const GlobalSelectors = {
  getCurrentVersion,
  getDepartments,
  getLocations,
  getLocationOptions,
  getDepartmentOptions,
  getRoleOptions,
  getRoles,
  getSearchUserOptions,
}
