import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'global'

export const createActionName = name => `@@${MODULE_NAME}/${name}`

export const INITIAL_STATE = fromJS({
  departments: [],
  locations: [],
  roles: [],
  searchUsers: [],
})

export default INITIAL_STATE
