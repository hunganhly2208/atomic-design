import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List types
    getItemsRequest: null,
    getItemsSuccess: ['items'],
    getItemsFailure: null,
    clearItems: null,
    // Create Type actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Edit Type actions
    editItemRequest: ['values'],
    editItemSuccess: null,
    editItemFailure: null,
    // Delete Type actions
    deleteItemRequest: ['id'],
    deleteItemSuccess: null,
    deleteItemFailure: null,
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const LeaveTypeTypes = Types
export const LeaveTypeActions = Creators
