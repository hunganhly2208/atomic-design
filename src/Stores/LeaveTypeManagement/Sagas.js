import { put, call, takeLatest, all } from 'redux-saga/effects'
import { LeaveTypeActions, LeaveTypeTypes } from './Actions'
import { NotificationActions } from 'Stores/Notification/Actions'
import { ModalActions } from '../Modal/Actions'
import { LeaveTypeService } from 'Services/LeaveTypeService'
import { LoadingActions } from '../Loading/Actions'
import sagaRegistry from '../Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'

function* getListLeaveTypeWorker() {
  try {
    yield put(LoadingActions.showLoadingList())
    const response = yield call(LeaveTypeService.getListLeaveType)
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: LeaveTypeTypes.GET_ITEMS_SUCCESS,
      items: response.items,
    })
  } catch (error) {
    yield put({
      type: LeaveTypeTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

// Create Leave Type worker
function* createLeaveTypeWorker({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(LeaveTypeService.createLeaveType, values)
    checkResponseError(response)
    yield put({
      type: LeaveTypeTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(NotificationActions.showNotification('Create Leave Type Success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(LeaveTypeActions.getItemsRequest())
  } catch (error) {
    yield put({
      type: LeaveTypeTypes.CREATE_ITEM_FAILURE,
    })
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

// Edit Leave Type worker
function* editLeaveTypeWorker({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const { id, ...datas } = values
    const response = yield call(LeaveTypeService.editLeaveType, id, datas)
    checkResponseError(response)
    yield put({
      type: LeaveTypeTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(NotificationActions.showNotification('Edit Leave Type Success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(LeaveTypeActions.getItemsRequest())
  } catch (error) {
    yield put({
      type: LeaveTypeTypes.EDIT_ITEM_FAILURE,
    })
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

// Delete Holiday worker
function* deleteLeaveTypeWorker({ id }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(LeaveTypeService.deleteLeaveType, id)
    checkResponseError(response)
    yield put(LeaveTypeActions.deleteItemSuccess())
    yield put(NotificationActions.showNotification('Delete Leave Type Success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(LeaveTypeActions.getItemsRequest())
  } catch (error) {
    yield put(LeaveTypeActions.deleteItemFailure())
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(LeaveTypeTypes.GET_ITEMS_REQUEST, getListLeaveTypeWorker),
    takeLatest(LeaveTypeTypes.CREATE_ITEM_REQUEST, createLeaveTypeWorker),
    takeLatest(LeaveTypeTypes.EDIT_ITEM_REQUEST, editLeaveTypeWorker),
    takeLatest(LeaveTypeTypes.DELETE_ITEM_REQUEST, deleteLeaveTypeWorker),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
