import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { LeaveTypeTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state => state.set('items', fromJS([]))
const setItems = (state, { items }) => state.set('items', fromJS(items))

const reducer = createReducer(INITIAL_STATE, {
  // List items action handler
  [LeaveTypeTypes.GET_ITEMS_SUCCESS]: setItems,
  [LeaveTypeTypes.CLEAR_ITEMS]: clearItems,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
