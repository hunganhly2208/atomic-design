import { MODULE_NAME } from './InitialState'
import { createSelector } from 'reselect'
import moment from 'moment'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

const getTotalPages = state => state[MODULE_NAME].get('totalPages')

const getUserInfo = state => state[MODULE_NAME].get('userInfo')

const getUserDepartments = state => state[MODULE_NAME].get('userDepartments')

const getUserOffices = state => state[MODULE_NAME].get('userOffices')

const getUserRoles = state => state[MODULE_NAME].get('userRoles')

const getUserDetails = state => state[MODULE_NAME].get('userDetails')

const getFormItem = createSelector(
  getUserInfo,
  info => {
    return {
      id: info.get('id'),
      displayName: info.get('displayName') || '',
      email: info.get('email') || '',
      phone: info.get('phone') || '',
      avatar: info.get('avatar') || '',
      joinMonth: info.get('joinMonth')
        ? moment(info.get('joinMonth'), 'YYYY-MM-DD').format('YYYY-MM-DD')
        : null,
      didSatuday: Boolean(info.get('didSaturday')),
      didSunday: Boolean(info.get('didSunday')),
      jobTitle: info.get('jobTitle') || '',
      gender: info.get('gender') || '',
      firstName: info.get('firstName') || '',
      lastName: info.get('lastName') || '',
    }
  }
)

// Get form assign user
const getFormAssignItem = createSelector(
  getUserOffices,
  getUserDepartments,
  (offices, departments) => {
    const mainOffice = offices.find(office => office.get('isOrigin') === 1)
    const subOffices = offices
      .filter(office => office.get('isOrigin') === 0)
      .map(office => office.get('agencyId'))
    const mainDepartment = departments.find(
      department => department.get('isOrigin') === 1
    )
    const subDepartments = departments
      .filter(department => department.get('isOrigin') === 0)
      .map(department => ({
        value: department.get('departmentId'),
        label: department.get('departmentName'),
      }))
    return {
      mainOffice: mainOffice ? mainOffice.get('agencyId') : '',
      subOffices: subOffices.toJS(),
      mainDepartment: mainDepartment
        ? {
            value: mainDepartment.get('departmentId'),
            label: mainDepartment.get('departmentName'),
          }
        : '',
      subDepartments: subDepartments.toJS(),
    }
  }
)
// Get form set roles items
const getSetRolesItem = createSelector(
  getUserRoles,
  roles => {
    let listRoles = roles.map(role => role.get('roleId'))
    return {
      roles: listRoles.toJS(),
    }
  }
)

export const UserSelectors = {
  getFilter,
  getItems,
  getTotalCount,
  getTotalPages,
  getFormItem,
  getFormAssignItem,
  getSetRolesItem,
  getUserDetails,
  getUserInfo,
}
