import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List User actions
    getItemsRequest: null,
    getItemsSuccess: ['items', 'totalCount'],
    getItemsFailure: null,
    clearItems: null,
    // Edit User actions
    editItemRequest: ['values'],
    editItemSuccess: null,
    editItemFailure: null,
    // Create User actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Delete User actions
    deleteItemRequest: ['id'],
    deleteItemSuccess: null,
    deleteItemFailure: null,
    // Change Filter
    setFilter: ['filter'],
    // Get user's roles
    getUserRolesRequest: ['id'],
    getUserRolesSuccess: ['items'],
    getUserRolesFailure: null,
    clearUserInfo: null,
    // Get user general info
    getUserInfoRequest: ['id'],
    getUserInfoSuccess: ['item'],
    getUserInfoFailure: null,
    clearUserRoles: null,
    // Upload avatar
    uploadAvatarRequest: ['files', 'module', 'callback'],
    uploadAvatarSuccess: null,
    uploadAvatarFailure: null,
    clearAvatarInfo: null,
    // Get user Department
    getUserDepartmentsRequest: ['id'],
    getUserDepartmentsSuccess: ['items'],
    getUserDepartmentsFailure: null,
    clearUserDepartments: null,
    // Get user offices
    getUserOfficesRequest: ['id'],
    getUserOfficesSuccess: ['items'],
    getUserOfficesFailure: null,
    clearUserOffices: null,
    // Organize user
    organizeUserRequest: ['userId', 'values'],
    organizeUserSuccess: null,
    organizeUserFailure: null,
    // Set User Roles
    setUserRolesRequest: ['userId', 'values'],
    setUserRolesSuccess: null,
    setUserRolesFailure: null,
    // Get list User details
    getListUserDetailRequest: ['listId'],
    getListUserDetailSuccess: ['items'],
    getListUserDetailFailure: null,
    clearListUserDetail: null,
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const UserTypes = Types
export const UserActions = Creators
