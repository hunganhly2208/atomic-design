import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { UserTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state =>
  state.merge(
    fromJS({
      items: [],
      totalCount: 0,
      totalPage: 0,
    })
  )

const setItems = (state, { items, totalCount }) =>
  state.merge(
    fromJS({
      items,
      totalCount,
    })
  )

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

// Set and clear user general Infos
const setUserInfo = (state, { item }) => state.set('userInfo', fromJS(item))
const clearUserInfo = state => state.set('userInfo', fromJS({}))
// Set and clear User roles
const setUserRoles = (state, { items }) => state.set('userRoles', fromJS(items))
const clearUserRoles = state => state.set('userRoles', fromJS([]))
// Set and clear User departments
const setUserDepartments = (state, { items }) =>
  state.set('userDepartments', fromJS(items))
const clearUserDepartments = state => state.set('userDepartments', fromJS([]))
// Set And clear User offices
const setUserOffices = (state, { items }) =>
  state.set('userOffices', fromJS(items))
const clearUserOffices = state => state.set('userOffices', fromJS([]))
// Set and clear list user detail
const setListUserDetail = (state, { items }) =>
  state.set('userDetails', fromJS(items))

const clearListUserDetail = state => state.set('userDetails', fromJS([]))

const reducer = createReducer(INITIAL_STATE, {
  // List items action handler
  [UserTypes.GET_ITEMS_SUCCESS]: setItems,
  [UserTypes.CLEAR_ITEMS]: clearItems,
  // Set filters handler
  [UserTypes.SET_FILTER]: setFilter,
  // Set and clear User roles
  [UserTypes.GET_USER_ROLES_SUCCESS]: setUserRoles,
  [UserTypes.CLEAR_USER_ROLES]: clearUserRoles,
  // Set and clear User Info
  [UserTypes.GET_USER_INFO_SUCCESS]: setUserInfo,
  [UserTypes.CLEAR_USER_ROLES]: clearUserInfo,
  // Set and clear User departments
  [UserTypes.GET_USER_DEPARTMENTS_SUCCESS]: setUserDepartments,
  [UserTypes.CLEAR_USER_DEPARTMENTS]: clearUserDepartments,
  // Set and clear user offices
  [UserTypes.GET_USER_OFFICES_SUCCESS]: setUserOffices,
  [UserTypes.CLEAR_USER_OFFICES]: clearUserOffices,
  // Get list user detail
  [UserTypes.GET_LIST_USER_DETAIL_SUCCESS]: setListUserDetail,
  [UserTypes.CLEAR_LIST_USER_DETAIL]: clearListUserDetail,
})

reducerRegistry.register(MODULE_NAME, reducer)
export default reducer
