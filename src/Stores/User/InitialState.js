import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'user'

export const INITIAL_STATE = fromJS({
  items: [],
  filter: {
    limit: 10,
    page: 1,
    keyword: '',
    departmentId: '',
    agencyId: '',
  },
  userInfo: {},
  userRoles: [],
  totalCount: 0,
  userDepartments: [],
  userOffices: [],
  userDetails: [],
})
