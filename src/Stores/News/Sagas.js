import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { NewActions, NewTypes } from './Actions'
import { NotificationActions } from 'Stores/Notification/Actions'
import { ModalActions } from '../Modal/Actions'
import { NewService } from 'Services/NewService'
import { NewSelectors } from './Selectors'
import { UserActions } from 'Stores/User/Actions'
import { LoadingActions } from '../Loading/Actions'
import sagaRegistry from '../Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import { getValueFromSelect } from 'Utils/getValueFromSelect'

function* getListUserOfCategoryWorker({ categoryId }) {
  try {
    yield put(LoadingActions.showLoadingList())
    const filter = yield select(NewSelectors.getFilter)
    const response = yield call(NewService.getListCategoryUsers, {
      ...filter.toJS(),
      categoryId,
    })
    checkResponseError(response)
    const listUserid = response.items.map(item => item.userId)
    yield put(UserActions.getListUserDetailRequest(listUserid.join(',')))
    yield put(
      NewActions.getListUserOfCategorySuccess(
        response.items,
        response.totalCount
      )
    )
    yield put(LoadingActions.hideLoadingList())
  } catch (error) {
    yield put(NewActions.getListUserOfCategoryFailure())
    yield put(LoadingActions.hideLoadingList())
  }
}

// Assign User to category
function* assignUserToCategoryWorker({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const data = {
      categoryId: values.categoryId,
      userId: getValueFromSelect(values.userId),
      view: values.view ? 1 : 0,
      post: values.post ? 1 : 0,
      email: values.email ? 1 : 0,
    }
    const response = yield call(NewService.assignUserToCategory, data)
    checkResponseError(response)
    yield put(NewActions.assignUserToCategorySuccess())
    yield put(NewActions.getListUserOfCategoryRequest(values.categoryId))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(NotificationActions.showNotification('Assign user success'))
  } catch (error) {
    yield put(NewActions.assignUserToCategoryFailure())
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}
// Delete user from category
function* deleteUserFromCategory({ id }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(NewService.deleteUserFromCategory, id)
    checkResponseError(response)
    yield put(NewActions.deleteUserFromCategorySuccess())
    yield put(NewActions.getListUserOfCategoryRequest())
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(NotificationActions.showNotification('Delete user success'))
  } catch (error) {
    yield put(NewActions.deleteUserFromCategoryFailure())
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

function* watcher() {
  yield all([
    takeLatest(
      NewTypes.GET_LIST_USER_OF_CATEGORY_REQUEST,
      getListUserOfCategoryWorker
    ),
    takeLatest(
      NewTypes.ASSIGN_USER_TO_CATEGORY_REQUEST,
      assignUserToCategoryWorker
    ),
    takeLatest(
      NewTypes.DELETE_USER_FROM_CATEGORY_REQUEST,
      deleteUserFromCategory
    ),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
