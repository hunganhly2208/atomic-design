import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    getListUserOfCategoryRequest: ['categoryId'],
    getListUserOfCategorySuccess: ['items', 'totalCount'],
    getListUserOfCategoryFailure: null,
    clearListUserOfCategory: null,
    assignUserToCategoryRequest: ['values'],
    assignUserToCategorySuccess: null,
    assignUserToCategoryFailure: null,
    deleteUserFromCategoryRequest: ['id'],
    deleteUserFromCategorySuccess: null,
    deleteUserFromCategoryFailure: null,
    // Change Filter
    setFilter: ['filter'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const NewTypes = Types
export const NewActions = Creators
