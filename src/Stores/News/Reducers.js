import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { fromJS } from 'immutable'
import { createReducer } from 'reduxsauce'
import { NewTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const setCategoryUsers = (state, { items, totalCount }) =>
  state.set('categoryUsers', fromJS(items)).set('totalCount', totalCount)

const clearCategoryUsers = state => state.set('categoryUsers', fromJS([]))

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INITIAL_STATE, {
  [NewTypes.GET_LIST_USER_OF_CATEGORY_SUCCESS]: setCategoryUsers,
  [NewTypes.CLEAR_LIST_USER_OF_CATEGORY]: clearCategoryUsers,
  // Set filters handler
  [NewTypes.SET_FILTER]: setFilter,
})

reducerRegistry.register(MODULE_NAME, reducer)

export default reducer
