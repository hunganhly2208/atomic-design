import { fromJS } from 'immutable'
/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'New'

export const INITIAL_STATE = fromJS({
  categoryUsers: [],
  filter: {
    page: 1,
    limit: 10,
    keyword: '',
  },
  totalCount: 0,
})
