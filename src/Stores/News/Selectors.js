import { MODULE_NAME } from './InitialState'
import { fromJS } from 'immutable'
import { UserSelectors } from 'Stores/User/Selectors'
import { createSelector } from 'reselect'

const getCategoryUsers = state => state[MODULE_NAME].get('categoryUsers')

const getFilter = state => state[MODULE_NAME].get('filter')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

const getCategoryUsersWithInfos = createSelector(
  getCategoryUsers,
  UserSelectors.getUserDetails,
  (categoryUsers, userDetails) => {
    let sortCategoryUsers = categoryUsers.sort((a, b) => +a.userId - +b.userId)
    let sortUserDetails = userDetails.sort((a, b) => +a.id - +b.id)

    return sortCategoryUsers.map((cate, index) => {
      return cate.merge({
        userName: sortUserDetails.getIn([index, 'displayName']),
      })
    })
  }
)

export const NewSelectors = {
  getCategoryUsers,
  getFilter,
  getTotalCount,
  getCategoryUsersWithInfos,
}
