/**
 * The initial values for the redux state.
 */
import { fromJS } from 'immutable'
export const MODULE_NAME = 'notification'

export const INITIAL_STATE = fromJS({
  isOpen: false,
  message: '',
  duration: 3000,
})
