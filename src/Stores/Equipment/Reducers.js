import { fromJS } from 'immutable'

import { INITIAL_STATE, MODULE_NAME } from './InitialState'
import { createReducer } from 'reduxsauce'
import { EquipmentTypes } from './Actions'
import reducerRegistry from '../Reducers/ReducerRegistry'

const clearItems = state =>
  state.merge(
    fromJS({
      items: [],
      totalCount: 0,
      totalPage: 0,
    })
  )

const setItems = (state, { items, totalCount }) =>
  state.merge(
    fromJS({
      items,
      totalCount,
    })
  )

const setFilter = (state, { filter }) =>
  state.mergeIn(['filter'], fromJS(filter))

const reducer = createReducer(INITIAL_STATE, {
  // List items action handler
  [EquipmentTypes.GET_ITEMS_SUCCESS]: setItems,
  [EquipmentTypes.CLEAR_ITEMS]: clearItems,
  // Set filters handler
  [EquipmentTypes.SET_FILTER]: setFilter,
  // // Set and clear Equipment Info
  // [EquipmentTypes.GET_EQUIPMENT_INFO_SUCCESS]: setEquipmentInfo,
  // // Get list user detail
  // [EquipmentTypes.GET_LIST_EQUIPMENT_DETAIL_SUCCESS]: setListEquipmentDetail,
  // [EquipmentTypes.CLEAR_LIST_EQUIPMENT_DETAIL]: clearListEquipmentDetail,
})

reducerRegistry.register(MODULE_NAME, reducer)
export default reducer
