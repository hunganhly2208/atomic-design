import { fromJS } from 'immutable'

/**
 * The initial values for the redux state.
 */
export const MODULE_NAME = 'equipment'

export const INITIAL_STATE = fromJS({
  items: [],
  filter: {
    limit: 10,
    page: 1,
    keyword: '',
  },
  totalCount: 0,
})
