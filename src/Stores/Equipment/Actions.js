import { createActions } from 'reduxsauce'
import { MODULE_NAME } from './InitialState'

const { Types, Creators } = createActions(
  {
    // Get List Equipment actions
    getItemsRequest: null,
    getItemsSuccess: ['items', 'totalCount'],
    getItemsFailure: null,
    clearItems: null,
    // Edit Equipment actions
    editItemRequest: ['values'],
    editItemSuccess: null,
    editItemFailure: null,
    // Create Equipment actions
    createItemRequest: ['values'],
    createItemSuccess: null,
    createItemFailure: null,
    // Delete Equipment actions
    deleteItemRequest: ['id'],
    deleteItemSuccess: null,
    deleteItemFailure: null,
    // Change Filter
    setFilter: ['filter'],
  },
  {
    prefix: `@@${MODULE_NAME}/`,
  }
)

export const EquipmentTypes = Types
export const EquipmentActions = Creators
