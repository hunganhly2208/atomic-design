import { MODULE_NAME } from './InitialState'
import { createSelector } from 'reselect'
import moment from 'moment'

const getFilter = state => state[MODULE_NAME].get('filter')

const getItems = state => state[MODULE_NAME].get('items')

const getTotalCount = state => state[MODULE_NAME].get('totalCount')

const getTotalPages = state => state[MODULE_NAME].get('totalPages')

const getUserInfo = state => state[MODULE_NAME].get('userInfo')

const getUserDetails = state => state[MODULE_NAME].get('userDetails')

const getFormItem = createSelector(
  getUserInfo,
  info => {
    return {
      id: info.get('id'),
      displayName: info.get('displayName') || '',
      email: info.get('email') || '',
      phone: info.get('phone') || '',
      avatar: info.get('avatar') || '',
      joinMonth: info.get('joinMonth')
        ? moment(info.get('joinMonth'), 'YYYY-MM-DD').format('YYYY-MM-DD')
        : null,
      didSatuday: Boolean(info.get('didSaturday')),
      didSunday: Boolean(info.get('didSunday')),
      jobTitle: info.get('jobTitle') || '',
      gender: info.get('gender') || '',
      firstName: info.get('firstName') || '',
      lastName: info.get('lastName') || '',
    }
  }
)


export const EquipmentSelectors = {
  getItems,
  getFilter,
  getTotalCount,
  getTotalPages,
  // getFormItem,
  // getUserDetails,
  // getUserInfo,
}
