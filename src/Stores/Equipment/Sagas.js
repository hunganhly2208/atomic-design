import { put, call, takeLatest, select, all } from 'redux-saga/effects'
import { EquipmentActions, EquipmentTypes } from './Actions'
import moment from 'moment'
import { EquipmentSelectors } from './Selectors'
import { NotificationActions } from 'Stores/Notification/Actions'
import { ModalActions } from '../Modal/Actions'
import { UserService } from '../../Services/UserService'
import { DepartmentService } from 'Services/DepartmentService'
import { OfficeService } from 'Services/OfficeService'
import { LoadingActions } from '../Loading/Actions'
import { GeneralService } from 'Services/GeneralService'
import sagaRegistry from '../Sagas/SagaRegistry'
import { MODULE_NAME } from './InitialState'
import { checkResponseError } from 'Utils/checkResponseError'
import {
  createDepartmentOption,
  createOfficeOption,
  createDepartmentOptionFromFormik,
} from 'Utils/createOptions'
import { decamelizeKeys } from 'humps'

function* getListUserWorker() {
  try {
    const filter = yield select(EquipmentSelectors.getFilter)
    yield put(LoadingActions.showLoadingList())
    const response = yield call(UserService.getListUser, filter.toJS())
    checkResponseError(response)
    yield put(LoadingActions.hideLoadingList())
    yield put({
      type: EquipmentTypes.GET_ITEMS_SUCCESS,
      items: response.items,
      totalCount: response.totalCount,
    })
  } catch (error) {
    yield put({
      type: EquipmentTypes.GET_ITEMS_FAILURE,
    })
    yield put(LoadingActions.hideLoadingList())
  }
}

// Get user's info worker
function* getUserInfoWorker({ id }) {
  try {
    const response = yield call(UserService.getUserInfo, id)
    checkResponseError(response)
    yield put(EquipmentActions.getUserInfoSuccess(response.item))
  } catch (error) {
    yield put(EquipmentActions.getUserInfoFailure())
  }
}

// Get user's roles worker

function* getUserRolesWorker({ id }) {
  try {
    const response = yield call(UserService.getUserRoles, id)
    checkResponseError(response)
    yield put(EquipmentActions.getUserRolesSuccess(response.roles))
  } catch (error) {
    yield put(EquipmentActions.getUserRolesFailure())
  }
}

function* deleteItemWorker({ id }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const respones = yield call(UserService.deleteUser, id)
    checkResponseError(respones)
    yield put({
      type: EquipmentTypes.DELETE_ITEM_SUCCESS,
    })
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(EquipmentActions.getItemsRequest())
    yield put(NotificationActions.showNotification('Delete User success'))
  } catch (error) {
    yield put({
      type: EquipmentTypes.DELETE_ITEM_FAILURE,
    })
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

// Create User worker
function* createItemWorker({ values }) {
  try {
    let {
      office,
      department,
      joinMonth,
      didSunday,
      didSaturday,
      ...data
    } = values
    joinMonth = moment(joinMonth).format('YYYY-MM-DD')
    department = JSON.stringify([
      decamelizeKeys(createDepartmentOption(department.value, 0, 1)),
    ])
    office = JSON.stringify([
      decamelizeKeys(createOfficeOption(office.value, 0, 1)),
    ])
    let dataCreate = {
      ...data,
      department,
      agency: office,
      joinMonth,
      didSunday: didSunday ? 1 : 0,
      didSaturday: didSaturday ? 1 : 0,
    }
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(UserService.createUser, dataCreate)
    checkResponseError(response)
    yield put({
      type: EquipmentTypes.CREATE_ITEM_SUCCESS,
    })
    yield put(NotificationActions.showNotification('Create User Success'))
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(EquipmentActions.getItemsRequest())
  } catch (error) {
    yield put({
      type: EquipmentTypes.CREATE_ITEM_FAILURE,
    })
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

// Edit User worker
function* editItemWorker({ values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    let { id, joinMonth, didSunday, didSaturday, ...data } = values
    joinMonth = moment(joinMonth).format('YYYY-MM-DD')
    let dataEdit = {
      ...data,
      joinMonth,
      didSunday: didSunday ? 1 : 0,
      didSaturday: didSaturday ? 1 : 0,
    }
    const response = yield call(UserService.editUser, id, dataEdit)
    checkResponseError(response)
    yield put({
      type: EquipmentTypes.EDIT_ITEM_SUCCESS,
    })
    yield put(NotificationActions.showNotification('Edit User success'))
    yield put(EquipmentActions.getItemsRequest())
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
  } catch (error) {
    yield put({
      type: EquipmentTypes.EDIT_ITEM_FAILURE,
    })
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

// Upload avatar worker
function* uploadAvatarWorker({ files, module, callback }) {
  try {
    yield put(LoadingActions.showLoadingItem('uploadAvatar'))
    const response = yield call(GeneralService.uploadFile, files, module)
    checkResponseError(response)
    let file = response.items[0]
    callback('avatar', file.image)
    yield put(EquipmentActions.uploadAvatarSuccess())
    yield put(LoadingActions.hideLoadingItem('uploadAvatar'))
    yield put(NotificationActions.showNotification('Upload avatar success'))
  } catch (error) {
    yield put(EquipmentActions.uploadAvatarFailure())
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingItem('uploadAvatar'))
  }
}

// Get User department Workers
function* getUserDepartmentsWorker({ id }) {
  try {
    const respone = yield call(DepartmentService.getListUserDepartment, id)
    checkResponseError(respone)
    yield put(EquipmentActions.getUserDepartmentsSuccess(respone.items))
  } catch (error) {
    yield put(EquipmentActions.getUserDepartmentsFailure())
    yield put(NotificationActions.showNotification(error.message))
  }
}

// Get User Office Workers
function* getUserOfficesWorker({ id }) {
  try {
    const respone = yield call(OfficeService.getListUserOffice, id)
    checkResponseError(respone)
    yield put(EquipmentActions.getUserOfficesSuccess(respone.items))
  } catch (error) {
    yield put(EquipmentActions.getUserOfficesFailure())
    yield put(NotificationActions.showNotification(error.message))
  }
}

// Organize user worker
function* organizeUserWorker({ userId, values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    let mainOffice = createOfficeOption(values.mainOffice, 0, 1)
    mainOffice = mainOffice ? [mainOffice] : []
    let subOffices = values.subOffices.map(subOffice =>
      createOfficeOption(subOffice, 0, 0)
    )
    let mainDepartment = createDepartmentOptionFromFormik(
      values.mainDepartment,
      0,
      1
    )
    mainDepartment = mainDepartment ? [mainDepartment] : []
    let subDepartments = values.subDepartments.map(subDepartment =>
      createDepartmentOptionFromFormik(subDepartment, 0, 0)
    )
    const data = {
      agency: JSON.stringify(decamelizeKeys([...subOffices, ...mainOffice])),
      department: JSON.stringify(
        decamelizeKeys([...subDepartments, ...mainDepartment])
      ),
    }
    const respone = yield call(UserService.editUser, userId, data)
    checkResponseError(respone)
    yield put(EquipmentActions.organizeUserSuccess())
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(EquipmentActions.getItemsRequest())
    yield put(NotificationActions.showNotification('Organize User success'))
  } catch (error) {
    yield put(EquipmentActions.organizeUserFailure())
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

// Set User roles worker
function* setUserRolesWorker({ userId, values }) {
  try {
    yield put(LoadingActions.showLoadingAction())
    const response = yield call(UserService.setUserRoles, userId, {
      items: JSON.stringify(values.roles),
    })
    checkResponseError(response)
    yield put(EquipmentActions.organizeUserSuccess())
    yield put(LoadingActions.hideLoadingAction())
    yield put(ModalActions.clearModal())
    yield put(EquipmentActions.getItemsRequest())
    yield put(NotificationActions.showNotification("Set User's roles success"))
  } catch (error) {
    yield put(EquipmentActions.setUserRolesFailure())
    yield put(NotificationActions.showNotification(error.message))
    yield put(LoadingActions.hideLoadingAction())
  }
}

// Get list user detail worker

function* getListUserDetailWorker({ listId }) {
  try {
    const response = yield call(UserService.getListUserDetail, {
      items: listId,
    })
    checkResponseError(response)
    yield put(EquipmentActions.getListUserDetailSuccess(response.items))
  } catch (error) {
    yield put(EquipmentActions.getListUserDetailFailure())
  }
}

function* watcher() {
  yield all([
    takeLatest(EquipmentTypes.GET_ITEMS_REQUEST, getListUserWorker),
    takeLatest(EquipmentTypes.DELETE_ITEM_REQUEST, deleteItemWorker),
    takeLatest(EquipmentTypes.CREATE_ITEM_REQUEST, createItemWorker),
    takeLatest(EquipmentTypes.EDIT_ITEM_REQUEST, editItemWorker),
    // Get user's info
    takeLatest(EquipmentTypes.GET_USER_INFO_REQUEST, getUserInfoWorker),
    takeLatest(EquipmentTypes.GET_USER_ROLES_REQUEST, getUserRolesWorker),
    takeLatest(EquipmentTypes.UPLOAD_AVATAR_REQUEST, uploadAvatarWorker),
    takeLatest(EquipmentTypes.GET_USER_OFFICES_REQUEST, getUserOfficesWorker),
    takeLatest(
      EquipmentTypes.GET_USER_DEPARTMENTS_REQUEST,
      getUserDepartmentsWorker
    ),
    // Organize user office and department
    takeLatest(EquipmentTypes.ORGANIZE_USER_REQUEST, organizeUserWorker),
    // Set Uesr roles
    takeLatest(EquipmentTypes.SET_USER_ROLES_REQUEST, setUserRolesWorker),
    takeLatest(EquipmentTypes.GET_LIST_USER_DETAIL_REQUEST, getListUserDetailWorker),
  ])
}
sagaRegistry.register(MODULE_NAME, watcher)

export default watcher
